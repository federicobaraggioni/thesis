# import
import h5py
import numpy as np
import matplotlib.pyplot as plt

def load_file(filename):
    f = h5py.File(filename, "r")
    #print("Itemes: ", list(f.items()))
    print("--------------------------")
    base=f.get('level_0')
    chombo = f.get('Chombo_global')
    #print(list(chombo.items()))
    #print(list(base.items()))
    GridIds = base.get('GridId')
    Hier_Down = base.get('Hier_Down')
    Hier_Next = base.get('Hier_Next')
    Hier_Up = base.get('Hier_Up')
    boxes = base.get('boxes')
    data = base.get('data:datatype=0')

    #print(np.array(GridIds))
    #print(np.array(Hier_Down))
    #print(np.array(Hier_Next))
    #print(np.array(Hier_Up))
    #print(np.array(boxes))

    size_data = 512

    density = np.array(data[:size_data**3]).reshape(size_data, size_data, size_data)
    hydro_density = np.array(data[-size_data**3:]).reshape(size_data, size_data, size_data)


    return hydro_density, density


def plot_colormap(matrix_to_plot, title_string, folder,x_label, y_label, color_label, min_max_1,min_max_2,is_log_10=True, v_min=0, v_max=0, axis=0):
    dir_name = folder+"/axis_"+str(axis)+"/"
    plt.clf()
    if is_log_10:
        plt.imshow(np.log10(matrix_to_plot), vmin=v_min, vmax=v_max,cmap='jet', origin='lower', extent=[min_max_1[0],min_max_1[1],min_max_2[0],min_max_2[1]])
    else:
        plt.imshow(matrix_to_plot, clim=(v_min,v_max), cmap='jet', origin='lower', extent=[min_max_1[0],min_max_1[1],min_max_2[0],min_max_2[1]])

    cbar = plt.colorbar(label=color_label)
    plt.title(color_label)
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.savefig(dir_name+"2D "+color_label)
    cbar.remove()

def plot_distribution(array_to_plot, title_string, folder,n_bins, v_min, v_max,axis=0):
    dir_name = folder+"/axis_"+str(axis)+"/"
    plt.clf()
    flat_matrix = np.ravel(array_to_plot)
    plt.hist(flat_matrix, bins=n_bins,range=(v_min,v_max), histtype='step', density=True)
    plt.title(title_string)
    plt.yscale("log")
    plt.savefig(dir_name+"1D " + title_string)

def Compute_column_density(Mass, cell_size,axis_par=0):
    sol_mass = 1.9891E33
    proton_mass = 1.6726E-24
    Unit_mass_to_g = sol_mass

    print("Toatal Mass", "{:e}".format(np.sum(Mass)))
    # compute mass along line of sight
    Mass_los = np.sum(Mass, axis=axis_par)
    # compute Number of particles along line of sight
    Particle_los = Mass_los * Unit_mass_to_g / proton_mass
    # compute density along line of sight
    Gas_density = Particle_los / (cell_size ** 2)

    Column_density = np.where(Gas_density == 0, 1E-1, Gas_density)

    return Column_density


# Load files
folder_location="/data/fbara/simulations/WDM/Output_New_"
filenames =["WDM_z_3","WDM_zoom_z_3","WDM_z_4","WDM_zoom_z_4"]
h=0.6777
min_max_list=[[[57/h,77/h],[57/h,77/h],[57/h,77/h]], [[67.273/h,69.981/h],[62.618/h,65.326/h],[69.214/h,71.922/h]]]

for j in range(4):
	Neutral_mass, Gas_mass = load_file(folder_location+filenames[j])
	size_in_Mpc=min_max_list[j%2][0][1]-min_max_list[j%2][0][0]

	Mpc_to_cm = 3.086 * (1E24)
	cell_size_cm = (size_in_Mpc / 512) * Mpc_to_cm

	for i in range(3):
		Neutral_Column_density = Compute_column_density(Neutral_mass, cell_size_cm,axis_par=i)
		Gas_Column_density = Compute_column_density(Gas_mass, cell_size_cm,axis_par=i)

		# plot density field
		plot_colormap(Gas_Column_density, 'Density',filenames[j],'x (Mpc)', 'y (Mpc)','Gas Column Density', min_max_list[j%2][i-1],min_max_list[j%2][(i+1)%3],is_log_10=True, v_min=15, v_max=24,axis=i)
		plot_distribution(Gas_Column_density, "Gas Column Density Distribution", filenames[j],1000, 12,22,axis=i)

		# plot density field
		plot_colormap(Neutral_Column_density, 'Density',filenames[j],'x (Mpc)', 'y (Mpc)','Neutral Hydrogen Column Density', min_max_list[j%2][i-1],min_max_list[j%2][(i+1)%3], is_log_10=True, v_min=15, v_max=24,axis=i)
		plot_distribution(Neutral_Column_density, "NHI Column Density Distribution", filenames[j],1000, 12,22,axis=i)

	coordinates=np.arange(512)
	coordinates_x=size_in_Mpc*coordinates/512.0+min_max_list[j%2][0][0]*np.ones(512)
	coordinates_y=size_in_Mpc*coordinates/512.0+min_max_list[j%2][1][0]*np.ones(512)
	coordinates_z=size_in_Mpc*coordinates/512.0+min_max_list[j%2][2][0]*np.ones(512)

	Mass = Gas_mass
	print("Toatal Mass", "{:e}".format(np.sum(Mass)))
	# compute mass along line of sight
	Mass_23 = np.sum(Mass, axis=0)
	Mass_13 = np.sum(Mass, axis=1)
	Mass_12 = np.sum(Mass, axis=2)
	Masses = [np.sum(Mass_12, axis=1),np.sum(Mass_12, axis=0),np.sum(Mass_13, axis=0)]

	plt.clf()

	for i in range(3):

		plt.plot(coordinates_x,Masses[i],label=str(i+1))
		plt.legend()
		plt.savefig(filenames[j]+"/"+str(i+1))
		plt.clf()
