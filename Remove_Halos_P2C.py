# import
import h5py
import numpy as np

size_data = 512
h = 0.6777
Mpc_to_cm = 3.086 * (1E24)

def load_file(filename,minx,miny,minz,max_x,max_y,max_z,cellsize,halo_location):
    f = h5py.File(filename, "r+")
    print("--------------------------")
    print(filename)
    base=f.get('level_0')
    data = base.get('data:datatype=0')

    SB = np.array(data[6*(size_data**3):7*(size_data**3)]).reshape((size_data, size_data, size_data),order='F')
    SB = remove_CGM(SB,minx,miny,minz,max_x,max_y,max_z,cellsize,halo_location)
    
    new_data = SB.reshape(size_data**3,order='F')
    data[-size_data**3:] = new_data
    f.close()

def remove_CGM(SB_matrix,min_x,min_y,min_z,max_x,max_y,max_z,cellsize,location):
    x,y,z,R = load_galaxies_positions_and_radius(location,min_x,min_y,min_z,max_x,max_y,max_z)
    px,py,pz = compute_grid_pos_3d(x,y,z,cellsize,min_x,min_y,min_z)
    SB_matrix = set_gal_pos_to_zero(SB_matrix,px,py,pz,R,cellsize)
    return SB_matrix 

def load_galaxies_positions_and_radius(location,minx,miny,minz,max_x,max_y,max_z):
    columns = (5, 6, 7, 64, 11)
    # Read the data using genfromtxt and select specific columns
    data = np.genfromtxt(location, usecols=columns)
    galaxies_mass = data[:,3]
    # selct galaxies with stellar mass and convert to physical Mpc from kpc/h
    min_mass=5E8
    xC = data[:,0][galaxies_mass>min_mass]/(1000*h)
    yC = data[:,1][galaxies_mass>min_mass]/(1000*h)
    zC = data[:,2][galaxies_mass>min_mass]/(1000*h)
    Rh = data[:,4][galaxies_mass>min_mass]/(1000*h)
    Gal_masses = data[:,3][galaxies_mass>min_mass]/h
    #select data with ZC in the zoom in region
    xC,yC,Gal_masses,Rh,zC = select_gal_inside_box(xC,yC,Gal_masses,Rh,zC,max_z,minz)     
    yC,zC,Gal_masses,Rh,xC = select_gal_inside_box(yC,zC,Gal_masses,Rh,xC,max_x,minx)     
    xC,zC,Gal_masses,Rh,yC = select_gal_inside_box(xC,zC,Gal_masses,Rh,yC,max_y,miny)     

    return xC,yC,zC,Rh

def select_gal_inside_box(arr1,arr2,arr3,arr4,arr5,maximum,minimum):
    arr1 = arr1[(arr5>minimum) & (arr5<maximum)]
    arr2 = arr2[(arr5>minimum) & (arr5<maximum)]
    arr3 = arr3[(arr5>minimum) & (arr5<maximum)]
    arr4 = arr4[(arr5>minimum) & (arr5<maximum)]
    arr5 = arr5[(arr5>minimum) & (arr5<maximum)]
    
    return arr1,arr2,arr3,arr4,arr5

def compute_grid_pos_3d(x,y,z,cell_size,le_cut_x,le_cut_y,le_cut_z):
    cell_size_Mpc=cell_size/(Mpc_to_cm)
    px = np.floor((x-le_cut_x)/(cell_size_Mpc))
    py = np.floor((y-le_cut_y)/(cell_size_Mpc))
    pz = np.floor((z-le_cut_z)/(cell_size_Mpc))
    return px,py,pz

def set_gal_pos_to_zero(matrix,px,py,pz,R,cellsize):
    cell_size_Mpc=cellsize/(Mpc_to_cm)
    for halo in range(len(px)):
        ext = R[halo]/(2*cell_size_Mpc)
        x_min = max(int(px[halo]-ext),0)
        x_max = min(int(px[halo]+ext),511)
        y_min = max(int(py[halo]-ext),0)
        y_max = min(int(py[halo]+ext),511)
        z_min = max(int(pz[halo]-ext),0)
        z_max = min(int(pz[halo]+ext),511)

        for i in range(x_min,x_max):
            for j in range(y_min,y_max):
                for k in range(z_min,z_max):
                    if (i-px[halo])**2+(j-py[halo])**2+(k-pz[halo])**2<ext**2:
                        matrix[i,j,k] = 0
    return matrix

#z, kev, folder
settings = ["4", "1", "1"]
folder_name = "Comparison_z_"+settings[0]+"_CDM_WDM_zoom_"+settings[1]+"p5kev/axis_"
folder_location=["/data/fbara/simulations/CDM/","/data/fbara/simulations/WDM/"]
filenames =["CDM_z_"+settings[0]+"_zoom_3p5kev","WDM_z_"+settings[0]+"_zoom_"+settings[1]+"p5kev"]
min_max_list=[[66.2415993685/h,68.95239936853/h],[64.0552471324/h,66.7660471324/h],[68.35748598955/h,71.06828598955/h]]
size_in_Mpc=min_max_list[0][1]-min_max_list[0][0]
cell_size_cm = (size_in_Mpc / 512) * Mpc_to_cm

minx = min_max_list[0][0]
miny = min_max_list[1][0]
minz = min_max_list[2][0]
max_x = min_max_list[0][1]
max_y = min_max_list[1][1]
max_z =  min_max_list[2][1]

halo_location = "/data/titouan/Eagle_cosmo_gizmo/200_512/CDM/zoom1p5_hydro_fire2_lvl7/Halo10/sftresh_cool_low_fb_boost/AHF_00"+settings[2]+"_allparts/200_snap_z"+settings[0]+".000_halos_sorted_main_hires"
halo_location_2 = "/data/titouan/Eagle_cosmo_gizmo/200_512/WDM_"+settings[1]+"p5keV/zoom1p5_hydro_fire2_lvl7/Halo10/sftresh_cool_low_fb_boost/AHF_00"+settings[2]+"_allparts/200_snap_z"+settings[0]+".000_halos_sorted_main_hires"

data = [load_file(folder_location[0]+filenames[0],minx,miny,minz,max_x,max_y,max_z,cell_size_cm,halo_location),load_file(folder_location[1]+filenames[1],minx,miny,minz,max_x,max_y,max_z,cell_size_cm,halo_location_2)]
