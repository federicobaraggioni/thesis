module GIZMO

      use hdf5
      use h5lt
      use logging
      use plotter

      implicit none

contains
    !.. readGroupAttribute_double
    !.. readGroupAttribute_integer
    !.. readParticleAttribute_2D
    !.. readParticleAttribute_1D
    !.. ReadGizmo

subroutine readGroupAttribute_double(file_id, gr_name, attr_name, targetDouble)

    !.. file_id: integer(hid_t) used to identify the already opened hdf5 file from which the group attribute will be read
    !.. gr_name: character string of the name of the group that the desired attribute is stored in
    !.. attr_name: character string of the name of the desired attribute
    !.. targetDouble: real(kind=8) that the attribute is read into

    implicit none

    !.. define the input variables
    integer(hid_t), intent(in)      :: file_id
    character(len=100), intent(in)  :: gr_name, attr_name
    real*8, intent(inout)           :: targetDouble

    !.. define local variables
    integer(hid_t)  :: gr_id, attr_id
    integer(HSIZE_T), dimension(1) :: dims_attr
    integer         :: status



    !.. Open the group and check whether there is an error
    call H5Gopen_f(file_id, gr_name, gr_id, status)
    IF(status/=0) THEN
        print *, 'There is a problem with H5Gopen_f() (opening the Group). The error code is:', status
        STOP
    END IF

    !.. Open the attribut and check whether there is an error
    call h5aopen_name_f(gr_id, attr_name, attr_id, status)
    IF(status/=0) THEN
        print *, 'There is a problem with H5aopen_f() (opening the Group). The error code is:', status
        STOP
    END IF

    dims_attr = 1
    call h5aread_f(attr_id, H5T_NATIVE_DOUBLE, targetDouble, dims_attr, status)

    !.. Release and terminate access to attribute & group
    call H5aclose_f(attr_id, status)
    call H5Gclose_f(gr_id, status)

end subroutine readGroupAttribute_double

subroutine readGroupAttribute_integer(file_id, gr_name, attr_name, targetInteger)

    !.. file_id: integer(hid_t) used to identify the already opened hdf5 file from which the group attribute will be read
    !.. gr_name: character string of the name of the group that the desired attribute is stored in
    !.. attr_name: character string of the name of the desired attribute
    !.. targetDouble: real(kind=8) that the attribute is read into

    implicit none

    !.. define the input variables
    integer(hid_t), intent(in)      :: file_id
    character(len=100), intent(in)  :: gr_name, attr_name
    integer, intent(inout)          :: targetInteger

    !.. define local variables
    integer(hid_t)  :: gr_id, attr_id
    integer(HSIZE_T), dimension(1) :: dims_attr
    integer         :: status



    !.. Open the group and check whether there is an error
    call H5Gopen_f(file_id, gr_name, gr_id, status)
    IF(status/=0) THEN
        print *, 'There is a problem with H5Gopen_f() (opening the Group). The error code is:', status
        STOP
    END IF

    !.. Open the attribut and check whether there is an error
    call h5aopen_name_f(gr_id, attr_name, attr_id, status)
    IF(status/=0) THEN
        print *, 'There is a problem with H5aopen_f() (opening the Group). The error code is:', status
        STOP
    END IF

    dims_attr = 1
    call h5aread_f(attr_id, H5T_NATIVE_INTEGER, targetInteger, dims_attr, status)

    !.. Release and terminate access to attribute & group
    call H5aclose_f(attr_id, status)
    call H5Gclose_f(gr_id, status)

end subroutine readGroupAttribute_integer

subroutine readParticleAttribute_2D(file_id, gr_name, ds_name, targetArray,number_array_elements)
    !.. This subroutine copies a 2D dataspace from an already open hdf5 file into the 2D array targetArray

    !.. file_id: integer(hid_t) used to identify the already opened hdf5 file from which the particle attribute will be read
    !.. gr_name: character string of the name of the group that dataspace holding the particle attribute is stored in
    !.. ds_name: character string of the name of the dataspace storing the particle attribute
    !.. targetArray: real(kind=4) 2D-array the particle attribute will be copied into

      implicit none

      !.. define the input variables
      integer :: number_array_elements
      real(kind=4), intent(inout)       :: TargetArray(:,:)
      INTEGER(HID_T), intent(in)        :: file_id
      CHARACTER(len=100), intent(in)    :: gr_name, ds_name

      !.. define local variables
      INTEGER                               :: status!, attr_num
      integer(hid_t)                        :: gr_id, ds_id, dsp_id
      INTEGER(HSIZE_T), dimension(number_array_elements)        :: data_dims, maxdims

      !.. opening the group and checking for error
      CALL H5Gopen_f(file_id, gr_name, gr_id, status)
        IF(status/=0) THEN
            print *, 'There is a problem with H5Gopen_f() (opening the Group). The error code is:', status
            STOP
        END IF

        !.. opening the dataspace and chekcing for errors
        CALL H5Dopen_f(file_id, ds_name, ds_id, status)
        IF(status/=0) THEN
            print *, 'There is a problem with H5Dopen_f() (opening the dataset). The error code is:', status
            STOP
        END IF

        !.. Return an identifier for a copy of the dataspace for a dataset
        CALL H5Dget_space_f(ds_id, dsp_id, status)

        !.. retrieve the dimensions of the dataset
        CALL H5Sget_simple_extent_dims_f(dsp_id, data_dims, maxdims, status)

        !load data from the chombo.hdf5 file into an array
        call H5Dread_f(ds_id, H5T_NATIVE_real, TargetArray, data_dims, status)

        !.. Release and terminate access to dataspace; Close the dataset, group, file and the FORTRAN interface
        CALL H5Sclose_f(dsp_id, status)
        CALL H5Dclose_f(ds_id,status)
        CALL H5Gclose_f(gr_id, status)

end subroutine readParticleAttribute_2D

subroutine readParticleAttribute_1D(file_id, gr_name, ds_name, targetArray)
    !.. This subroutine copies a 2D dataspace from an already open hdf5 file into the 2D array targetArray

    !.. file_id: integer(hid_t) used to identify the already opened hdf5 file from which the particle attribute will be read
    !.. gr_name: character string of the name of the group that dataspace holding the particle attribute is stored in
    !.. ds_name: character string of the name of the dataspace storing the particle attribute
    !.. targetArray: real(kind=4) 1D-array the particle attribute will be copied into

      implicit none

      !.. define the input variables
      real(kind=4), intent(inout)       :: TargetArray(:)
      INTEGER(HID_T), intent(in)        :: file_id
      CHARACTER(len=100), intent(in)    :: gr_name, ds_name

      !.. define local variables
      INTEGER                               :: status!, attr_num
      integer(hid_t)                        :: gr_id, ds_id, dsp_id
      INTEGER(HSIZE_T), dimension(3)        :: data_dims, maxdims

      !.. opening the group and checking for error
      CALL H5Gopen_f(file_id, gr_name, gr_id, status)
        IF(status/=0) THEN
            print *, 'There is a problem with H5Gopen_f() (opening the Group). The error code is:', status
            STOP
        END IF

        !.. opening the dataspace and chekcing for errors
        CALL H5Dopen_f(file_id, ds_name, ds_id, status)
        IF(status/=0) THEN
            print *, 'There is a problem with H5Dopen_f() (opening the dataset). The error code is:', status
            STOP
        END IF

        !.. Return an identifier for a copy of the dataspace for a dataset
        CALL H5Dget_space_f(ds_id, dsp_id, status)

        !.. retrieve the dimensions of the dataset
        CALL H5Sget_simple_extent_dims_f(dsp_id, data_dims, maxdims, status)

        !load data from the chombo.hdf5 file into an array
        call H5Dread_f(ds_id, H5T_NATIVE_real, TargetArray, data_dims, status)

        !.. Release and terminate access to dataspace; Close the dataset, group, file and the FORTRAN interface
        CALL H5Sclose_f(dsp_id, status)
        CALL H5Dclose_f(ds_id,status)
        CALL H5Gclose_f(gr_id, status)

end subroutine readParticleAttribute_1D

subroutine ReadGizmo(file_path, pos_gas, vel_gas, T_gas, rho_gas, neutralFrac_gas, metallicity_gas,mass_gas, smoothing_gas, ave_baryonic_rho, &
                        Redshift, aExp, ComovingBoxOrigin, ComovingBoxSize, gas_n, cell_n, cut_le, cut_re, ave_grid_mass, logger)
    !.. this subroutine reads all the necessary particle atributes and information from arepo-type simulation boxes

    !.. file_path: character string of the path  to the (potentially multiple) hdf5 files
    !.. pos_gas: real(kind=4) allocatable 2D array to store the coordinates of the gas particles [bosxize]
    !.. vel_gas: real(kind=4) allocatable 2D array to store the velocities of the gas particles [comoving km/s]
    !.. u_gas: real(kind=4) allocatable 1D array to store the temperature of the gas particles computed from the internal energy [K??]
    !.. T_gas: real(kind=4) allocatable 1D array to store the temperature of the gas particle as read from the snapshots [K]
    !.. rho_gas: real(kind=4) allocatable 1D array to store the density of the gas particles as read direcly from the sim [overdensity]
    !.. mass_gas: real(kind=4) allocatable 1D array to store the masses of the gas particles [average mass contained in a cell]
    !.. smoothing_gas: real(kind=4) allocatable 1D array to store the smoothing lengths of the gas particles [boxsize]
    !.. ave_baryonic_rho: real(kind=8) that holds the average baryonic denisty at z=0 for this simulation [g/cm @ z=0]
    !.. redshift: real(kind=8) that holds the redshift of the simulation
    !.. aExp: real(kind=8) that holds the expansion factor of the simulation
    !.. ComovingBoxOrigin: real(kind=8) 1D array that stores the origin of the box (0,0,0)
    !.. ComovingBoxSize: real(kind=8) that holds the comoving side length of the simulation box as read from the hdf5 file [comoving Mpc]
    !.. gas_n: integer storing the amount of paricles in the sim box as read from the simulation
    !.. cell_n: integer storing the basegrid size [# cells]
    !.. cut_le: real(kind=8) 1D array with size=3 storing the lower left corner of the region to be converted to a grid
    !.. cut_re: real(kind=8) 1D array with size=3 storing the upper right corner of the region to be converted to a grid
    !.. logger : type(logging) needs to be passed to enable logging capabilities within this subrouinte, see src/logger

      implicit none

      !.. define the input variables
      character(len=250), intent(in)    :: file_path
      real*4, allocatable               :: pos_gas(:, :), vel_gas(:,:), u_gas(:), T_gas(:), rho_gas(:), &
                                            mass_gas(:), smoothing_gas(:),metallicity_gas(:,:)
      real*8                            :: ComovingBoxSize, ave_baryonic_rho, Redshift, aExp, ComovingBoxOrigin(3), &
                                            cut_le(3), cut_re(3)
      integer*8, intent(inout)          :: gas_n
      integer, intent(in)               :: cell_n
      type(logging_t), intent(inout)    :: logger

      !.. define local variables
      character(len=250)    :: file_name
      character(len=30)     :: filecount
      integer               :: ipos, n = 0, n_loaded = 0, gas_n_file, gas_n_internal, i, counter
      logical               :: file_exists = .true., single_prec = .true.
      real*8                :: UnitLength_in_cm, UnitVelocity_in_cm_per_s, UnitMass_in_g, UnitTime_in_s, &
                                UnitEnergy_in_cgs, UnitDensity_in_cgs, UnitMass_in_Msol, &
                                rho_crit_0_cgs, OmegaBaryon, HubbleParam, h_inv, cell_volume, ave_grid_mass, &
                                value_floor, dx(3), mu = 1.0, total_mass_gas = 0
      real*8, parameter     :: cmToMpc=3.2408d-25, cmToKm=1d-5, gToMsol=5.0279d-34, &
                                HFrac = .76d0, nelecgas = 1.157, & ! using same param as readGadget may well be wrong!!
                                mproton = 1.6726d-24, kB = 1.3806d-16, gamma = 5.d0/3.d0, pi = 3.14159d0, &
                                gravityConst = 6.6743d-8, hubbleConst = 3.2407789d-18 !.. weird units to match the sim outputs
      real*4, allocatable   :: elecAbundance_gas(:), neutralFrac_gas(:)
      real*4, allocatable   :: pos_gasAll(:, :), vel_gasAll(:,:), u_gasAll(:), T_gasAll(:), rho_gasAll(:), &
                                mass_gasAll(:), smoothing_gasAll(:), elecAbundance_gasAll(:), neutralFrac_gasAll(:), metallicity_gasAll(:,:)

      ! Plotter
      type(plotter_canvas_t) :: canvas
      type(plotter_histogram_t) :: coor_hist, vel_hist, temp_u_hist, temp_hist, rho_hist
      integer :: hist_id
      character(len=128) :: hist_label
      character(len=1) :: vel_labels(3) = ['x', 'y', 'z']

      !REAL(kind=8), PARAMETER :: !UnitPressure_in_cgs = UnitMass_in_g/UnitLength_in_cm/UnitTime_in_s**2, &
                                 !GRAVITY = 6.672d-8,
                                 !G = GRAVITY/UnitLength_in_cm**3*UnitMass_in_g*UnitTime_in_s**2, &

      real*8, allocatable       :: pos_gas_dp(:, :)
      !INTEGER                   :: flat_index

      !!..HDF5 Types
      CHARACTER(len=100)        :: gr_name, ds_name, attr_name
      INTEGER                   :: status!, attr_num
      INTEGER(HID_T)            :: file_id, gr_id, ds_id, dsp_id, attr_id!, datatype
      INTEGER(HSIZE_T), dimension(3) :: data_dims, maxdims
      INTEGER(HSIZE_T), dimension(1) :: dims_attr

      call logger%begin_section('gizmo')
      call canvas%init(80, 20)
      call logger%log('path', '(formatted)', '=', [file_path])

      ! check the formatting of the inputted file name
      if(scan(file_path, '%') == 0) then
            call logger%err('[Error] Please use a formatted file path! E.g. /path/to/gizmo/snap_000.%d.hdf5')
            stop
      end if

      !.. initialize the HDF5 interface and check whether there is an error
      CALL H5open_f(status)
      IF(status/=0) THEN
              print *, 'There is a problem with H5open_f() (initializing the hdf5 interface). The error code is:', status
              STOP
      END IF

      ! determine where in file_path the index place holder is
      ipos = index(file_path, '%d')

      do while(file_exists)
            ! generate the file name and check whether it exists
            write (filecount, "(I3.3)") n
            file_name = trim(file_path(1:ipos-1))//trim(filecount)//trim(file_path(ipos+2:))
            inquire(file=file_name, exist=file_exists)

            if (.not. file_exists .and. n == 0) then
                call logger%err('[Error] The first file does not have the expected name! E.g. E.g. /path/to/gizmo/snap_000.000.hdf5')
                stop
            else if (.not. file_exists) then
                cycle
            end if

            !.. need to obtain the total amount of particles and other necessary info from the first file
            if (n == 0) then
                !.. open the InpFile and check whether there is an error
                CALL H5Fopen_f(file_name, H5F_ACC_RDONLY_F, file_id, status)
                IF(status/=0) THEN
                    print *, 'There is a problem with H5Fopen_f() (opening the input file). The error code is:', status
                    STOP
                END IF

                call logger%begin_section('units')

                call readGroupAttribute_double(file_id, '/Header', 'HubbleParam', HubbleParam)
                call logger%log('hubble parameter h', '[]', '=', [HubbleParam])
                h_inv = 1/HubbleParam !.. will be dividing by the HubbleParam a lot, so just define it's inverse here

                call readGroupAttribute_double(file_id, '/Header', 'UnitLength_In_CGS', UnitLength_in_cm)
                call readGroupAttribute_double(file_id, '/Header', 'UnitVelocity_In_CGS', UnitVelocity_in_cm_per_s)
                call readGroupAttribute_double(file_id, '/Header', 'UnitMass_In_CGS', UnitMass_in_g)
                UnitLength_in_cm = UnitLength_in_cm*HubbleParam
                UnitMass_in_g = UnitMass_in_g*HubbleParam
                UnitTime_in_s = UnitLength_in_cm/UnitVelocity_in_cm_per_s
                UnitEnergy_in_cgs = UnitMass_in_g*UnitLength_in_cm**2/UnitTime_in_s**2
                UnitDensity_in_cgs = UnitMass_in_g/UnitLength_in_cm**3
                UnitMass_in_Msol = UnitMass_in_g * gToMsol

                call logger%log('length', '[cm]', '=', [UnitLength_in_cm])
                call logger%log('velocity', '[cm s^-1]', '=', [UnitVelocity_in_cm_per_s])
                call logger%log('mass', '[Msol]', '=', [UnitMass_in_Msol])
                call logger%log('time', '[s]', '=', [UnitTime_in_s])
                call logger%log('energy', '[g cm^2 s^-2]', '=', [UnitEnergy_in_cgs])
                call logger%log('density', '[g cm^3]', '=', [UnitDensity_in_cgs])

                call logger%end_section ! units

                call logger%begin_section('snapshot')

                call readGroupAttribute_double(file_id, '/Header', 'HubbleParam', HubbleParam)
                call logger%log('hubble parameter h', '[]', '=', [HubbleParam])
                h_inv = 1/HubbleParam !.. will be dividing by the HubbleParam a lot, so just define it's inverse here

                call readGroupAttribute_double(file_id, '/Header', 'Omega_Baryon', OmegaBaryon)
                call logger%log('omega_baryon', '[]', '=', [OmegaBaryon])

                call readGroupAttribute_double(file_id, '/Header', 'Redshift', Redshift)
                call logger%log('redshift', 'z', '=', [Redshift])

                aExp = 1d0/(1d0 + redshift) !.. calculate expansion factor
                call logger%log('expansion factor', '[a]', '=', [aExp])

                call readGroupAttribute_double(file_id, '/Header', 'BoxSize', ComovingBoxSize)
                ComovingBoxSize = ComovingBoxSize * UnitLength_in_cm * cmToMpc * h_inv
                call logger%log('l_box', '[cMpc]', '=', [ComovingBoxSize])

                rho_crit_0_cgs = (3.d0*(hubbleConst)**2)/(8.d0*pi*gravityConst)
                call logger%log('rho_crit (z = 0)', '[g cm^-3]', '=', [rho_crit_0_cgs])

                ave_baryonic_rho = OmegaBaryon*rho_crit_0_cgs !
                call logger%log('rho_baryon (z = 0)', '[g cm^-3]', '=', [ave_baryonic_rho])

                cell_volume = (ComovingBoxSize/cmToMpc)**3/cell_n**3 !.. units: comoving cm^3
                ave_grid_mass = cell_volume * ave_baryonic_rho * gToMsol
                call logger%log('<mass contained in cell>', '[Msol]', '=', [ave_grid_mass])

                call readGroupAttribute_integer(file_id, '/Header', 'NumPart_Total', gas_n_internal)
                !.. really irritating, main expects an integer*8, readGroupAttribute_integer, can't handle one
                gas_n = int(gas_n_internal, 8)
                call logger%log('# of gas particles in whole box', ' ', '=', [gas_n])

                !.. allocate all the necessary arrays
                if(single_prec) then
                    allocate(pos_gasAll(3, gas_n), &
                        vel_gasAll(3, gas_n), &
                        u_gasAll(gas_n), &
                        T_gasAll(gas_n), &
                        elecAbundance_gasAll(gas_n), &
                        rho_gasAll(gas_n), &
                        mass_gasAll(gas_n), &
                        neutralFrac_gasAll(gas_n), &
                        smoothing_gasAll(gas_n), &
                        metallicity_gasAll(11,gas_n))
                else
                    allocate(pos_gas_dp(3, gas_n))
                end if

                ! close the file
                CALL H5Fclose_f(file_id, status)
            end if

            !.. load the particle attirbutes from all the files into the relavant arrays
            !.. open the InpFile and check whether there is an error
            CALL H5Fopen_f(file_name, H5F_ACC_RDONLY_F, file_id, status)
            IF(status/=0) THEN
                print *, 'There is a problem with H5Fopen_f() (opening the input file). The error code is:', status
                STOP
            END IF

            !.. obtaining the amount of particles in individual file
            !.. for some reason NumPart_ThisFile read via readGroupAttribute_integer always throws an error
            !.. so it is read in the following ssection
            !#############################
            gr_name = "/Header"
            attr_name = "NumPart_ThisFile"

            CALL H5Gopen_f(file_id, gr_name, gr_id, status)
            IF(status/=0) THEN
                print *, 'There is a problem with H5Gopen_f() (opening the Group). The error code is:', status
                STOP
            END IF
            !.. Open the attribute and check whether there is an error
            CALL h5aopen_name_f(gr_id, attr_name, attr_id, status)
            IF(status/=0) THEN
                print *, 'There is a problem with H5aopen_f() (opening the Group). The error code is:', status
                STOP
            END IF
            dims_attr = 1
            CALL h5aread_f(attr_id, H5T_NATIVE_INTEGER, gas_n_file, dims_attr, status)
            !.. technically the NumPart_ThisFile attribute should be closed. However, always throws an error, so are not closing it
            !properly for now
            !CALL H5aclose_f(attr_id, status)
            CALL H5Gclose_f(gr_id, status)
            !#############################

            !.. loading the attributes of the gas particles into the appropriate sections of the target arrays
            call readParticleAttribute_2D(file_id, "/PartType0", "/PartType0/Coordinates", pos_gasAll(:,n_loaded+1:n_loaded+gas_n_file),3)
            call readParticleAttribute_2D(file_id, "/PartType0", "/PartType0/Velocities", vel_gasAll(:,n_loaded+1:n_loaded+gas_n_file),3)
            call readParticleAttribute_1D(file_id, "/PartType0", "/PartType0/InternalEnergy", u_gasAll(n_loaded+1:n_loaded+gas_n_file))
            call readParticleAttribute_1D(file_id, "/PartType0", "/PartType0/Temperature", T_gasAll(n_loaded+1:n_loaded+gas_n_file))
            call readParticleAttribute_1D(file_id, "/PartType0", "/PartType0/ElectronAbundance", elecAbundance_gasAll(n_loaded+1:n_loaded+gas_n_file))
            call readParticleAttribute_1D(file_id, "/PartType0", "/PartType0/NeutralHydrogenAbundance", neutralFrac_gasAll(n_loaded+1:n_loaded+gas_n_file))
            call readParticleAttribute_1D(file_id, "/PartType0", "/PartType0/Density", rho_gasAll(n_loaded+1:n_loaded+gas_n_file))
            call readParticleAttribute_1D(file_id, "/PartType0", "/PartType0/Masses", mass_gasAll(n_loaded+1:n_loaded+gas_n_file))
            call readParticleAttribute_1D(file_id, "/PartType0", "/PartType0/SmoothingLength", smoothing_gasAll(n_loaded+1:n_loaded+gas_n_file))
            call readParticleAttribute_2D(file_id, "/PartType0", "/PartType0/Metallicity", metallicity_gasAll(:,n_loaded+1:n_loaded+gas_n_file),11)

            ! close the file
            CALL H5Fclose_f(file_id, status)

            !.. update the amount of particle that have already been loaded
            n_loaded = n_loaded + gas_n_file

            n = n + 1 !.. update the filecount
      end do

      !.. close the HDF5 interface
      CALL H5close_f(status)

    call logger%begin_section('precise_cut')

    counter = 1

    !.. only need to bother looping through particles if cut_le and cut_re are non-default
    if(any(cut_le .ne. -1e9) .and. any(cut_re .ne. 1e9)) then
        !.. convert cut_le and cut_re to the same units as pos_gasAll (unit of cuts is cMpc/h)
        cut_le = cut_le * (1/cmToMpc) * (1/UnitLength_in_cm)
        cut_re = cut_re * (1/cmToMpc) * (1/UnitLength_in_cm) 

        do i = 1, gas_n
            if (all(pos_gasAll(:, i) >= cut_le) .and. all(pos_gasAll(:, i) <= cut_re)) then
                if (counter < i) then
                    pos_gasAll(:, counter) = pos_gasAll(:, i)
                    vel_gasAll(:, counter) = vel_gasAll(:, i)
                    u_gasAll(counter) = u_gasAll(i)
                    T_gasAll(counter) = T_gasAll(i)
                    elecAbundance_gasAll(counter) = elecAbundance_gasAll(i)
                    neutralFrac_gasAll(counter) = neutralFrac_gasAll(i)
                    rho_gasAll(counter) = rho_gasAll(i)
                    mass_gasAll(counter) = mass_gasAll(i)
                    smoothing_gasAll(counter) = smoothing_gasAll(i)
                    metallicity_gasAll(:,counter) = metallicity_gasAll(:,i)
                end if 
                counter = counter + 1
            end if 
        end do
        
        call logger%log('# of removed gas particles', '', '=', [gas_n - counter + 1])
        gas_n = counter - 1

    end if

    !.. perform quick check
    if(gas_n .eq. 0) then
        print *, 'Due to the choice of cut_le and cut_re all particles have been removed. => STOPPING P2C'
        STOP
    end if

    call logger%log('# of gas particles', '', '=', [gas_n])

    !.. allocate the arrays to hold the cut particles
    allocate(pos_gas(3, gas_n), &
                vel_gas(3, gas_n), &
                u_gas(gas_n), &
                elecAbundance_gas(gas_n), &
                neutralFrac_gas(gas_n), &
                rho_gas(gas_n), &
                T_gas(gas_n), &
                mass_gas(gas_n), &
                smoothing_gas(gas_n), &
                metallicity_gas(11,gas_n))
   
    !.. transfer the cut particles
    pos_gas(:,:) = pos_gasAll(:, 1:gas_n)
    vel_gas(:,:) = vel_gasAll(:, 1:gas_n)
    u_gas(:) = u_gasAll(1:gas_n)
    T_gas(:) = T_gasAll(1:gas_n)
    elecAbundance_gas(:) = elecAbundance_gasAll(1:gas_n)
    neutralFrac_gas(:) = neutralFrac_gasAll(1:gas_n)
    rho_gas(:) = rho_gasAll(1:gas_n)
    mass_gas(:) = mass_gasAll(1:gas_n)
    smoothing_gas(:) = smoothing_gasAll(1:gas_n)
    metallicity_gas(:,:) = metallicity_gasAll(:,1:gas_n)

    deallocate(pos_gasAll, vel_gasAll, u_gasAll, elecAbundance_gasAll, neutralFrac_gasAll,rho_gasAll, &
        T_gasAll, mass_gasAll, smoothing_gasAll,metallicity_gasAll)

    !.. if a cut has been performed, need to redefine the boxsize, due to the check in line 480
    !.. counter .eq. 1 can at this point in the code only that no cut was performed
    if(counter .eq. 1) then
        ComovingBoxOrigin = 0.0
    else
        !.. determine the origin of the box
        do i = 1, 3
            ComovingBoxOrigin(i) = minval(pos_gas(i,:))
        end do
        !.. determine the sidelength of box based on pos_gas
        do i = 1, 3
            dx(i) = maxval(pos_gas(i,:)) - minval(pos_gas(i,:))
        end do

        !.. caluculate ratio of whole box size and the cut box size to rescale the ave_grid_mass with
        mu = (maxval(dx) * UnitLength_in_cm *cmToMpc *h_inv)/ComovingBoxSize

        !.. calculate the cut boxsize in units of cMpc
        ComovingBoxSize = maxval(dx) * UnitLength_in_cm *cmToMpc *h_inv

        !.. translate the coordinates
        do i = 1, gas_n
            pos_gas(:, i) = pos_gas(:, i) - ComovingBoxOrigin
        end do

        ComovingBoxOrigin = ComovingBoxOrigin * h_inv

        call logger%log('new boxsize', '[cMpc]', '=', [ComovingBoxSize])
        call logger%log('origin of the new box', '[ckpc]', '=', [ComovingBoxOrigin])

    end if

    call logger%end_section !.. precise_cut

    call logger%begin_section('coordinates')

    !.. convert pos_gas to units of boxlength 
    !.. i.e. first convert to comoving Mpc, then divide by comoving boxsize in Mpc
    pos_gas = (pos_gas * UnitLength_in_cm * cmToMpc * h_inv) / ComovingBoxSize

    call logger%log('x_min', '[boxsize]', '=', [minval(pos_gas(1, :))])
    call logger%log('x_max', '[boxsize]', '=', [maxval(pos_gas(1, :))])
    call logger%log('y_min', '[boxsize]', '=', [minval(pos_gas(2, :))])
    call logger%log('y_max', '[boxsize]', '=', [maxval(pos_gas(2, :))])
    call logger%log('z_min', '[boxsize]', '=', [minval(pos_gas(3, :))])
    call logger%log('z_max', '[boxsize]', '=', [maxval(pos_gas(3, :))])

    call logger%log('histogram')

    coor_hist = plotter_histogram(pos_gas, 80, plid%linear, normalized=.true.)

    call canvas%add_axis(plid%left, 6, [0e0, maxval(coor_hist%counts)], &
                           scale=plid%linear, label='Pos histogram', color=colors%indigo)

    call canvas%add_axis(plid%bottom, 8, &
                           [minval(pos_gas), maxval(pos_gas)], &
                           scale=plid%linear, label='Position [boxsize]', color=colors%indigo)

    call coor_hist%draw_on(canvas, xaxis=plid%bottom, yaxis=plid%left, color=colors%indigo)
    call canvas%plot
    call canvas%clear
    call logger%end_section ! coordinate

    call logger%begin_section('velocities')

    !.. convert vel_gas to units of boxlength
    vel_gas = vel_gas * UnitVelocity_in_cm_per_s * cmToKm

    call logger%log('vx_min', '[km s^-1 a^0.5]', '=', [minval(vel_gas(1, :))])
    call logger%log('vx_max', '[km s^-1 a^0.5]', '=', [maxval(vel_gas(1, :))])
    call logger%log('vy_min', '[km s^-1 a^0.5]', '=', [minval(vel_gas(2, :))])
    call logger%log('vy_max', '[km s^-1 a^0.5]', '=', [maxval(vel_gas(2, :))])
    call logger%log('vz_min', '[km s^-1 a^0.5]', '=', [minval(vel_gas(3, :))])
    call logger%log('vz_max', '[km s^-1 a^0.5]', '=', [maxval(vel_gas(3, :))])

    call logger%log('histograms')

    do hist_id = 1, 3
        vel_hist = plotter_histogram(vel_gas(hist_id, :), 80, plid%linear, normalized=.true.)

        write (hist_label, '(A4,A1,A)') 'Vel ', vel_labels(hist_id), ' histogram'
        call canvas%add_axis(plid%left, 6, [0e0, maxval(vel_hist%counts)], &
                              scale=plid%linear, label=hist_label, color=colors%indigo)

        write (hist_label, '(A2,A1,A)') 'v_', vel_labels(hist_id), ' [km s^-1 a^0.5]'
        call canvas%add_axis(plid%bottom, 8, &
                              [minval(vel_gas(hist_id, :)), maxval(vel_gas(hist_id, :))], &
                              scale=plid%linear, label=hist_label, color=colors%indigo)

        call vel_hist%draw_on(canvas, xaxis=plid%bottom, yaxis=plid%left, color=colors%indigo)

        call canvas%plot
        call canvas%clear
    end do

    call logger%end_section ! velocity

    call logger%begin_section('masses')

    !.. convert to Msol
    mass_gas = mass_gas * UnitMass_in_Msol * h_inv

    total_mass_gas = 0
    DO i =1,gas_n
    total_mass_gas = total_mass_gas + mass_gas(i)
    END DO
    call logger%log('Total_Mass_Gizmo', '[Msol]', '=', [total_mass_gas])

    !.. deal with any particles with mass_gas = 0.

    call logger%log('mass_min', '[Msol]', '=', [minval(mass_gas)])
    call logger%log('mass_max', '[Msol]', '=', [maxval(mass_gas)])

    value_floor = minval(mass_gas, MASK=mass_gas > 0.)
    call logger%log('', 'mass_floor', '=', [value_floor])

    call logger%warn('Applying mass floor')
    WHERE (mass_gas == 0.) mass_gas = value_floor

    !.. rescale ave_grid_mass to the correct boxsize (if a cut was performed, ave_grid is too high 
    ! for the cell size and all quantities saved to the grid will be too low by a factor of mu^3)
    !.. if no cut was performed, mu = 1
    call logger%log('ave_grid_mass_before', '[Msol]', '=', [ave_grid_mass])
    ave_grid_mass = ave_grid_mass * mu**3
    !.. normalize by the average mass contained in a cell
    mass_gas = mass_gas / ave_grid_mass
    call logger%log('ave_grid_mass_after', '[Msol]', '=', [ave_grid_mass])

    call logger%log('mass_min', '[normalized by <cell_mass>]', '=', [minval(mass_gas)])
    call logger%log('mass_max', '[normalized by <cell_mass>]', '=', [maxval(mass_gas)])

    call logger%end_section

    call logger%begin_section('smoothing_length')

    !.. convert smoothing_gas to units of boxlength
    smoothing_gas = (smoothing_gas * UnitLength_in_cm * cmToMpc * h_inv) / ComovingBoxSize

    value_floor = minval(smoothing_gas, MASK=smoothing_gas > 0.)
    call logger%log('', 'smoothing_length_floor', '=', [value_floor])

    call logger%warn('Applying smoothing length floor')
    WHERE (smoothing_gas == 0.) smoothing_gas = value_floor

    !.. for now, setting a smoothing lenght that depends on the anount of cells in the basegrid
    !smoothing_gas = (3.5/cell_n) !.. corresponds to 3.5 cells

    call logger%log('smoothing_length_min', '[boxsize]', '=', [minval(smoothing_gas)])
    call logger%log('smoothing_length_max', '[boxsize]', '=', [maxval(smoothing_gas)])

    call logger%end_section !..smoothing_length

    call logger%begin_section('density')

    rho_gas = rho_gas * UnitDensity_in_cgs * HubbleParam**2 !.. convert density to comoving g/cm**3
    !.. the units of gas_rho are comoving cgs at this point in the code, so to obtain units of overdensity
    !.. can divide by ave_baryonic_rho(@z=0)
    rho_gas = rho_gas / ave_baryonic_rho

    value_floor = minval(rho_gas, MASK=rho_gas > 0.)
    call logger%log('', 'density_floor', '=', [value_floor])

    call logger%warn('Applying density floor')
    WHERE (rho_gas == 0.) rho_gas = value_floor

    call logger%log('den_min', '[overdensity]', '=', [minval(rho_gas)])
    call logger%log('den_max', '[overdensity]', '=', [maxval(rho_gas)])

    if(minval(rho_gas) .eq. maxval(rho_gas)) then
        call logger%warn('All density values are identical, no point plotting histogram')

    else

        rho_hist = plotter_histogram(rho_gas, 80, plid%log, normalized=.true.)
        !rho_hist = plotter_histogram(rho_gas, 80, plid%linear, normalized=.true.)

        call canvas%add_axis(plid%left, 6, [0e0, maxval(rho_hist%counts)], &
                           scale=plid%linear, label='Density histogram', color=colors%indigo)

        call canvas%add_axis(plid%bottom, 8, &
                           [minval(rho_gas), maxval(rho_gas)], &
                           scale=plid%log, label='Density [overdensity]', color=colors%indigo)

        call rho_hist%draw_on(canvas, xaxis=plid%bottom, yaxis=plid%left, color=colors%indigo)
        call canvas%plot
        call canvas%clear
    end if


    call logger%end_section    !.. density

    call logger%begin_section('electronAbundance')

    !elecAbundance_gas = 1.157

    call logger%log('electronAbundance_min', '[]', '=', [minval(elecAbundance_gas)])
    call logger%log('electronAbundance_max', '[]', '=', [maxval(elecAbundance_gas)])

    call logger%end_section !.. electronAbundance

    call logger%begin_section('internal energy temperature')

    !..convert u_gas from specific internal energy [???] to temperature [K?]
    u_gas = u_gas*4.0/(3*HFrac + 1.+4*HFrac*elecAbundance_gas)*mproton/kB*(gamma - 1)*UnitEnergy_in_cgs/UnitMass_in_g ! Temperature in K
    !u_gas = u_gas*4.0/(3*HFrac + 1.+4*HFrac*1.157)*mproton/kB*(gamma - 1)*UnitEnergy_in_cgs/UnitMass_in_g ! Temperature in K

    call logger%log('temp_min', '[K?]', '=', [minval(u_gas)])
    call logger%log('temp_max', '[K?]', '=', [maxval(u_gas)])

    call logger%log('histogram')

    temp_u_hist = plotter_histogram(u_gas, 80, plid%log, normalized=.true.)

    call canvas%add_axis(plid%left, 6, [0e0, maxval(temp_u_hist%counts)], &
                           scale=plid%linear, label='Temp histogram', color=colors%indigo)

    call canvas%add_axis(plid%bottom, 8, &
                           [minval(u_gas), maxval(u_gas)], &
                           scale=plid%log, label='Temperature [K?]', color=colors%indigo)

    call temp_u_hist%draw_on(canvas, xaxis=plid%bottom, yaxis=plid%left, color=colors%indigo)
    call canvas%plot
    call canvas%clear

    call logger%end_section ! temperature

    call logger%begin_section('temperature')

    call logger%log('temp_min', '[K]', '=', [minval(T_gas)])
    call logger%log('temp_max', '[K]', '=', [maxval(T_gas)])

    call logger%log('histogram')

    temp_hist = plotter_histogram(T_gas, 80, plid%log, normalized=.true.)

    call canvas%add_axis(plid%left, 6, [0e0, maxval(temp_hist%counts)], &
                           scale=plid%linear, label='Temp histogram', color=colors%indigo)

    call canvas%add_axis(plid%bottom, 8, &
                           [minval(T_gas), maxval(T_gas)], &
                           scale=plid%log, label='Temperature [K]', color=colors%indigo)

    call temp_hist%draw_on(canvas, xaxis=plid%bottom, yaxis=plid%left, color=colors%indigo)
    call canvas%plot
    call canvas%clear

    call logger%end_section ! temperature

    !deallocate(pos_gas, vel_gas, u_gas, rho_gas, neutralFrac_gas, T_gas,  mass_gas, smoothing_gas)
    deallocate(elecAbundance_gas)

    call logger%end_section ! snapshot
    call logger%end_section(print_duration=.true.) ! gizmo

    END SUBROUTINE ReadGizmo
end module GIZMO
