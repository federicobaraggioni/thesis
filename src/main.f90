! Converts a particle-based snapshot (gadget2/tipsy) into a single Chombo file
!
! version: 1.0
! Usage: P2C-1.0 -inp gadget_snap_directory/tipsy_file -out Chombo_file
!
! Compile with: (also take a look at the Makefile)
! mkdir -p build && cd build && cmake .. && make
!
! Author: SC
!
!------------------------------------------------------------------

PROGRAM P2C
   USE, INTRINSIC :: IEEE_ARITHMETIC

   USE HDF5
   USE H5LT
   USE amr
   USE SHERWOOD
   USE EAGLE
   USE AREPO
   USE GIZMO
   use logging
   use plotter

   IMPLICIT NONE

   CHARACTER(len=250) :: inpfile, dummy_string, chombofile, tipsyparam

   CHARACTER(len=250) :: groupname, attr_name, ftype, &
                         starfile, sb99

   INTEGER :: level, lmax, ndim, chombo_lev, lmin, &
              xx, yy, zz, nstep_coarse, &
              ierr, nvarh, ngrids, prob_domain(3), MaxNGrids, &
              i, C(3), j, k, id, ii, &
              size_basegrids, x, y, z, st1(8), delta, &
              st2(8), &
              st_in(8), &
              ncmin, npmin, px, py, pz, npref, CMin(3), CMax(3), nstars, &
              NFilledGrids, pC(3)
   INTEGER(kind=8) ::  ngas, totsize, totflag, totref

   LOGICAL :: inband, conv_star_gas

   !..gadget variables
   INTEGER :: npart(0:5), npartall(0:5), flag_sfr, &
              flag_feedback
   REAL(kind=8) :: massarr(0:5), gadget_redshIFt, gadget_time, &
                   GadgetBoxCenter(3), BoxOrig(3), ComovingBoxSize, ComovingOriginalBoxSize, ave_baryonic_den, &
                   recomb_coeff, ave_grid_mass
   REAL(kind=4), ALLOCATABLE :: pos_gas(:, :), ugas(:), rhogas(:), rhogas_entropy(:), &
                                massgas(:), nelecgas(:),neutralFrac_gas(:), vel_gas(:, :), &
                                hsml(:), wsmlMap(:, :, :), wsmlMap_(:, :, :), stmass(:), pos_stars(:, :), tform(:), &
                                stmetal(:), gas_metal(:),metallicity_gas(:,:)
   REAL(kind=4) :: dist2, r, sumw, sumw2, h, h_inv, minhsml, density_floor,density_floor_neutral, temp_floor, &
                   maxhsml, ThisCellSize, checkpos(3), sf_overdensity_threshold, sfr_density_Zdep, &
                   norm_massgas, sumw_thislev, dMSolUnit, dKpcUnit, tcode_to_Myr, &
                   SimTime, current_time, sim_tstep, min_sumw, photoHeatingFloor
   CHARACTER(len=10) :: units

#ifdef SP
   REAL(kind=4), ALLOCATABLE :: DataArray(:)
#else
   REAL(kind=8), ALLOCATABLE :: DataArray(:)
#endif

   REAL(kind=4) :: omega_m, omega_l, omega_b, &
                   ceff, ct1, ct2, ct_in, MeanBarDen, &
                   h0, maxstage, hubble
   REAL(kind=8) :: box_le(3), box_re(3), cut_re(3), cut_le(3), enforced_temperature, new_origin(3)
   CHARACTER(len=80) :: date, time, zone
   LOGICAL :: flag = .true., skipISM, stmref, calc_emissivity, recentre_box
   CHARACTER(len=80):: file_fmt
   INTEGER, ALLOCATABLE :: head(:), tail(:), &
                           twotolevarr(:)
   INTEGER(kind=4), ALLOCATABLE :: GridId(:), Hier_Down(:), Hier_Up(:), &
                                   Hier_Next(:)

   ! constant needed to calculate emissivity
   REAL(kind=4),PARAMETER :: T_H=157807.
   TYPE(AMRBoxList_type), ALLOCATABLE, DIMENSION(:) :: SubGrids

   TYPE Grid_type
      INTEGER :: Id, LE(3), RE(3), Level, Dims(3)

#ifdef SP
      REAL(kind=4), ALLOCATABLE :: Data(:, :, :, :)
#else
      REAL(kind=8), ALLOCATABLE :: Data(:, :, :, :)
#endif

      INTEGER, ALLOCATABLE :: flag(:, :, :), gdown(:, :, :)
      REAL(kind=4), ALLOCATABLE :: mw(:, :, :)
      TYPE(Grid_type), POINTER :: Next => NULL(), Up => NULL(), Down => NULL()
      REAL(kind=8) :: InvCellSize, CellSize
   END TYPE Grid_type

   TYPE(Grid_type), ALLOCATABLE, TARGET :: grid(:)
   TYPE(Grid_type), POINTER :: Curr, Parent

!..HDF5 types
   INTEGER(HID_T) :: file_id, dset_id, group_id, &
                     attr_id, dt1_id, dt2_id, space_id, datatype, &
                     str_type, comp_id, boxspace_id, &
                     dspace_id
   INTEGER(HSIZE_T), DIMENSION(7) :: dims
   INTEGER(HSIZE_T), PARAMETER :: attr_1d(1) = 1, attr_3d(1) = 3, dims_(1) = 6
   INTEGER(SIZE_T) :: type_size, str_size, compound_size
   INTEGER(HID_T) :: plist_id ! Dataset trasfer property
   INTEGER(HSIZE_T), DIMENSION(3), PARAMETER  :: offset_out = (/0, 0, 0/), &
                                                 count_out = (/0, 0, 0/)
   INTEGER(HSIZE_T) :: offset
   INTEGER :: err

   integer(hid_t) :: type_id, comp_type
   integer(size_t) :: element_size, tot_size, offset_pd
   integer(hsize_t) :: dims_pd(1) = 1
   integer :: prob_domain_values(6), hdferr

   ! Eagle SPH kernel
   real(kind=4) :: w_factor ! 8 / (pi h^3)
   real(kind=4) :: pi = 40*atan(1.0)
   real(kind=4) :: inv_pi

   ! Final average density
   real(kind=8) :: total_mass, total_volume, average_density, total_mass_gas
   integer :: number_of_cells, number_of_leaf_cells

   ! logging
   type(logging_t) :: logger
   character(len=1024) :: exe_filename
   type(plotter_canvas_t) :: canvas
   type(plotter_histogram_t) :: histogram
   integer :: hist_id
   character(len=128) :: hist_label
   character(len=8) :: level_number_str
   character(len=1) :: vel_labels(3) = ['x', 'y', 'z']

   inv_pi = 1.0/pi

   call canvas%init(80, 20)

   call get_command_argument(0, exe_filename)

   call logger%init('p2c')

   call logger%begin_section('ツ')
   call logger%log('command line argument:', 'exe', '=', [exe_filename])
   call logger%log('', 'log_file', '=', [logger%logfile])
   call logger%log('', 'err_file', '=', [logger%errfile])
   call logger%end_section

#ifdef SP
   call logger%warn('Working in single precision!')
#else
   call logger%warn('Working in double precision!')
#endif

   CALL read_param

   IF (file_fmt == 'gadgetfmt') THEN
      CALL ReadGadget(inpfile)
   ELSE IF (file_fmt == 'tipsyfmt') THEN
      IF (TRIM(tipsyparam) == "??") THEN
         call logger%err('Unknown tispy parameter file (option -tpar <fname>)!')
         stop
      END IF

      IF (h0 == -1) THEN
         h0 = 0.73
         call logger%warn('H0 value has been changed!', 'H0', '=', [h0])
      END IF

      IF (omega_b == -1) THEN
         omega_b = 0.046
         call logger%warn('Omega_b value has been changed!', 'Omega_b', '=', [omega_b])
      END IF

      CALL ReadTipsy(inpfile, tipsyparam)
   ELSE IF (file_fmt == 'eaglefmt') THEN
      CALL READEAGLE(inpfile, pos_gas, vel_gas, ugas, rhogas, rhogas_entropy, massgas, hsml, gas_metal, stmass, &
                     pos_stars, tform, stmetal, ave_baryonic_den, gadget_redshIFt, gadget_time, BoxOrig, &
                     ComovingBoxSize, sf_overdensity_threshold, SimTime, ngas, nstars, &
                     size_basegrids, dMSolUnit, dkpcUnit, cut_le, cut_re, recentre_box, new_origin, calc_emissivity, &
                     delta, logger)
   ELSE IF (file_fmt == 'sherwoodfmt') THEN
      CALL readsherwood(inpfile, pos_gas, vel_gas, ugas, rhogas, massgas, hsml, stmass, &
                        pos_stars, tform, stmetal, gadget_redshIFt, BoxOrig, &
                        ComovingBoxSize, ComovingOriginalBoxSize, sf_overdensity_threshold, SimTime, ngas, nstars, &
                        size_basegrids, dMSolUnit, dkpcUnit, cut_le, cut_re, delta, hubble, conv_star_gas)
   ELSE IF (file_fmt == 'arepofmt') THEN
      call ReadArepo(inpfile, pos_gas, vel_gas, ugas, rhogas, rhogas_entropy, massgas, hsml, &
                        ave_baryonic_den, gadget_redshIFt, gadget_time, BoxOrig, ComovingBoxSize, ngas, &
                        size_basegrids, cut_le, cut_re, logger)
      sf_overdensity_threshold = (10.0 * 1.6735575e-24) / (ave_baryonic_den*(1 + gadget_redshIFt)**3) !in comoving cgs/ave_rho_baryon(z=0)
      nstars = 1
   ELSE IF (file_fmt == 'gizmofmt') THEN
      call ReadGizmo(inpfile, pos_gas, vel_gas, ugas, rhogas, neutralFrac_gas,metallicity_gas,massgas, hsml, ave_baryonic_den, &
                        gadget_redshIFt, gadget_time, BoxOrig, ComovingBoxSize, ngas, size_basegrids, cut_le, cut_re, ave_grid_mass, logger)
                !here ugas is directly the temperature read from gizmo, not the one obtained from internal energy
      allocate(rhogas_entropy(ngas))
      rhogas_entropy = rhogas     
      sf_overdensity_threshold = (10.0 * 1.6735575e-24) / (ave_baryonic_den*(1 + gadget_redshIFt)**3) !in comoving cgs/ave_rho_baryon(z=0)
      nstars = 1
   ELSE
      call logger%err('Unknonw file format!', 'format', '=', [file_fmt])
      stop
   END IF

   call logger%begin_section('base_grid')
   prob_domain = size_basegrids
   call logger%log('problem domain', '', '=', [prob_domain])

   call logger%begin_section('allocating')

   IF (nstars > 0) THEN
      IF (TRIM(starfile) /= "??") THEN
         nvarh = 8  ! 6=nphotons, 7=bb-temperature
      ELSE
         IF (calc_emissivity) THEN
            nvarh = 11 ! 1=den, 2=velx, 3=vely, 4=velz, 5=temp, 6=stars, 7=emissivity, 8=den**2weighted velx, 9=den**2weighted vely, 10=den**2weighted velz
         ELSE
            nvarh = 7  ! 6=stars
         END IF
      END IF
   ELSE
      nvarh = 6
   END IF
   call logger%log('# of variables', '', '=', [nvarh])

   MaxNGrids = 100000000
   NFilledGrids = 1

   call logger%log('grid array', 'len', '=', [MaxNGrids])

   ALLOCATE (grid(MaxNGrids))
   ALLOCATE (grid(1)%flag(size_basegrids, size_basegrids, size_basegrids))
   ALLOCATE (grid(1)%gdown(size_basegrids, size_basegrids, size_basegrids))
   ALLOCATE (grid(1)%mw(size_basegrids, size_basegrids, size_basegrids))
   ALLOCATE (grid(1)%Data(nvarh, size_basegrids, size_basegrids, size_basegrids))
   grid(1)%Id = 1
   grid(1)%Level = 0
   grid(1)%gdown = -1
   grid(1)%mw = 0.
   grid(1)%LE = 1
   grid(1)%RE = size_basegrids
   grid(1)%Dims(:) = size_basegrids
   grid(1)%CellSize = 1.d0/size_basegrids
   grid(1)%InvCellSize = REAL(size_basegrids, KIND=8)

   grid(1)%flag = 0
   grid(1)%Data = 0.
   call logger%end_section(print_duration=.true.) ! allocating

   call logger%begin_section('builing')

   maxhsml = maxval(hsml)*size_basegrids

   call logger%log('max smoothing length', '[cells]', '=', [maxhsml])
   call logger%log('min smoothing length (read from parameters)', '[cells]', '=', [minhsml])
   call logger%warn('snapshot min smoothing length (not effective, just as a reference)', &
                    '[cells]', '=', [minval(hsml)*size_basegrids])

   call logger%log('allocating wsmlMap array for base grid', 'len', '=', [INT(2*maxhsml) + 1])
   ALLOCATE (wsmlMap(INT(2*maxhsml) + 1, INT(2*maxhsml) + 1, INT(2*maxhsml) + 1))

   ! impose temperature floor due to photo-heating
   WHERE(ugas < photoHeatingFloor) ugas = photoHeatingFloor
   DO i = 1, ngas
      IF (calc_emissivity) THEN
          if (file_fmt == 'arepofmt' .or. file_fmt == 'gizmofmt') then
              sfr_density_Zdep = sf_overdensity_threshold
          else
              !sfr_density_Zdep = (0.1*(0.002/gas_metal(i))**0.64)*(1.6735575e-24)*0.752/(ave_baryonic_den*(1 + gadget_redshIFt)**3)
              sfr_density_Zdep = (1.0)*(1.6735575e-24)*0.752/(ave_baryonic_den*(1 + gadget_redshIFt)**3)
              ! the SFthreshold as used in the EAGLE simulation converted into units of overdensity
          end if
         IF (skipISM .and. rhogas(i) > sfr_density_Zdep) CYCLE ! skip particles in the ISM
      ELSE
         IF (skipISM .and. rhogas(i) > sf_overdensity_threshold) CYCLE ! skip particles in the ISM
      END IF

      !.. the min is to deal with the edge cases where pos_gas = bosxize
      pC = MIN(INT(pos_gas(:, i)*size_basegrids) + 1, size_basegrids)
      grid(1)%flag(pC(1), pC(2), pC(3)) = grid(1)%flag(pC(1), pC(2), pC(3)) + 1

      ! If h is smaller than a cell, r will become very large and it does not make sense
      ! anymore, because r will be very sensitive to the position of the particle within the cell
      h = MAX(hsml(i)*size_basegrids, minhsml)

      h_inv = 1./h

      !..get w_factor (cheaper calculation)
      w_factor = 8.*inv_pi*h_inv*h_inv*h_inv

      !..share particle between cells
      !CMin(:) = INT(pos_gas(:, i)*size_basegrids - h) + 1
      CMin(:) = pC(:) - INT(h)
      WHERE (CMin < 1) CMin = 1
      !CMax(:) = CMin(:) + INT(h) + 1
      CMax(:) = pC(:) + INT(h)            ! +1 unnecessary
      WHERE (CMax > size_basegrids) CMax = size_basegrids !

      !..check wsmlMAP boundaries, currently used for debugging purposes, ro be eventually removed
      !IF (ANY(CMax(:) - CMin(:) > INT(2*maxhsml))) STOP "wsmlMAP boundary problem!"

      !..compute sumweight
      sumw = 0.
      sumw2 = 0.
      DO z = CMin(3), CMax(3)
         DO y = CMin(2), CMax(2)
            DO x = CMin(1), CMax(1)
               C = [x, y, z]
               !dist2 = SUM((pos_gas(:, i)*size_basegrids + 0.5 - C)**2) ! 0.5 = 1-0.5, moved back to REAL calculation
               dist2 = SUM((pC(:) - C(:))**2)
               IF (dist2 <= h*h) THEN
                  r = sqrt(dist2)*h_inv
                  IF (r <= .5) THEN
                     IF (file_fmt == 'gadgetfmt') THEN
                        wsmlMap(x - CMin(1) + 1, y - CMin(2) + 1, z - CMin(3) + 1) = 2.546479089470 + 15.278874536822*(r - 1.0)*r*r
                     ELSE IF (file_fmt == 'eaglefmt' .or. file_fmt == 'sherwoodfmt' .or. file_fmt == 'arepofmt' & 
                              .or. file_fmt == 'gizmofmt') THEN
                        wsmlMap(x - CMin(1) + 1, y - CMin(2) + 1, z - CMin(3) + 1) = w_factor*(1 - 6*r*r + 6*r*r*r)
                     ELSE
                        call logger%err('Unknown kernel format!', 'format', '=', [file_fmt])
                     END IF
                     sumw = sumw + wsmlMap(x - CMin(1) + 1, y - CMin(2) + 1, z - CMin(3) + 1)
                     sumw2 = sumw2 + wsmlMap(x - CMin(1) + 1, y - CMin(2) + 1, z - CMin(3) + 1)**2
                  ELSEIF (r <= 1.) THEN
                     IF (file_fmt == 'gadgetfmt') THEN
                        wsmlMap(x - CMin(1) + 1, y - CMin(2) + 1, z - CMin(3) + 1) = 5.092958178941*(1.0 - r)*(1.0 - r)*(1.0 - r)
                     ELSE IF (file_fmt == 'eaglefmt' .or. file_fmt == 'sherwoodfmt' .or. file_fmt == 'arepofmt' &
                              .or. file_fmt == 'gizmofmt') THEN
                        wsmlMap(x - CMin(1) + 1, y - CMin(2) + 1, z - CMin(3) + 1) = w_factor*(2*(1 - r)**3)
                     ELSE
                        call logger%err('Unknown kernel format!', 'format', '=', [file_fmt])
                     END IF
                     sumw = sumw + wsmlMap(x - CMin(1) + 1, y - CMin(2) + 1, z - CMin(3) + 1)
                     sumw2 = sumw2 + wsmlMap(x - CMin(1) + 1, y - CMin(2) + 1, z - CMin(3) + 1)**2
                  ELSE
                     call logger%err('We should not be here!')
                     wsmlMap(x - CMin(1) + 1, y - CMin(2) + 1, z - CMin(3) + 1) = 0.
                  END IF
               ELSE
                  wsmlMap(x - CMin(1) + 1, y - CMin(2) + 1, z - CMin(3) + 1) = 0.
               END IF
            END DO
         END DO
      END DO

      IF (sumw == 0.) THEN
         call logger%err('Sum of weights is zero!', '(Cmin, Cmax)', '=', [Cmin, Cmax])
         stop
      ENDIF

      IF (sumw2 == 0.) THEN
         call logger%err('Sum of the squared weights is zero!', '(Cmin, Cmax)', '=', [Cmin, Cmax])
         stop
      ENDIF
      grid(1)%Data(1, CMin(1):CMax(1), CMin(2):CMax(2), CMin(3):CMax(3)) = &
         grid(1)%Data(1, CMin(1):CMax(1), CMin(2):CMax(2), CMin(3):CMax(3)) &
         + wsmlMap(1:CMax(1) - CMin(1) + 1, 1:CMAx(2) - CMin(2) + 1, 1:CMax(3) - CMin(3) + 1) &
         *massgas(i)/sumw
 
      IF (enforced_temperature == -1.) THEN
         grid(1)%Data(5, CMin(1):CMax(1), CMin(2):CMax(2), CMin(3):CMax(3)) = &
            grid(1)%Data(5, CMin(1):CMax(1), CMin(2):CMax(2), CMin(3):CMax(3)) &
            + wsmlMap(1:CMax(1) - CMin(1) + 1, 1:CMAx(2) - CMin(2) + 1, 1:CMax(3) - CMin(3) + 1) &
            *massgas(i)/sumw*ugas(i)
      ENDIF

      grid(1)%Data(2, CMin(1):CMax(1), CMin(2):CMax(2), CMin(3):CMax(3)) = &          !x-component of velocity
         grid(1)%Data(2, CMin(1):CMax(1), CMin(2):CMax(2), CMin(3):CMax(3)) &
         + wsmlMap(1:CMax(1) - CMin(1) + 1, 1:CMax(2) - CMin(2) + 1, 1:CMax(3) - CMin(3) + 1) &
         *massgas(i)/sumw*vel_gas(1, i)

      grid(1)%Data(3, CMin(1):CMax(1), CMin(2):CMax(2), CMin(3):CMax(3)) = &          !y-component of velocity
         grid(1)%Data(3, CMin(1):CMax(1), CMin(2):CMax(2), CMin(3):CMax(3)) &
         + wsmlMap(1:CMax(1) - CMin(1) + 1, 1:CMax(2) - CMin(2) + 1, 1:CMax(3) - CMin(3) + 1) &
         *massgas(i)/sumw*vel_gas(2, i)

      grid(1)%Data(4, CMin(1):CMax(1), CMin(2):CMax(2), CMin(3):CMax(3)) = &          !z-component of velocity
         grid(1)%Data(4, CMin(1):CMax(1), CMin(2):CMax(2), CMin(3):CMax(3)) &
         + wsmlMap(1:CMax(1) - CMin(1) + 1, 1:CMax(2) - CMin(2) + 1, 1:CMax(3) - CMin(3) + 1) &
         *massgas(i)/sumw*vel_gas(3, i)

      IF (calc_emissivity) THEN
         IF (ugas(i) < 1e3) CYCLE
         ! the effective case A (assuming gas is optically thin) Ly-alpha recombination coefficient
         !total recombination coefficient as taken from Hui & Gnedin 1997; Appendix A
         ! effective recombination coefficient = 0.35 * (total recombination coefficient Case A)
         ! units: [1.269e-13 cm^3/s]
         recomb_coeff = ((2.*T_H/ugas(i))**1.503)/(1.e0+((2.*T_H/ugas(i))/0.522)**0.47)**1.923
         recomb_coeff = recomb_coeff * 0.35

         grid(1)%Data(7, CMin(1):CMax(1), CMin(2):CMax(2), CMin(3):CMax(3)) = &          !emissivity of the particles =
            grid(1)%Data(7, CMin(1):CMax(1), CMin(2):CMax(2), CMin(3):CMax(3)) &         !(h_planck * Lyalpha_freq * recomb_coeff * n_den**2)/4*pi
            + wsmlMap(1:CMax(1) - CMin(1) + 1, 1:CMax(2) - CMin(2) + 1, 1:CMax(3) - CMin(3) + 1)**2 &
            * rhogas_entropy(i)**2  * recomb_coeff * (massgas(i)/rhogas(i)) / sumw2
        
         !.. the velocities weighted by the emissivity are calculated in addition to be used when generating the mock
         !observations
         grid(1)%Data(8, CMin(1):CMax(1), CMin(2):CMax(2), CMin(3):CMax(3)) = &          !x-component of velocity weighted by emissivity
            grid(1)%Data(8, CMin(1):CMax(1), CMin(2):CMax(2), CMin(3):CMax(3)) &
            + wsmlMap(1:CMax(1) - CMin(1) + 1, 1:CMax(2) - CMin(2) + 1, 1:CMax(3) - CMin(3) + 1)**2 &
            * rhogas_entropy(i)**2  * recomb_coeff * (massgas(i)/rhogas(i)) / sumw2 * vel_gas(1,i)

         grid(1)%Data(9, CMin(1):CMax(1), CMin(2):CMax(2), CMin(3):CMax(3)) = &          !y-component of velocity weighted by emissivity
            grid(1)%Data(9, CMin(1):CMax(1), CMin(2):CMax(2), CMin(3):CMax(3)) &
            + wsmlMap(1:CMax(1) - CMin(1) + 1, 1:CMax(2) - CMin(2) + 1, 1:CMax(3) - CMin(3) + 1)**2 &
            * rhogas_entropy(i)**2  * recomb_coeff * (massgas(i)/rhogas(i)) / sumw2 * vel_gas(2,i)

         grid(1)%Data(10, CMin(1):CMax(1), CMin(2):CMax(2), CMin(3):CMax(3)) = &          !z-component of velocity weighted by emissivity
            grid(1)%Data(10, CMin(1):CMax(1), CMin(2):CMax(2), CMin(3):CMax(3)) &
            + wsmlMap(1:CMax(1) - CMin(1) + 1, 1:CMax(2) - CMin(2) + 1, 1:CMax(3) - CMin(3) + 1)**2 &
            * rhogas_entropy(i)**2  * recomb_coeff * (massgas(i)/rhogas(i)) / sumw2 * vel_gas(3,i)

         grid(1)%Data(11, CMin(1):CMax(1), CMin(2):CMax(2), CMin(3):CMax(3)) = &
         grid(1)%Data(11, CMin(1):CMax(1), CMin(2):CMax(2), CMin(3):CMax(3)) &
         + wsmlMap(1:CMax(1) - CMin(1) + 1, 1:CMAx(2) - CMin(2) + 1, 1:CMax(3) - CMin(3) + 1) &
         *massgas(i)*neutralFrac_gas(i)*(1-metallicity_gas(1,i)-metallicity_gas(0,i))/sumw !(Fraction of neutral Hydrogen)(1-He-Other_metals)

      END IF
   END DO
   if (enforced_temperature /= -1.) then
      call logger%warn('Enforcing a temperature on all cells!', 'T', '=', [enforced_temperature])
      grid(1)%Data(5, :, :, :) = enforced_temperature
   end if

   call logger%begin_section('normalizing')
   call logger%log('temperature and velocities by the density')
   !.. also multiply the velocities by sqrt(a) to obtain the physical velocites
   IF (enforced_temperature == -1.) THEN
      where (grid(1)%Data(1, :, :, :) /= 0.)
      grid(1)%Data(5, :, :, :) = grid(1)%Data(5, :, :, :)/grid(1)%Data(1, :, :, :)
      end where
   ENDIF

   WHERE (grid(1)%Data(1, :, :, :) /= 0.)
   grid(1)%Data(2, :, :, :) = grid(1)%Data(2, :, :, :)/grid(1)%Data(1, :, :, :)*SQRT(1/(1 + gadget_redshIFt))
   grid(1)%Data(3, :, :, :) = grid(1)%Data(3, :, :, :)/grid(1)%Data(1, :, :, :)*SQRT(1/(1 + gadget_redshIFt))
   grid(1)%Data(4, :, :, :) = grid(1)%Data(4, :, :, :)/grid(1)%Data(1, :, :, :)*SQRT(1/(1 + gadget_redshIFt))
   ENDWHERE

   IF (calc_emissivity) THEN
      !.. divide the additional velocities by emissivity to get the correct weighting
      WHERE (grid(1)%Data(7, :, :, :) /= 0.)
      grid(1)%Data(8, :, :, :) = grid(1)%Data(8, :, :, :)/grid(1)%Data(7, :, :, :)*SQRT(1/(1 + gadget_redshIFt))
      grid(1)%Data(9, :, :, :) = grid(1)%Data(9, :, :, :)/grid(1)%Data(7, :, :, :)*SQRT(1/(1 + gadget_redshIFt))
      grid(1)%Data(10, :, :, :) = grid(1)%Data(10, :, :, :)/grid(1)%Data(7, :, :, :)*SQRT(1/(1 + gadget_redshIFt))
      END WHERE

      !WHERE (grid(1)%Data(1, :, :, :) /= 0.)
      !grid(1)%Data(11, :, :, :) = grid(1)%Data(11, :, :, :)/grid(1)%Data(1, :, :, :)
      !ENDWHERE

   END IF

   call logger%end_section ! normalizing

   call logger%end_section(print_duration=.true.) ! building

   call logger%begin_section('overdensity')
   call logger%log('overdensity', '(w.r.t average baryonic density at z=0)', '=', ['rho / <rho_b0>'])

   call logger%log('min', '', '=', [minval(grid(1)%Data(1, :, :, :))])
   call logger%log('max', '', '=', [maxval(grid(1)%Data(1, :, :, :))])

   density_floor = MINVAL(grid(1)%Data(1, :, :, :), MASK=grid(1)%Data(1, :, :, :) > 0.)
   call logger%log('', 'density_floor', '=', [density_floor])

   call logger%warn('Applying density floor')
   WHERE (grid(1)%Data(1, :, :, :) == 0.) grid(1)%Data(1, :, :, :) = density_floor

   call logger%log('histogram')

   histogram = plotter_histogram(real(grid(1)%Data(1, :, :, :)), 80, plid%log, normalized=.true.)

   call canvas%add_axis(plid%left, 6, [0e0, maxval(histogram%counts)], &
                        scale=plid%linear, label='histogram', color=colors%indigo)
   call canvas%add_axis(plid%bottom, 8, &
                        real([minval(grid(1)%Data(1, :, :, :)), maxval(grid(1)%Data(1, :, :, :))]), &
                        scale=plid%log, label='Overdensity [w.r.t average baryonic density at z=0]', color=colors%yellow)

   call histogram%draw_on(canvas, xaxis=plid%bottom, yaxis=plid%left, color=colors%indigo)

   call canvas%plot
   call canvas%clear

   call logger%begin_section('Neutral Mass')

   call logger%log('min', '', '=', [minval(grid(1)%Data(11, :, :, :))])
   call logger%log('max', '', '=', [maxval(grid(1)%Data(11, :, :, :))])

   density_floor_neutral = MINVAL(grid(1)%Data(11, :, :, :), MASK=grid(1)%Data(11, :, :, :) > 0.)
   call logger%log('', 'density_floor', '=', [density_floor_neutral])

   call logger%warn('Applying density floor')
   WHERE (grid(1)%Data(11, :, :, :) == 0.) grid(1)%Data(11, :, :, :) = density_floor_neutral

   histogram = plotter_histogram(real(grid(1)%Data(11, :, :, :)), 80, plid%log, normalized=.true.)

   call canvas%add_axis(plid%left, 6, [0e0, maxval(histogram%counts)], &
                        scale=plid%linear, label='histogram', color=colors%indigo)
   call canvas%add_axis(plid%bottom, 8, &
                        real([minval(grid(1)%Data(11, :, :, :)), maxval(grid(1)%Data(11, :, :, :))]), &
                        scale=plid%log, label='Neutral Hydrogen Mass [Average_grid_mass]', color=colors%yellow)

   call histogram%draw_on(canvas, xaxis=plid%bottom, yaxis=plid%left, color=colors%indigo)

   call canvas%plot
   call canvas%clear

   call logger%end_section ! density

   call logger%begin_section('velocities')

   !call logger%log('redshift', 'z', '=', [gadget_redshIFt])
   call logger%log('vx_min', '[km/s]', '=', [minval(grid(1)%Data(2, :, :, :))])
   call logger%log('vx_max', '[km/s]', '=', [maxval(grid(1)%Data(2, :, :, :))])
   call logger%log('vy_min', '[km/s]', '=', [minval(grid(1)%Data(3, :, :, :))])
   call logger%log('vy_max', '[km/s]', '=', [maxval(grid(1)%Data(3, :, :, :))])
   call logger%log('vz_min', '[km/s]', '=', [minval(grid(1)%Data(4, :, :, :))])
   call logger%log('vz_max', '[km/s]', '=', [maxval(grid(1)%Data(4, :, :, :))])
   IF(calc_emissivity) THEN
        call logger%log('min emissivity', '[??]', '=', [minval(grid(1)%Data(7, :, :, :))])
        call logger%log('max emissivity', '[??]', '=', [maxval(grid(1)%Data(7, :, :, :))])
        call logger%log('min em weighted velx', '[km/s]', '=', [minval(grid(1)%Data(8, :, :, :))])
        call logger%log('max em weighted velx', '[km/s]', '=', [maxval(grid(1)%Data(8, :, :, :))])
        call logger%log('min em weighted vely', '[km/s]', '=', [minval(grid(1)%Data(9, :, :, :))])
        call logger%log('max em weighted vely', '[km/s]', '=', [maxval(grid(1)%Data(9, :, :, :))])
        call logger%log('min em weighted velz', '[km/s]', '=', [minval(grid(1)%Data(10, :, :, :))])
        call logger%log('max em weighted velz', '[km/s]', '=', [maxval(grid(1)%Data(10, :, :, :))])
   ENDIF

   call logger%log('histograms')

   do hist_id = 2, 4
      histogram = plotter_histogram(real(grid(1)%Data(hist_id, :, :, :)), 80, plid%linear, normalized=.true.)

      write (hist_label, '(A4,A1,A)') 'Vel ', vel_labels(hist_id - 1), ' histogram'
      call canvas%add_axis(plid%left, 6, [0e0, maxval(histogram%counts)], &
                           scale=plid%linear, label=hist_label, color=colors%indigo)

      write (hist_label, '(A2,A1,A)') 'v_', vel_labels(hist_id - 1), ' [km/s]'
      call canvas%add_axis(plid%bottom, 8, &
                           real([minval(grid(1)%Data(hist_id, :, :, :)), maxval(grid(1)%Data(hist_id, :, :, :))]), &
                           scale=plid%linear, label=hist_label, color=colors%indigo)

      call histogram%draw_on(canvas, xaxis=plid%bottom, yaxis=plid%left, color=colors%indigo)

      call canvas%plot
      call canvas%clear
   end do

   call logger%end_section ! velocities

   call logger%begin_section('temperature')

   call logger%log('min', '[K]', '=', [minval(grid(1)%Data(5, :, :, :))])
   call logger%log('max', '[K]', '=', [maxval(grid(1)%Data(5, :, :, :))])

   IF (enforced_temperature == -1.) THEN
      temp_floor = MINVAL(grid(1)%Data(5, :, :, :), MASK=grid(1)%Data(5, :, :, :) > 0.)
      call logger%log('', 'temperature_floor', '=', [temp_floor])

      call logger%warn('Applying temperature floor')
      WHERE (grid(1)%Data(5, :, :, :) == 0.)
      grid(1)%Data(5, :, :, :) = temp_floor
      END WHERE

      call canvas%add_axis(plid%bottom, 8, &
                           real([minval(grid(1)%Data(5, :, :, :)), maxval(grid(1)%Data(5, :, :, :))]), &
                           scale=plid%log, label='T [K]', color=colors%indigo)
   ELSE
      call canvas%add_axis(plid%bottom, 8, &
                           real([enforced_temperature/2, enforced_temperature*2]), &
                           scale=plid%log, label='T [K]', color=colors%indigo)
   ENDIF

   call logger%log('histogram')

   histogram = plotter_histogram(real(grid(1)%Data(5, :, :, :)), 80, plid%log, normalized=.true.)

   call canvas%add_axis(plid%left, 6, [0e0, maxval(histogram%counts)], &
                        scale=plid%linear, label='Temp histogram', color=colors%indigo)

   call histogram%draw_on(canvas, xaxis=plid%bottom, yaxis=plid%left, color=colors%indigo)

   call canvas%plot
   call canvas%clear

   call logger%end_section ! temperature

   IF (lmax == -1) THEN
      lmax = INT(.9*log10(REAL(MAXVAL(grid(1)%flag)/npref)))
      call logger%warn('Max level of refinement has been changed!', 'l_max', '=', [lmax])
   ELSE
      IF (MAXVAL(grid(1)%flag) < 2.**(3*lmax)) THEN
         call logger%err('Max level of refinement is too hight!', 'suggestion', ':', &
                         [INT(.9*log10(REAL(MAXVAL(grid(1)%flag)/npref)))])
      END IF

      call logger%log('l_max', '', '=', [lmax])
   END IF

   call logger%log('Deactivating offset strips', '[currently only implemented for sherwood]')
   DO k = 1, grid(1)%Dims(3)
      DO j = 1, grid(1)%Dims(2)
         DO i = 1, grid(1)%Dims(1)
            IF (grid(1)%flag(i, j, k) > 0) THEN
               IF (grid(1)%flag(i, j, k) > npref) THEN
                  CALL in_strip(inband, i, j, k, size_basegrids, delta, cut_le, cut_re, ComovingOriginalBoxSize)
                  IF ((inband .eqv. .true.) .and. (file_fmt == "sherwoodfmt")) THEN
                     grid(1)%flag(i, j, k) = 0
                  ELSE
                     grid(1)%flag(i, j, k) = 1
                  ENDIF
               ELSE
                  grid(1)%flag(i, j, k) = 0
               END IF
            ENDIF
         END DO
      END DO
   END DO



   call logger%end_section(print_duration=.true.) ! base_grid

   !-------- Adaptive Mesh Refinement ------------------

   call logger%begin_section('AMR')

   call logger%log('initializing')
   CALL AMRinit()
   CALL AMRSetMax(100000000)
   CALL AMRSetMinNCells(ncmin)
   IF (ceff /= -1) CALL AMRSetClustEff(ceff)

   ALLOCATE (head(lmin - 1:lmax), tail(lmin - 1:lmax), twotolevarr(lmin:lmax))
   head(lmin - 1) = 0
   tail(lmin - 1) = 0
   head(lmin) = 1
   tail(lmin) = 1

   DO i = lmin, lmax
      twotolevarr(i) = 2**i
   END DO

   call logger%begin_section('level')

   DO level = lmin, lmax - 1, 1
      write (level_number_str, '(I1)') level + 1
      call logger%begin_section(level_number_str)

      ngrids = 0

      head(level + 1) = tail(level) + 1

      totref = 0
      totflag = 0

      !..calculate maxhsml for current level using only particles
      !..that have not been "deactived" in previous search, if available
      IF (level > lmin) THEN
         maxhsml = MAXVAL(hsml(:), MASK=massgas(:) /= -1)*size_basegrids*twotolevarr(level + 1)
      ELSE
         maxhsml = maxhsml*2  !..simply increases it by a factor of two
      END IF
      call logger%log('maxshml at current level', '[cell units]', '=', [maxhsml])

      !..reallocate wsmlMAP for this level
      DEALLOCATE (wsmlMAP)
      call logger%log('re-allocating wsmlMap array at current level', 'len', '=', [INT(2*maxhsml) + 1])
      ALLOCATE (wsmlMap(INT(2*maxhsml) + 1, INT(2*maxhsml) + 1, INT(2*maxhsml) + 1))

      call logger%begin_section('grids')

      call logger%begin_section('allocating')
      DO i = head(level), tail(level)

         IF (ALL(grid(i)%flag == 0)) CYCLE

         CALL AMRClustering(grid(i)%flag(:, :, :), SubGrids)

         grid(i)%Down => grid(tail(level) + ngrids + 1)

         totflag = totflag + COUNT(grid(i)%flag == 1)

         DO ii = 1, SIZE(SubGrids)
            id = tail(level) + ngrids + ii
            IF (id > MaxNGrids) then
               call logger%err('Unable to allocate, increase MaxNGrids!', id, '>', [MaxNGrids])
               STOP
            end if
            grid(id)%LE = (SubGrids(ii)%LE + grid(i)%LE - 1)*2 - 1
            grid(id)%RE = (SubGrids(ii)%RE + grid(i)%LE - 1)*2
            ALLOCATE (grid(id)%flag(grid(id)%RE(1) - grid(id)%LE(1) + 1, &
                                    grid(id)%RE(2) - grid(id)%LE(2) + 1, &
                                    grid(id)%RE(3) - grid(id)%LE(3) + 1))
            ALLOCATE (grid(id)%gdown(grid(id)%RE(1) - grid(id)%LE(1) + 1, &
                                     grid(id)%RE(2) - grid(id)%LE(2) + 1, &
                                     grid(id)%RE(3) - grid(id)%LE(3) + 1))
            ALLOCATE (grid(id)%mw(grid(id)%RE(1) - grid(id)%LE(1) + 1, &
                                  grid(id)%RE(2) - grid(id)%LE(2) + 1, &
                                  grid(id)%RE(3) - grid(id)%LE(3) + 1))
            ALLOCATE (grid(id)%Data(nvarh, grid(id)%RE(1) - grid(id)%LE(1) + 1, &
                                    grid(id)%RE(2) - grid(id)%LE(2) + 1, &
                                    grid(id)%RE(3) - grid(id)%LE(3) + 1))
            totref = totref + PRODUCT(SubGrids(ii)%RE - SubGrids(ii)%LE + 1)
            grid(id)%Dims(:) = grid(id)%RE(:) - grid(id)%LE(:) + 1
            grid(id)%CellSize = 1.d0/REAL(size_basegrids*twotolevarr(level + 1), KIND=8)
            grid(id)%flag = 0
            grid(id)%Data = 0.
            grid(id)%gdown = -1
            grid(id)%mw = 0.
            grid(id)%Id = id
            grid(id)%Level = level + 1
            grid(id)%Up => grid(i)
            grid(i)%gdown(SubGrids(ii)%LE(1):SubGrids(ii)%RE(1), &
                          SubGrids(ii)%LE(2):SubGrids(ii)%RE(2), &
                          SubGrids(ii)%LE(3):SubGrids(ii)%RE(3)) = id
            IF (ii /= SIZE(SubGrids)) grid(id)%Next => grid(id + 1)

         END DO

         ngrids = ngrids + SIZE(SubGrids)

      END DO
      call logger%end_section ! allocating

      tail(level + 1) = tail(level) + ngrids

      NFilledGrids = NFilledGrids + ngrids
      call logger%log('# of new grids', '', '=', [ngrids])

      IF (ngrids == 0) THEN
         lmax = level
         call logger%err('No grids have been found!', 'l_max', '=', [level])
         EXIT
      END IF

      call logger%end_section(print_duration=.true.) ! grids

      call logger%begin_section('flagging')

      ThisCellSize = 1./(size_basegrids*twotolevarr(level + 1))

      call logger%log('particle loop')
      ploop: DO i = 1, ngas
         ! skip particles in the ISM or the particles flagged at previous level
         IF (skipISM .and. rhogas(i) > sf_overdensity_threshold .or. massgas(i) == -1.) CYCLE

         ! IF h becomes is smaller than a cell, r will become very large which does not really make sense anymore
         h = MAX(hsml(i)*size_basegrids*twotolevarr(level + 1), minhsml)

         Curr => grid(1)
         get_grid: DO
            C = INT(pos_gas(:, i)*size_basegrids*twotolevarr(Curr%level)) - Curr%LE + 2
            IF (Curr%level == level + 1) EXIT get_grid
            Id = Curr%gdown(C(1), C(2), C(3))
            IF (Id /= -1) THEN
               Curr => grid(Id)
            ELSE
               EXIT get_grid
            END IF
         END DO get_grid

         IF (Curr%Level == level + 1) THEN
            Curr%flag(C(1), C(2), C(3)) = Curr%flag(C(1), C(2), C(3)) + 1
         END IF

         pC(:) = INT(pos_gas(:, i)*size_basegrids*twotolevarr(level + 1)) + 1
         WHERE (pC > size_basegrids*twotolevarr(level + 1)) pC = size_basegrids*twotolevarr(level + 1)

         h_inv = 1./h
         !..get w_factor
         w_factor = 8.*inv_pi*h_inv*h_inv*h_inv

         !..get minimum value of the expected sumw
         IF (file_fmt == 'gadgetfmt') THEN
            min_sumw = 2.546479089470
         ELSE IF (file_fmt == 'eaglefmt' .or. file_fmt == 'sherwoodfmt') THEN
            min_sumw = w_factor
         ENDIF

         !..share particle between cells
         !CMin(:) = INT(pos_gas(:, i)*size_basegrids*twotolevarr(level + 1) - h) + 1
         CMin(:) = pC(:) - INT(h)
         WHERE (CMin < 1) CMin = 1
         !CMax(:) = CMin(:) + INT(h) + 1
         CMax(:) = pC(:) + INT(h)
         WHERE (CMax > size_basegrids*twotolevarr(level + 1)) CMax = size_basegrids*twotolevarr(level + 1)

         !..check wsmlMAP boundaries, currently used for debugging purposes, ro be eventually removed
         !IF (ANY(CMax(:) - CMin(:) > INT(2*maxhsml))) STOP "wsmlMAP boundary problem!"

         !..compute sumweight
         sumw = 0.
         sumw_thislev = 0.
         DO z = CMin(3), CMax(3)
            DO y = CMin(2), CMax(2)
               check: DO x = CMin(1), CMax(1)

                  !..find grid
                  checkpos(:) = ([x, y, z] - .5)*ThisCellSize  ! convert to box units

                  Curr => grid(1)
                  get_grid2: DO

                     C = INT(checkpos*size_basegrids*twotolevarr(Curr%level)) - Curr%LE + 2
                     IF (Curr%level == level + 1) EXIT get_grid2
                     Id = Curr%gdown(C(1), C(2), C(3))
                     IF (Id /= -1) THEN
                        Curr => grid(Id)
                     ELSE
!                       CYCLE check ! this position is on upper level
                        EXIT get_grid2 ! even IF checkpos is on upper level we need to calc sumw anyway
                     END IF

                  END DO get_grid2

                  !..calc distance from "cell center" (i.e., checkpos)
                  dist2 = SUM((pC(:) - [x, y, z])**2)
                  xx = x - CMin(1) + 1; yy = y - CMin(2) + 1; zz = z - CMin(3) + 1
                  IF (dist2 <= h*h) THEN
                     r = sqrt(dist2)*h_inv
                     IF (r <= .5) THEN
                        IF (file_fmt == 'gadgetfmt') THEN
                           wsmlMap(xx, yy, zz) = 2.546479089470 + 15.278874536822*(r - 1.0)*r*r
                        ELSE IF (file_fmt == 'eaglefmt' .or. file_fmt == 'sherwoodfmt') THEN
                           wsmlMap(xx, yy, zz) = w_factor*(1 - 6*r**2 + 6*r*r*r)
                        ELSE
                           call logger%err('Unknown kernel format!', 'format', '=', [file_fmt])
                        END IF
                     ELSEIF (r <= 1.) THEN
                        IF (file_fmt == 'gadgetfmt') THEN
                           wsmlMap(xx, yy, zz) = 5.092958178941*(1.0 - r)*(1.0 - r)*(1.0 - r)
                        ELSE IF (file_fmt == 'eaglefmt' .or. file_fmt == 'sherwoodfmt') THEN
                           wsmlMap(xx, yy, zz) = w_factor*(2*(1 - r)**3)
                        ELSE
                           call logger%err('Unknown kernel format!', 'format', '=', [file_fmt])
                        END IF
                     ELSE
                        call logger%err('We should not be here!')
                        wsmlMap(xx, yy, zz) = 0.
                     END IF
                     sumw = sumw + wsmlMap(xx, yy, zz)

                     IF (Curr%Level == level + 1) THEN
                        sumw_thislev = sumw_thislev + wsmlMap(xx, yy, zz)
                     ELSE
                        wsmlMap(xx, yy, zz) = 0.  ! this position will be skipped later on
                     END IF
                  ELSE
                     wsmlMap(xx, yy, zz) = 0.
                  END IF
               END DO check
            END DO
         END DO

         !..assign particle
         IF (sumw_thislev == 0.) THEN
            !..this particle does not map into this level and it will be removed for speeding up next level searches
            massgas(i) = -1.
            CYCLE ploop
         END IF

         IF (sumw < min_sumw) THEN
            !call logger%err('sumw less than the expected min_sumw!', ' sumw ', '=', [sumw])
            print *, sumw, sumw_thislev, min_sumw, h, pC, CMin, CMax, pos_gas(:,i), hsml(i)
            STOP
         ENDIF
         norm_massgas = massgas(i)/sumw

         DO z = CMin(3), CMax(3)
            DO y = CMin(2), CMax(2)
               assign: DO x = CMin(1), CMax(1)

                  xx = x - CMin(1) + 1; yy = y - CMin(2) + 1; zz = z - CMin(3) + 1

                  IF (wsmlMap(xx, yy, zz) == 0.) CYCLE assign

                  !..find grid
                  checkpos(:) = ([x, y, z] - .5)*ThisCellSize

                  Curr => grid(1)
                  get_grid3: DO

                     C = INT(checkpos*size_basegrids*twotolevarr(Curr%level)) - Curr%LE + 2
                     IF (Curr%level == level + 1) EXIT get_grid3
                     Id = Curr%gdown(C(1), C(2), C(3))
                     IF (Id /= -1) THEN
                        Curr => grid(Id)
                     ELSE
                        CYCLE assign ! particle is on upper level
                     END IF

                  END DO get_grid3

                  IF (enforced_temperature /= -1.) THEN
                     Curr%Data(5, C(1), C(2), C(3)) = enforced_temperature
                  ELSE
                     Curr%Data(5, C(1), C(2), C(3)) = Curr%Data(5, C(1), C(2), C(3)) + wsmlMap(xx, yy, zz)*norm_massgas*ugas(i)
                  ENDIF
                  Curr%Data(1, C(1), C(2), C(3)) = Curr%Data(1, C(1), C(2), C(3)) + wsmlMap(xx, yy, zz)*norm_massgas
                  Curr%Data(11, C(1), C(2), C(3)) = Curr%Data(11, C(1), C(2), C(3)) + wsmlMap(xx, yy, zz)*norm_massgas
    Curr%Data(2, C(1), C(2), C(3)) = Curr%Data(2, C(1), C(2), C(3)) + wsmlMap(xx, yy, zz)*norm_massgas*vel_gas(1, i)     !x-velocity
    Curr%Data(3, C(1), C(2), C(3)) = Curr%Data(3, C(1), C(2), C(3)) + wsmlMap(xx, yy, zz)*norm_massgas*vel_gas(2, i)     !y-velocity
    Curr%Data(4, C(1), C(2), C(3)) = Curr%Data(4, C(1), C(2), C(3)) + wsmlMap(xx, yy, zz)*norm_massgas*vel_gas(3, i)     !z-velocity
               END DO assign
            END DO
         END DO
      END DO ploop

      call logger%end_section(print_duration=.true.) ! flagging

      call logger%begin_section('stars')
      !..add star particles to refine criteria
      IF (nstars > 0) THEN

         DO i = 1, nstars

            IF (SimTime - tform(i) > maxstage) CYCLE

            !..simple tree search
            Curr => Grid(1)
            ii = 1
            findcell: DO
               C = INT(pos_stars(:, i)*size_basegrids*twotolevarr(Curr%level)) - Curr%LE + 2
               ii = Curr%gdown(C(1), C(2), C(3))
               IF (ii > 0) THEN
                  Curr => grid(ii)
               ELSE
                  EXIT findcell
               END IF
            END DO findcell

            !..update flag
            IF (stmref) THEN ! max refinment, every cell with a star is refined to the max level
               Curr%flag(C(1), C(2), C(3)) = npref + 1
            ELSE
               Curr%flag(C(1), C(2), C(3)) = Curr%flag(C(1), C(2), C(3)) + 1
            END IF

         END DO
      END IF
      call logger%end_section ! stars

      call logger%begin_section('normalizing')
      call logger%log('temperature and velocities by the density')
      DO ii = head(level + 1), tail(level + 1)
         !..normalizing the temperature and the velocites by the density (necessary due to the smoothing kernel)
         IF (enforced_temperature == -1.) THEN
            grid(ii)%Data(5, :, :, :) = grid(ii)%Data(5, :, :, :)/grid(ii)%Data(1, :, :, :)
         ENDIF
         WHERE (grid(ii)%Data(1, :, :, :) /= 0.)
         grid(ii)%Data(2, :, :, :) = grid(ii)%Data(2, :, :, :)/grid(ii)%Data(1, :, :, :)
         grid(ii)%Data(3, :, :, :) = grid(ii)%Data(3, :, :, :)/grid(ii)%Data(1, :, :, :)
         grid(ii)%Data(4, :, :, :) = grid(ii)%Data(4, :, :, :)/grid(ii)%Data(1, :, :, :)
         END WHERE

         grid(ii)%Data(1, :, :, :) = grid(ii)%Data(1, :, :, :)*(twotolevarr(grid(ii)%Level))**3

         IF (ANY(grid(ii)%Data(1, :, :, :) == 0.)) THEN

            Parent => grid(ii)%Up

            !..for cells without particles, get info from parent grid
            DO z = 1, SIZE(grid(ii)%flag, DIM=3)
               DO y = 1, SIZE(grid(ii)%flag, DIM=2)
                  DO x = 1, SIZE(grid(ii)%flag, DIM=1)

                     IF (grid(ii)%Data(1, x, y, z) > 0.) CYCLE

                     px = (grid(ii)%LE(1) + x)/2 - Parent%LE(1) + 1
                     py = (grid(ii)%LE(2) + y)/2 - Parent%LE(2) + 1
                     pz = (grid(ii)%LE(3) + z)/2 - Parent%LE(3) + 1

                     grid(ii)%Data(:, x, y, z) = Parent%Data(:, px, py, pz)

                  END DO
               END DO
            END DO

         END IF

         WHERE (grid(ii)%flag > npref)
         grid(ii)%flag = 1
         ELSEWHERE
         grid(ii)%flag = 0
         END WHERE

      END DO
      call logger%end_section ! normalizing

      call logger%log('flagged cells', '', '=', [totref])
      call logger%log('# of grids (patches)', '', '=', [ngrids])
      call logger%log('balance (average size)', '', '=', [real(totref)/ngrids])
      call logger%log('overal efficiency', '', '=', [totflag/real(totref)])

      call logger%end_section ! level_number_str
   END DO

   call logger%end_section ! level
   call logger%end_section ! AMR

   IF (nstars > 0 .and. .not. calc_emissivity) THEN
      CALL StarGrid
   END IF

   call logger%begin_section('chombo')

   !..init. HDF5
   CALL H5open_f(err)

   !..create Chombo file
   CALL H5Fcreate_f(chombofile, H5F_ACC_TRUNC_F, file_id, err)

   !..write Chombo Headear
   CALL WriteChomboHeader

   !..write data level by level
   IF (file_fmt == "gizmofmt") THEN
	grid(1)%Data(1, :, :, :) = grid(1)%Data(1, :, :, :)*ave_grid_mass
        grid(1)%Data(11, :, :, :) = grid(1)%Data(11, :, :, :)*ave_grid_mass
   END IF
   DO level = lmin, lmax

      chombo_lev = level - lmin
      CALL WriteChombo(chombo_lev)

   END DO

   CALL H5Pclose_f(plist_id, err)
   CALL H5Tclose_f(comp_id, err)

   call h5gopen_f(file_id, "/level_0", group_id, hdferr)

   call h5tget_size_f(H5T_NATIVE_INTEGER, element_size, hdferr)
   tot_size = 6*element_size
   call h5tcreate_f(H5T_COMPOUND_F, tot_size, comp_type, hdferr)

   offset_pd = 0
   call h5tinsert_f(comp_type, "lo_i", offset_pd, H5T_NATIVE_INTEGER, hdferr)
   offset_pd = offset_pd + element_size
   call h5tinsert_f(comp_type, "lo_j", offset_pd, H5T_NATIVE_INTEGER, hdferr)
   offset_pd = offset_pd + element_size
   call h5tinsert_f(comp_type, "lo_k", offset_pd, H5T_NATIVE_INTEGER, hdferr)
   offset_pd = offset_pd + element_size
   call h5tinsert_f(comp_type, "hi_i", offset_pd, H5T_NATIVE_INTEGER, hdferr)
   offset_pd = offset_pd + element_size
   call h5tinsert_f(comp_type, "hi_j", offset_pd, H5T_NATIVE_INTEGER, hdferr)
   offset_pd = offset_pd + element_size
   call h5tinsert_f(comp_type, "hi_k", offset_pd, H5T_NATIVE_INTEGER, hdferr)
   offset_pd = offset_pd + element_size

   prob_domain_values = 0
   prob_domain_values(4:6) = prob_domain - 1

   call h5screate_f(H5S_SCALAR_F, space_id, hdferr)
   call h5acreate_f(group_id, "prob_domain", comp_type, space_id, attr_id, hdferr)
   call h5awrite_f(attr_id, comp_type, prob_domain_values, dims_pd, hdferr)
   call h5aclose_f(attr_id, hdferr)

   call h5tclose_f(comp_type, hdferr)
   call h5sclose_f(space_id, hdferr)
   call h5gclose_f(group_id, hdferr)
   CALL H5Fclose_f(file_id, err)
   CALL H5close_f(err)

   call logger%end_section(print_duration=.true.) ! chombo

   call logger%begin_section('final_check')

   total_mass = 0d0
   total_mass_gas = 0d0
   total_volume = 0d0
   number_of_cells = 0
   number_of_leaf_cells = 0
   do ii = 1, tail(lmax)
      number_of_cells = number_of_cells + product(grid(ii)%dims)

      do k = 1, grid(ii)%dims(3)
      do j = 1, grid(ii)%dims(2)
      do i = 1, grid(ii)%dims(1)
         IF (grid(ii)%gdown(i, j, k) /= -1) cycle

         number_of_leaf_cells = number_of_leaf_cells + 1

         IF (ieee_is_nan(grid(ii)%data(1, i, j, k))) THEN
            call logger%warn('NaN density detected in grid', ii, 'cell', [i, j, k])
         ELSE
            total_mass = total_mass + grid(ii)%data(1, i, j, k) !*grid(ii)%cellsize**3
            total_mass_gas = total_mass_gas + grid(ii)%data(11, i, j, k)
            total_volume = total_volume + grid(ii)%cellsize**3
         END IF
      END do
      END do
      END do
   END do
   total_mass = total_mass
   total_mass_gas = total_mass_gas
   call logger%log('M_total Neutral Hydrogen', '[Msol]', '=', [total_mass_gas])
   call logger%log('# of leaves', number_of_leaf_cells, 'out of', [number_of_cells])

   call logger%log('M_total', '[Msol]', '=', [total_mass])
   call logger%log('V_total', '[???]', '=', [total_volume])

   average_density = total_mass/total_volume
   call logger%log('<density>', '[???]', '=', [average_density])
   call logger%end_section ! final_check
CONTAINS

   SUBROUTINE in_strip(inband, i, j, k, size_basegrids, delta, cut_le, cut_re, boxsize)
      IMPLICIT NONE
      LOGICAL, intent(inout) :: inband
      INTEGER, intent(in) :: i, j, k, size_basegrids, delta
      INTEGER :: l
      REAL(kind=8), intent(in) :: cut_re(3), cut_le(3)
      REAL(kind=8), intent(in) :: boxsize
      INTEGER :: pos(3)

      pos(1) = i
      pos(2) = j
      pos(3) = k

      inband = .false.
      do l = 1, 3
         IF (cut_le(l) == 0) THEN
            IF (pos(l) >= size_basegrids - 2*delta) THEN
               inband = .true.
            ENDIF
         ELSE IF (cut_re(l) == boxsize) THEN
            IF (pos(l) <= 2*delta) THEN
               inband = .true.
            END IF
         ELSE
            IF (pos(l) <= delta .or. pos(l) >= size_basegrids - delta) THEN
               inband = .true.
            ENDIF
         ENDIF
      ENDDO

   END SUBROUTINE in_strip

   SUBROUTINE read_param

      IMPLICIT NONE
      INTEGER :: n, i, iargc
      CHARACTER(len=200)   :: opt
      CHARACTER(len=500) :: arg

      call logger%begin_section('param')

      !..set default values
      lmin = 0
      lmax = -1   ! it will be set by refinement criteria
      size_basegrids = -1 ! it will be set to Ngas^(1/3)
      box_le = 0.d0
      box_re = 1.d0
      cut_le = -1.e9  ! original particle pos units
      cut_re = 1.e9
      new_origin = 0.0 ! New (0,0,0) point of the box. We subtract this from the particle coordinates. Same units as cut_le (i.e. h*cMpc).
      ncmin = 1
      minhsml = 1e-9 ! Minimum smoothing length [cells]; in the current version, this is now just to avoid 0 values
      npmin = -1
      npref = 8
      units = " "
      skipISM = .false.
      omega_l = 1.0 - omega_m
      tipsyparam = "??"
      maxstage = -1.00 ! the maximum stellar age is set to -1 by default to bypass the 'stellar sections' of the code
      h0 = -1
      omega_b = -1
      starfile = "??"  ! output file with star catalogue
      stmref = .false. ! IF .true., activate refinement for each cell that contains at least 1 star
      sb99 = "??"      ! starburst99 table file with number of ioniz photons in func of metal and age
      file_fmt = "??"
      enforced_temperature = -1.
      sf_overdensity_threshold = -1e0
      calc_emissivity = .false. ! whether to calculate the emissivity and emissivity weighted velocities in each cell
      conv_star_gas = .false.
      recentre_box = .false. ! whether to re-centre the coordinates of the gas particles in simulation box on the given coordinates
      photoHeatingFloor = 0. ! which temperature to impose as a temperature floor on the ionised gas due to photo-heating (0. means no floor)

      n = iargc()
      CALL getarg(0, opt)
      IF (n < 4) THEN
         print *, 'usage: ', TRIM(opt), ' -inp snap_dir/fname -out chombo_file'
         print *, 'optional parameters: '
         print *, '  [-units "mil"] use this to switch to millenium runs units'
         print *, '  [-base_grid] base grid size [default=Ngas^(1/3)] '
         print *, '  [-lmax] max_level [default set by refinement]'
         print *, '  [-npref] minimum number of particles (gas + stars) for refinement [default=8]'
         print *, '  [-eff] Clustering_efficiency [default=0.75]'
         print *, '  [-cut_le] left edge of the cut box in orig part pos  [default=-inf]'
         print *, '  [-cut_re] right edge of the cut box in orig part pos [default=+inf]'
         print *, '  [-new_origin] New (0,0,0) point of the box. We subtract this from the particle coordinates. Same units as cut_le(i.e. h*cMpc).'
         print *, '  [-ncmin] min grid size (units of coarser grid, one level up) [default=1]'
         print *, '  [-minhsml] min smoothing lenght (radius) in cell units [default=2]'
         print *, '  [-skipISM] skip gas particles into the ISM [default=.false.]'
         print *, '  [-sf_overdensity_threshold] Star formation overdensity threshold (make sure -skipISM is set to .true.)'
         print *, '  [-tpar] tipsy parameter file, MANDATORY for tipsy input'
         print *, '  [-h0] h0, check this value for tipsy input! [default=0.73]'
         print *, '  [-ob] omega_b, check this value for tipsy input! [default=0.046]'
         print *, '  [-msa] max age in Myr to consider stellar part [default=use all]'
         print *, '  [-starfile] <fname>, creates a file with source positions and lum'
         print *, '  [-stmref] IF .true. refine every cell that contains a star'
         print *, '  [-sb99] sb99 table file with number of ioniz photons [mandatory IF -starfile is used] '
         print *, '  [-file_fmt] format of your file (eg. sherwoodfmt, eaglefmt, arepofmt, gizmo ...)'
         print *, '  [-enf_temp] A given temperature to be enforced on all mesh cells regardless of their original temperature'
         print *, '  [-calc_emissivity] Whether to calculate the emissivity and emissivtiy weighted velocites in each cell, default: .false.'
         print *, '  [-conv_s2g] Wether to convert star particles into gas particles. Only in Sherwoid. [dafault=False]'
         print *, '  [-recentre_box] Whether to re-centre the coordinates of the gas particles in simulation box on the given coordinates [default=False]'
         print *, '  [-photoHeatingFloor]  Which temperature to impose as a temperature floor on the ionised gas due to photo-heating (0. means no floor)'
         STOP
      END IF

      ceff = 0.75

      call logger%begin_section('user_defined')
      DO i = 1, n, 2
         CALL getarg(i, opt)
         IF (i == n) THEN
            call logger%err('Please pass necessary arguments!')
            STOP
         END IF
         CALL getarg(i + 1, arg)
         call logger%log(opt, arg)
         SELECT CASE (TRIM(opt))
         CASE ('-inp')
            inpfile = TRIM(arg)
         CASE ('-out')
            chombofile = TRIM(arg)
         CASE ('-lmax')
            READ (arg, *) lmax
         CASE ('-units')
            READ (arg, *) units
         CASE ('-om')
            READ (arg, *) omega_m
         CASE ('-ob')
            READ (arg, *) omega_b
         CASE ('-eff')
            READ (arg, *) ceff
         CASE ('-cut_le')
            READ (arg, *) cut_le
         CASE ('-cut_re')
            READ (arg, *) cut_re
         CASE ('-new_origin')
            READ (arg, *) new_origin
         CASE ('-ncmin')
            READ (arg, *) ncmin
         CASE ('-npmin')
            READ (arg, *) npmin
         CASE ('-minhsml')
            READ (arg, *) minhsml
         CASE ('-npref')
            READ (arg, *) npref
         CASE ('-base_grid')
            READ (arg, *) size_basegrids
         CASE ('-skipISM')
            READ (arg, *) skipISM
         CASE ('-tpar')
            READ (arg, *) tipsyparam
         CASE ('-h0')
            READ (arg, *) h0
         CASE ('-msa')
            READ (arg, *) maxstage
         CASE ('-starfile')
            READ (arg, *) starfile
         CASE ('-stmref')
            READ (arg, *) stmref
         CASE ('-sb99')
            READ (arg, *) sb99
         CASE ('-file_fmt')
            READ (arg, *) file_fmt
         CASE ('-enf_temp')
            READ (arg, *) enforced_temperature
         CASE ('-sf_overdensity_threshold')
            READ (arg, *) sf_overdensity_threshold
         CASE ('-calc_emissivity')
            READ (arg, *) calc_emissivity
         CASE ('-conv_s2g')
            READ (arg, *) conv_star_gas
         CASE ('-recentre_box')
            READ (arg, *) recentre_box
         CASE ('-photoHeatingFloor')
            READ (arg, *) photoHeatingFloor
         CASE default
            print *, 'option ', TRIM(arg), 'unknown!'
            STOP
         END SELECT
      END DO
      call logger%end_section ! user_defined

      IF (ANY(box_le < 0.) .or. ANY(box_re > 1.) .or. ANY(box_le >= box_re) &
          .or. (box_re(1) - box_le(1) /= box_re(2) - box_le(2)) &
          .or. (box_re(1) - box_le(1) /= box_re(3) - box_le(3)) &
          .or. (box_re(2) - box_le(2) /= box_re(3) - box_le(3))) then
         call logger%err('Invalid cut values!')
         STOP
      end if

      IF (TRIM(starfile) /= "??") THEN
         IF (TRIM(sb99) == "??") then
            call logger%err('sb99 file not specified')
            STOP
         end if
      END IF

      call logger%log('', 'lmin', '=', [lmin])
      call logger%log('', 'lmax', '=', [lmax])
      call logger%log('', 'size_basegrids', '=', [size_basegrids])
      call logger%log('', 'box_le', '=', [box_le])
      call logger%log('', 'box_re', '=', [box_re])
      call logger%log('', 'cut_le', '=', [cut_le])
      call logger%log('', 'cut_re', '=', [cut_re])
      call logger%log('', 'new_origin', '=', [new_origin])
      call logger%log('', 'ncmin', '=', [ncmin])
      call logger%log('', 'npmin', '=', [npmin])
      call logger%log('', 'minhsml', '=', [minhsml])
      call logger%log('', 'npref', '=', [npref])
      call logger%log('', 'units', '=', [units])
      call logger%log('', 'skipISM', '=', [skipISM])
      call logger%log('', 'omega_l', '=', [omega_l])
      call logger%log('', 'tipsyparam', '=', [tipsyparam])
      call logger%log('', 'maxstage', '=', [maxstage])
      call logger%log('', 'h0', '=', [h0])
      call logger%log('', 'omega_b', '=', [omega_b])
      call logger%log('', 'starfile', '=', [starfile])
      call logger%log('', 'stmref', '=', [stmref])
      call logger%log('', 'sb99', '=', [sb99])
      call logger%log('', 'file_fmt', '=', [file_fmt])
      call logger%log('', 'enforced_temperature', '=', [enforced_temperature])
      call logger%log('', 'sf_overdensity_threshold', '=', [sf_overdensity_threshold])
      call logger%log('', 'calc_emissivity', '=', [calc_emissivity])
      call logger%log('', 'recentre_box', '=', [recentre_box])
      call logger%log('', 'photoHeatingFloor', '=', [photoHeatingFloor])
      call logger%end_section ! param
    END SUBROUTINE read_param

   SUBROUTINE ReadGadget(repository)

      IMPLICIT NONE
      CHARACTER(len=250), INTENT(IN) :: repository
      CHARACTER(len=250) :: fname
      INTEGER :: ipos, i, fpos, lev, flag_cooling, nsnaps, GADGET_FORMAT, ierr
      REAL(kind=8) :: box100_dp, omega0_dp, xlambda0_dp, h100_dp
      CHARACTER(len=3) :: nchar, sn
      LOGICAL :: ok, file_is_directory
      REAL(kind=4), ALLOCATABLE :: tArray(:, :)
      REAL(kind=8), PARAMETER :: HFrac = .76d0, UnitLength_in_cm = 3.085678d21, &
                                 UnitMass_in_g = 1.989d43, UnitVelocity_in_cm_per_s = 1d5, &
                                 UnitTime_in_s = UnitLength_in_cm/UnitVelocity_in_cm_per_s, &
                                 UnitDensity_in_cgs = UnitMass_in_g/UnitLength_in_cm**3, &
                                 UnitPressure_in_cgs = UnitMass_in_g/UnitLength_in_cm/UnitTime_in_s**2, &
                                 UnitEnergy_in_cgs = UnitMass_in_g*UnitLength_in_cm**2/UnitTime_in_s**2, &
                                 GRAVITY = 6.672d-8, kB = 1.3806d-16, mproton = 1.6726d-24, &
                                 G = GRAVITY/UnitLength_in_cm**3*UnitMass_in_g*UnitTime_in_s**2, &
                                 gamma = 5.d0/3.d0, hubble = 0.73d0, OmegaBaryonNow = 0.041d0

      call logger%begin_section('gadget')

      MeanBarDen = 27.7*OmegaBaryonNow  ! Gadget Units

      !..get initial info

      !..inquire file
      ipos = INDEX(repository, 'snapdir_')
      nchar = repository(ipos + 8:ipos + 10)
      fname = TRIM(repository)//'/snap_'//TRIM(nchar)//'.0'
      inquire (file=fname, exist=ok) ! verIFy input file
      file_is_directory = ok
      IF (.not. ok) THEN
         !..maybe is not a directory but the name file (for single files), let's try
         fname = TRIM(repository)
         inquire (file=fname, exist=ok) ! verIFy input file
         IF (.not. ok) THEN
            print *, 'Input file/directory not found!'
            stop
         ELSE
            file_is_directory = .false.
         END IF
      END IF

      !..read and store general info
      OPEN (1, file=fname, action='read', form='unformatted')
      !..check format
      READ (1, iostat=ierr) npart
      IF (ierr /= 0) THEN
         GADGET_FORMAT = 2
      ELSE
         GADGET_FORMAT = 1
      END IF
      REWIND (1)
      print *, "Input files are in GADGET_FORMAT=", GADGET_FORMAT
      IF (GADGET_FORMAT == 2) READ (1)
      READ (1) npart, massarr, gadget_time, gadget_redshIFt, flag_sfr, &
         flag_feedback, npartall, flag_cooling, nsnaps, box100_dp, omega0_dp, &
         xlambda0_dp, h100_dp
      CLOSE (1)

      !..ALLOCATE DATA ARRAYS
      ALLOCATE (pos_gas(3, npartall(0)), ugas(npartall(0)), rhogas(npartall(0)), &
                nelecgas(npartall(0)), vel_gas(3, npartall(0)), &
                massgas(npartall(0)), hsml(npartall(0)))
      rhogas = 0.
      ugas = 0.
      ngas = npartall(0)

      fpos = 0
      !..fill arrays
      DO i = 0, nsnaps - 1

         IF (file_is_directory) THEN

            IF (i < 10) THEN
               WRITE (sn, '(i1)') i
            ELSEIF (i < 100) THEN
               WRITE (sn, '(i2)') i
            ELSEIF (i < 1000) THEN
               WRITE (sn, '(i3)') i
            ELSE
               STOP 'are you crazy? too many snapshots!'
            END IF

            fname = TRIM(repository)//'/snap_'//TRIM(nchar)//'.'//TRIM(sn)

         ELSE
            fname = TRIM(repository)

         END IF

         OPEN (1, file=fname, action='read', form='unformatted')
         IF (GADGET_FORMAT == 2) READ (1)
         READ (1) npart, massarr, gadget_time, gadget_redshIFt, flag_sfr, &
            flag_feedback, npartall

         print *, 'reading ', TRIM(fname), " npart_gas=", npart(0), "over ", npartall(0)

         ALLOCATE (tArray(3, SUM(npart)))

         IF (GADGET_FORMAT == 2) READ (1)
         READ (1) tArray
         pos_gas(:, fpos + 1:fpos + npart(0)) = tArray(:, 1:npart(0))
         print *, "pos_gas read"
         IF (GADGET_FORMAT == 2) READ (1)
         READ (1) tArray
         vel_gas(:, fpos + 1:fpos + npart(0)) = tArray(:, 1:npart(0))
         print *, "vel_gas read"

         DEALLOCATE (tArray)

         IF (GADGET_FORMAT == 2) READ (1)
         READ (1) !skip particle id
         print *, "id skipped"

         IF (ANY(npart > 0 .and. massarr == 0)) THEN
            IF (GADGET_FORMAT == 2) READ (1)
            READ (1) massgas(fpos + 1:fpos + npart(0))
            print *, "massgas read"
         END IF

         IF (npart(0) > 0) THEN

            IF (GADGET_FORMAT == 2) READ (1)
            READ (1) ugas(fpos + 1:fpos + npart(0))
            print *, "ugas read"

            IF (GADGET_FORMAT == 2) READ (1)
            READ (1) rhogas(fpos + 1:fpos + npart(0))
            print *, "rhogas read"

            IF (flag_sfr /= 0) THEN

               IF (GADGET_FORMAT == 2) READ (1)
               READ (1) nelecgas(fpos + 1:fpos + npart(0))
               print *, "nelecgas read"

            ELSE

               nelecgas = 1.157
               print *, "nelecgas fixed to", nelecgas

            END IF

            IF (GADGET_FORMAT == 2) READ (1)
            READ (1) !skip NH0
            print *, "NH0 skipped"

            IF (GADGET_FORMAT == 2) READ (1)
            READ (1) hsml(fpos + 1:fpos + npart(0))
            print *, "hsml read"

         ENDIF

         fpos = fpos + npart(0)

         CLOSE (1)

      END DO

      IF (size_basegrids == -1) THEN
         size_basegrids = INT(npartall(0)**(1./3.))
         DO lev = 2, 12
            IF (size_basegrids < 2**lev) EXIT
         END DO
         size_basegrids = 2**(lev - 1)
         print *, "base grid size has been changed to ", size_basegrids
      END IF

      IF (2*maxhsml > size_basegrids) THEN
         print *, "WARNING: maxhsml larger than box size for base level!"
      END IF

      !..convert ugas to temperature
      ugas = ugas*4.0/(3*HFrac + 1.+4*HFrac*nelecgas)*mproton/kB*(gamma - 1)*UnitEnergy_in_cgs/UnitMass_in_g ! Temperature in K
      sf_overdensity_threshold = 0.854872e6/(1.+gadget_redshIFt)**3    ! gadget code units (physical) / (1+z)**3
      IF (TRIM(units) == "mil") THEN
         pos_gas = pos_gas/hubble
         rhogas = rhogas/hubble
         hsml = hsml*.73/hubble
      ELSE
         pos_gas = pos_gas*1.d-3/hubble
         rhogas = rhogas*1.d9/hubble
         hsml = hsml*1.d-3/hubble
      END IF
      ComovingBoxSize = MAXVAL(pos_gas) - MINVAL(pos_gas)
      BoxOrig(1:3) = MINVAL(pos_gas)
      pos_gas = (pos_gas - BoxOrig(1))/ComovingBoxSize
      hsml = hsml/ComovingBoxSize
      massgas = massgas*(size_basegrids/ComovingBoxSize)**3/MeanBarDen   ! in order to have densities in units of density contrast

      WHERE (pos_gas == 0.) pos_gas = NEAREST(pos_gas, 1.d0)
      WHERE (pos_gas == 1.) pos_gas = NEAREST(pos_gas, -1.d0)

      print *, "redshIFt=", gadget_redshIFt
      print *, "Comoving BoxSize=", ComovingBoxSize, "Mpc"
      print *, "Chombo Box Origin=", BoxOrig
      GadgetBoxCenter = BoxOrig/ComovingBoxSize
      print *, "previous center of coords is now at position:", BoxOrig/ComovingBoxSize, "(box units)"
      print *, "npart^(1/3)=", npartall(0)**(1./3.)
      print *, "Min Temp=", MINVAL(ugas(:)), "Max Temp=", MAXVAL(ugas(:))
      print *, "Min rhogas=", MINVAL(rhogas(:)), "Max rhogas=", MAXVAL(rhogas(:)), "Mean=", SUM(rhogas)/SIZE(rhogas)
      print *, "Mean(rhogas<1.e3)=", SUM(rhogas, MASK=rhogas < 1.e3)/COUNT(rhogas < 1.e3)
      print *, "Min massgas=", MINVAL(massgas(:)), "Max massgas=", MAXVAL(massgas(:)), " in code units"

      call logger%end_section ! gadget

   END SUBROUTINE ReadGadget

   SUBROUTINE ReadTipsy(inpfile, tipsyparam)

      IMPLICIT NONE
      CHARACTER(len=*), INTENT(IN) :: inpfile, tipsyparam
      CHARACTER(len=250)  :: dummy_string
      CHARACTER(len=10)   :: st2
      INTEGER(kind=4) :: nprint, ndark, i, lev
      REAL(kind=4) :: time_sp, dum, H0code, H0phys, z, massgas_, pos_gas_(3), pos_stars_(3), stmass_, tform_, &
                      vel_gas_(3), hsml_, ugas_, rhogas_, metal_
      REAL(kind=8) :: time_db, thishmin
      REAL(kind=4), PARAMETER:: MpcToCm = 3.08588e24, proton_mass = 1.672622e-24, MyrToSec = 3.1556926e13, &
                                T_Hubble = 0.97777506d4          ! Hubble time in Myr/h

      call logger%begin_section('tipsy')

      !..read parameter file to get units
      dMsolUnit = 0; dKpcUnit = 0.; omega_m = 0.;
      OPEN (1, file=tipsyparam, action="read", form='formatted')
      ierr = 0
      DO
         READ (1, '(a)', iostat=ierr) dummy_string
         st2 = dummy_string
         !print *, TRIM(st2)
         IF (ierr /= 0) EXIT
         BACKSPACE (1)
         SELECT CASE (TRIM(st2))
         CASE ('dMsolUnit')
            READ (1, *) dummy_string, dummy_string, dMsolUnit
         CASE ('dKpcUnit')
            READ (1, *) dummy_string, dummy_string, dKpcUnit
         CASE ('dOmega0')
            READ (1, *) dummy_string, dummy_string, omega_m
         CASE DEFAULT
            READ (1, *)
         END SELECT
      END DO
      IF (dMsolUnit == 0.) STOP "dMsolUnit not found in parameter file!"
      IF (dKpcUnit == 0.) STOP "dKpcUnit not found in parameter file!"
      IF (omega_m == 0.) STOP "domega_m not found in parameter file!"
      print *, "found: dMsolUnit=", dMsolUnit, " dKpcUnit=", dKpcUnit, " dOmega0=", omega_m
      omega_l = 1.0 - omega_m
      CLOSE (1)

      !..derive tcode_to_Myr
      H0code = 2.889405
      H0phys = h0*100.*1.e5*(MyrToSec/MpcToCm)
      tcode_to_Myr = H0code/H0phys
      print *, "tcode_to_Myr=", tcode_to_Myr

      OPEN (1, file=inpfile, action="read", form="unformatted", access="stream")

      ierr = 0

      !..read header
      READ (1, iostat=ierr) time_db, nprint, ndim, ngas, ndark, nstars
      IF (ierr /= 0) STOP "problem reading header"
      IF (time_db < 0 .or. nprint < 0 .or. ndim > 3) STOP "wrong ENDianess of tipsy file? Try with: 'export F_UFMTENDIAN=big:1,1' "
      time_sp = REAL(time_db, KIND=4)
      print *, "header=", time_db, nprint, ndim, ngas, ndark, nstars

      z = 1.0/(time_sp) - 1.0
      SimTime = (2./3.)*log(sqrt(omega_l/omega_m)/(1.0 + z)**1.5 + sqrt(1.0 + omega_l/omega_m/(1.0 + z)**3))* &
                (1./sqrt(omega_l))*T_Hubble/h0 ! Myr

      print *, "Sim redshIFt=", z
      print *, "Sim Time=", SimTime, " Myr"

      print *, "found: ngas=", ngas, " ndark=", ndark, " nstars=", nstars

      !..ALLOCATE DATA ARRAYS
      ALLOCATE ( &
         pos_gas(ndim, ngas), &
         ugas(ngas), &
         rhogas(ngas), &
         vel_gas(ndim, ngas), &
         massgas(ngas), hsml(ngas))

      IF (nstars > 0) ALLOCATE ( &
         stmass(nstars), &
         pos_stars(ndim, nstars), &
         tform(nstars), &
         stmetal(nstars))

      rhogas = 0.
      ugas = 0.

      READ (1, iostat=ierr) dum
      IF (ierr /= 0) STOP "problem reading first line after header"
      ii = 0
      DO i = 1, ngas
         READ (1, iostat=ierr) massgas_, pos_gas_(1:ndim), vel_gas_(1:ndim), rhogas_, ugas_, hsml_, dum, dum
         IF (ierr /= 0) STOP "problem reading gas particles"
         IF (ALL(pos_gas_ <= cut_re) .and. ALL(pos_gas_ >= cut_le)) THEN
            ii = ii + 1
            massgas(ii) = massgas_; pos_gas(:, ii) = pos_gas_(:); vel_gas(:, ii) = vel_gas_(:);
            rhogas(ii) = rhogas_; ugas(ii) = ugas_; hsml(ii) = hsml_;
         END IF
      END DO
      IF (ii == 0) STOP " NO PARTICLES FOUND WITHIN CUT REGION! "
      IF (ngas /= ii) THEN
         print *, "NEW ngas within cut region: ngas=", ii
      END IF
      ngas = ii
      !..skip DM
      DO i = 1, ndark
         READ (1) dum, dum, dum, dum, dum, dum, dum, dum, dum
      END DO
      !..read stars IF present
      IF (nstars > 0) THEN
         ii = 0
         DO i = 1, nstars
            READ (1, iostat=ierr) stmass_, pos_stars_(1:ndim), dum, dum, dum, metal_, tform_, dum, dum
            IF (ierr /= 0) STOP "problem reading stellar particles"
            IF (ALL(pos_stars_ <= cut_re) .and. ALL(pos_stars_ >= cut_le)) THEN
               ii = ii + 1
               stmass(ii) = massgas_; pos_stars(:, ii) = pos_stars_(:); tform(ii) = tform_; stmetal(ii) = metal_;
            END IF
         END DO
         IF (nstars /= ii) THEN
            IF (ii /= 0) THEN
               print *, "NEW nstars within cut region: nstars=", ii
            ELSE
               print *, " "
               print *, "###### WARNING: no star are present within cut region!!! ###### "
               print *, " "
            END IF
         END IF
         nstars = ii
         print *, "minmax stellar part=", MINVAL(stmass(1:nstars))*dMsolUnit, MAXVAL(stmass(1:nstars))*dMsolUnit, " Msol"
         tform = tform*tcode_to_Myr
         print *, "minmax stellar tform=", MINVAL(tform(1:nstars)), MAXVAL(tform(1:nstars)), " Myr"
      END IF
      CLOSE (1)

      IF (nstars > 0) THEN
         !..find timestep using stellar form time
         current_time = MAXVAL(tform(1:nstars))
         sim_tstep = current_time - MAXVAL(tform(1:nstars), MASK=tform(1:nstars) /= current_time)
         print *, "sim_tstep=", sim_tstep, " Myr"
         print *, "# stars younger than sim_tstep=", COUNT(tform(1:nstars) >= current_time - sim_tstep)
         print *, "# stars younger than 10Myr=", COUNT(tform(1:nstars) >= current_time - 10.0)
      END IF

      IF (size_basegrids == -1) THEN
         size_basegrids = INT(ngas**(1./3.))
         DO lev = 2, 12
            IF (size_basegrids < 2**lev) EXIT
         END DO
         size_basegrids = 2**(lev - 1)
         print *, "base grid size has been changed to ", size_basegrids
      END IF

      IF (2*maxhsml > size_basegrids) THEN
         print *, "WARNING: maxhsml larger than box size for base level!"
      END IF

      print *, "original gas minpos=", MINVAL(pos_gas(:, 1:ngas), DIM=2)
      print *, "original gas maxpos=", MAXVAL(pos_gas(:, 1:ngas), DIM=2)
      IF (nstars > 0) THEN
         print *, "original star minpos=", MINVAL(pos_stars(:, 1:nstars), DIM=2)
         print *, "original star maxpos=", MAXVAL(pos_stars(:, 1:nstars), DIM=2)
      END IF

      ComovingBoxSize = 0
      DO i = 1, 3
         BoxOrig(i) = MINVAL(pos_gas(i, 1:ngas))
         ComovingBoxSize = MAX(ComovingBoxSize, abs(MAXVAL(pos_gas(i, 1:ngas)) - BoxOrig(i)))
      END DO
      DO i = 1, 3
         pos_gas(i, 1:ngas) = (pos_gas(i, 1:ngas) - BoxOrig(i))/ComovingBoxSize
         IF (nstars > 0) THEN
            pos_stars(i, 1:nstars) = (pos_stars(i, 1:nstars) - BoxOrig(i))/ComovingBoxSize
         END IF
      END DO
      hsml = hsml/ComovingBoxSize

      sf_overdensity_threshold = 1.e35  ! large dummy value

      IF (ANY(pos_gas(:, 1:ngas) < 0) .OR. ANY(pos_gas(:, 1:ngas) > 1)) THEN
         print *, MAXVAL(pos_gas(:, 1:ngas)), MINVAL(pos_gas(:, 1:ngas))
         STOP "some pos<0 or >1!!"
      END IF

      WHERE (pos_gas == 0.) pos_gas = NEAREST(pos_gas, 1.d0)
      WHERE (pos_gas == 1.) pos_gas = NEAREST(pos_gas, -1.d0)

      gadget_redshIFt = 1.0/time_sp - 1
      gadget_time = time_sp

      print *, "redshIFt=", gadget_redshIFt
      print *, "Comoving BoxSize=", ComovingBoxSize, "original box units"
      print *, "Chombo Box Origin=", BoxOrig
      GadgetBoxCenter = BoxOrig/ComovingBoxSize
      print *, "previous center of coords is now at position:", abs(BoxOrig/ComovingBoxSize), "(box units)"
      print *, "ngas^(1/3)=", ngas**(1./3.), " within selected region "
      print *, "Min Temp=", MINVAL(ugas(1:ngas)), "Max Temp=", MAXVAL(ugas(1:ngas))
      print *, "Min rhogas=", MINVAL(rhogas(1:ngas)), "Max rhogas=", MAXVAL(rhogas(1:ngas)), "Mean=", SUM(rhogas(1:ngas))/ngas
      print *, "Mean(rhogas<1.e3)=", SUM(rhogas(1:ngas), MASK=rhogas(1:ngas) < 1.e3)/COUNT(rhogas(1:ngas) < 1.e3)

      print *, "Min hsml=", MINVAL(hsml(1:ngas)), "Max hsml=", MAXVAL(hsml(1:ngas)), " in code units"
      thishmin = MINVAL(hsml(1:ngas))
      IF (MAXVAL(hsml(1:ngas)) - MINVAL(hsml(1:ngas)) < 1.e-4) THEN
         print *, "WARNING: CHANGING SMOOTHING LENGTHS (fixed values in tipsy?)..."
         print *, "creating SPH smoothing lengths using h=(m/rho)^(1/3)"
         hsml = (massgas/rhogas)**0.33333
         hsml = MAX(thishmin, hsml/ComovingBoxSize*2)
         print *, "NEW Min hsml=", MINVAL(hsml(1:ngas)), "NEW Max hsml=", MAXVAL(hsml(1:ngas)), " in code units"
      END IF

      print *, "Min massgas=", MINVAL(massgas(1:ngas)), "Max massgas=", MAXVAL(massgas(1:ngas)), " in tipsy code units"
      !..convert gas mass
      massgas = massgas*omega_m/omega_b*(size_basegrids/ComovingBoxSize)**3 ! RADAMESH code units (i.e., in units of MeanBarDen)
      print *, "Min massgas=", MINVAL(massgas(1:ngas)), "Max massgas=", MAXVAL(massgas(1:ngas)), " in RADAMESH code units"

      call logger%end_section ! tipsy
   END SUBROUTINE ReadTipsy

!------------------------------------

   SUBROUTINE StarGrid
! populate grids with stellar particles

      IMPLICIT NONE
      INTEGER :: i, ii, C(3), x, y, z, count, SpectrumType, a, met
      TYPE(Grid_type), POINTER :: CurrGrid
      real(kind=8) :: maxstars
      REAL(kind=4) :: this_stars, TotalIonRate, InitHIIradius, &
                      spec_attr, SourceCoords(3), Qtable(5, 1000), thisQ(5), intpfact, &
                      metbins(5), Q, ttable

      call logger%begin_section('star_grid')

      !..associate particles to cells

      IF (TRIM(starfile) /= "??") THEN

         !..read SB99 table
         OPEN (111, file=sb99, action="read")
         !..skip header
         READ (111, *)
         DO i = 1, 1000
            READ (111, *) ttable, Qtable(1:5, i)
         END DO
         CLOSE (111)
         metbins = [0.0004, 0.004, 0.008, 0.02, 0.05]

         !..use variable flag as a counter, but first we have to reset it to zero
         DO i = 1, SIZE(grid)
            grid(i)%flag = 0
         END DO

      END IF

      DO i = 1, nstars

         IF (SimTime - tform(i) > maxstage) CYCLE

         !..simple tree search
         CurrGrid => Grid(1)
         ii = 1
         findcell: DO
            C = INT(pos_stars(:, i)*size_basegrids*twotolevarr(CurrGrid%level)) - CurrGrid%LE + 2
            ii = CurrGrid%gdown(C(1), C(2), C(3))
            IF (ii > 0) THEN
               CurrGrid => grid(ii)
            ELSE
               EXIT findcell
            END IF
         END DO findcell

         !..assign star
         IF (TRIM(starfile) == "??") THEN

            CurrGrid%Data(nvarh, C(1), C(2), C(3)) = CurrGrid%Data(nvarh, C(1), C(2), C(3)) + stmass(i)

         ELSE ! use Data(nvarh-1) to store number of ionizing photons

            !..interpolate table, starting with age
            a = INT((SimTime - tform(i))*10)
            intpfact = (SimTime - tform(i))*10.-a
            thisQ(1:5) = Qtable(1:5, a) + (Qtable(1:5, a + 1) - Qtable(1:5, a))*intpfact
            !..interpolate along metallicity
            IF (stmetal(i)*0.02 <= metbins(1)) THEN
               met = 1
               intpfact = 0.
            ELSEIF (stmetal(i)*0.02 >= metbins(5)) THEN
               met = 5
               intpfact = 0.
            ELSE
               !..find minimum
               minsearch: DO met = 1, 5
                  IF (stmetal(i)*0.02 < metbins(met)) EXIT minsearch
               END DO minsearch
               met = met - 1
               intpfact = (stmetal(i)*0.02 - metbins(met))/(metbins(met + 1) - metbins(met))
            ENDIF
            Q = thisQ(met) + (thisQ(met + 1) - thisQ(met))*intpfact
            !..normalize by stmass, units of 1.e50 photons/sec
            Q = 10**(Q + log10(stmass(i)*dMSolUnit) - 6.-50.)

            CurrGrid%Data(nvarh - 1, C(1), C(2), C(3)) = CurrGrid%Data(nvarh - 1, C(1), C(2), C(3)) + Q
            CurrGrid%flag(C(1), C(2), C(3)) = CurrGrid%flag(C(1), C(2), C(3)) + 1
            CurrGrid%Data(nvarh, C(1), C(2), C(3)) = CurrGrid%Data(nvarh, C(1), C(2), C(3)) + (SimTime - tform(i))

         END IF

      END DO

      IF (TRIM(starfile) /= '??') THEN

         !..average the stellar ages
         DO i = 1, SIZE(grid)
            WHERE (grid(i)%flag /= 0)
            grid(i)%Data(nvarh, :, :, :) = grid(i)%Data(nvarh, :, :, :)/grid(i)%flag(:, :, :)
            END WHERE
         END DO

         print *, "writing stellar source catalogue: ", TRIM(starfile)
         OPEN (11, file=starfile, action="write")

         !..set defaults
         SpectrumType = 2 ! blackbody
         spec_attr = 4.e4 ! eff temperature
         InitHIIradius = 1.e-9

         count = 0
         !fconv=dMSolUnit/maxstage*1.e-6
         DO i = 1, SIZE(grid)

            IF (ANY(grid(i)%Data(nvarh - 1, :, :, :) /= 0.)) THEN

               DO z = 1, grid(i)%Dims(3)
                  DO y = 1, grid(i)%Dims(2)
                     DO x = 1, grid(i)%Dims(1)

                        this_stars = grid(i)%Data(nvarh - 1, x, y, z)

                        IF (this_stars > 0.) THEN

                           count = count + 1

                           SourceCoords(:) = (grid(i)%LE(:) - 0.5 + ([x, y, z] - 1))*grid(i)%CellSize
                           TotalIonRate = 50 + log10(this_stars)
                           spec_attr = grid(i)%Data(nvarh, x, y, z)**(-0.25)*5.7780e4 ! bb temperature in function of age

              WRITE (11, '(3(f12.9,1x),i3,f9.5,1x,2(es10.3,1x))') SourceCoords, SpectrumType, TotalIonRate, spec_attr, InitHIIradius

                        END IF

                     END DO
                  END DO
               END DO

            END IF

         END DO

         print *, "# sources=", count

      END IF

      IF (TRIM(starfile) == "??") THEN
         maxstars = 0.
         DO i = 1, NFilledGrids
            !..renormalize
            grid(i)%Data(nvarh, :, :, :) = grid(i)%Data(nvarh, :, :, :)*dMSolUnit/(grid(i)%CellSize*ComovingBoxSize*dKpcUnit)**3
            maxstars = MAX(maxstars, MAXVAL(grid(i)%Data(nvarh, :, :, :)))
         END DO
         print *, "MAXVAL star cell=", maxstars, " Msol/cKpc^3"
      END IF

      call logger%end_section(print_duration=.true.) ! star_grid
   END SUBROUTINE StarGrid

   SUBROUTINE WriteChomboHeader

      IMPLICIT NONE
      INTEGER          :: i, num_components
      CHARACTER(len=20), ALLOCATABLE :: comp_name(:)

      IF (calc_emissivity) THEN
         ALLOCATE (comp_name(nvarh))
         comp_name = [CHARACTER(len=20) :: "density","vel_x","vel_y","vel_z","temperature","stars","emissivity","velx_em","vely_em","velz_em"]
      ELSE
         ALLOCATE (comp_name(7))
         comp_name = [CHARACTER(len=20) :: "density", "vel_x", "vel_y", "vel_z", "temperature", "stars", "tform"]
      END IF

      num_components = nvarh

      !..write file attributes (RedshIFt, Cosmological Parameters, etc..) in root group
      CALL H5Screate_simple_f(1, attr_1d, space_id, err)
      CALL H5Tcopy_f(H5T_NATIVE_REAL, datatype, err)

      CALL H5Gopen_f(file_id, "/", group_id, err)

      ftype = "Chombo"
      str_size = LEN_TRIM(ftype)
      CALL H5Tcopy_f(H5T_NATIVE_CHARACTER, str_type, err)
      CALL H5Tset_size_f(str_type, str_size, err)

      CALL H5Acreate_f(group_id, "FileType", str_type, space_id, attr_id, err)
      CALL H5Awrite_f(attr_id, str_type, TRIM(ftype), attr_1d, err)
      CALL H5Aclose_f(attr_id, err)

      ftype = "Gadget/Tipsy"
      str_size = LEN_TRIM(ftype)
      CALL H5Tcopy_f(H5T_NATIVE_CHARACTER, str_type, err)
      CALL H5Tset_size_f(str_type, str_size, err)

      CALL H5Sclose_f(space_id, err)
      CALL H5Screate_f(H5S_SCALAR_F, space_id, err)

      CALL H5Acreate_f(group_id, "Generator", str_type, space_id, attr_id, err)
      CALL H5Awrite_f(attr_id, str_type, TRIM(ftype), attr_1d, err)
      CALL H5Aclose_f(attr_id, err)

      CALL H5Acreate_f(group_id, "RedshIFt", H5T_NATIVE_DOUBLE, space_id, attr_id, err)
      CALL H5Awrite_f(attr_id, H5T_NATIVE_DOUBLE, gadget_redshIFt, attr_1d, err)
      CALL H5Aclose_f(attr_id, err)

      CALL H5Acreate_f(group_id, "ComovingBoxSize", H5T_NATIVE_REAL, space_id, attr_id, err)
      CALL H5Awrite_f(attr_id, H5T_NATIVE_REAL, ComovingBoxSize, attr_1d, err)
      CALL H5Aclose_f(attr_id, err)

      CALL H5Gclose_f(group_id, err)
      !..we need the following attributes for VisIt
      CALL H5Gopen_f(file_id, "/", group_id, err)

      CALL H5Sclose_f(space_id, err)
      CALL H5Screate_f(H5S_SCALAR_F, space_id, err)

      CALL H5Acreate_f(group_id, "num_levels", H5T_NATIVE_INTEGER, space_id, attr_id, err)
      CALL H5Awrite_f(attr_id, H5T_NATIVE_INTEGER, (lmax - lmin + 1), attr_1d, err)
      CALL H5Aclose_f(attr_id, err)

      CALL H5Acreate_f(group_id, "num_unrefined_cells", H5T_NATIVE_INTEGER, space_id, attr_id, err)
      CALL H5Awrite_f(attr_id, H5T_NATIVE_INTEGER, delta, attr_1d, err)
      CALL H5Aclose_f(attr_id, err)

      CALL H5Acreate_f(group_id, "max_level", H5T_NATIVE_INTEGER, space_id, attr_id, err)
      CALL H5Awrite_f(attr_id, H5T_NATIVE_INTEGER, (lmax - lmin), attr_1d, err)
      CALL H5Aclose_f(attr_id, err)

      nstep_coarse = 1
      CALL H5Acreate_f(group_id, "iteration", H5T_NATIVE_INTEGER, space_id, attr_id, err)
      CALL H5Awrite_f(attr_id, H5T_NATIVE_INTEGER, nstep_coarse, attr_1d, err)
      CALL H5Aclose_f(attr_id, err)

      CALL H5Acreate_f(group_id, "time", H5T_NATIVE_DOUBLE, space_id, attr_id, err)
      CALL H5Awrite_f(attr_id, H5T_NATIVE_DOUBLE, gadget_time, attr_1d, err)
      CALL H5Aclose_f(attr_id, err)

      CALL H5Acreate_f(group_id, "num_components", H5T_NATIVE_INTEGER, space_id, attr_id, err)
      CALL H5Awrite_f(attr_id, H5T_NATIVE_INTEGER, num_components, attr_1d, err)
      CALL H5Aclose_f(attr_id, err)

      CALL H5Acreate_f(group_id, "NGrids", H5T_NATIVE_INTEGER, space_id, attr_id, err)
      CALL H5Awrite_f(attr_id, H5T_NATIVE_INTEGER, tail(lmax), attr_1d, err)
      CALL H5Aclose_f(attr_id, err)

      !..here are some extra parameters:
      CALL H5Acreate_F(group_id, "BoxOrigin_OriginalUnits", H5T_NATIVE_DOUBLE, space_id, attr_id, err)
      CALL H5Awrite_f(attr_id, H5T_NATIVE_DOUBLE, BoxOrig, attr_3d, err)
      CALL H5Aclose_f(attr_id, err)

      DO i = 0, num_components - 1

         WRITE (dummy_string, '(i1.1)') i
         attr_name = "component_"//TRIM(dummy_string)

         str_size = LEN_TRIM(comp_name(i + 1))
         CALL H5Tcopy_f(H5T_NATIVE_CHARACTER, str_type, err)
         CALL H5Tset_size_f(str_type, str_size, err)

         CALL H5Acreate_f(group_id, TRIM(attr_name), str_type, space_id, attr_id, err)
         CALL H5Awrite_f(attr_id, str_type, TRIM(comp_name(i + 1)), attr_1d, err)
         CALL H5Aclose_f(attr_id, err)

      END DO

      CALL H5Sclose_f(space_id, err)
      CALL H5Screate_simple_f(1, attr_3d, space_id, err)

      CALL H5Acreate_f(group_id, "ProblemDomain", H5T_NATIVE_INTEGER, space_id, &
                       attr_id, err)
      CALL H5Awrite_f(attr_id, H5T_NATIVE_INTEGER, prob_domain, attr_3d, err)
      CALL H5Aclose_f(attr_id, err)

      CALL H5Gclose_f(group_id, err)

      CALL H5Sclose_f(space_id, err)
      CALL H5Screate_simple_f(1, attr_1d, space_id, err)

      CALL H5Gcreate_f(file_id, "/Chombo_global", group_id, err)

      CALL H5Acreate_f(group_id, "SpaceDim", H5T_NATIVE_INTEGER, space_id, attr_id, err)
      CALL H5Awrite_f(attr_id, H5T_NATIVE_INTEGER, 3, attr_1d, err)
      CALL H5Aclose_f(attr_id, err)

      CALL H5Gclose_f(group_id, err)

      !..create compound type for prob_domain and boxes

      ! Set dataset transfer property to preserve partially initialized fields
      ! during write/read to/from dataset with compound datatype.

      CALL H5PCreate_f(H5P_DATASET_XFER_F, plist_id, err)
      CALL H5PSet_preserve_f(plist_id, flag, err)

      CALL H5TGet_size_f(H5T_NATIVE_INTEGER, compound_size, err)
      compound_size = compound_size*6

      CALL H5Tcreate_f(H5T_COMPOUND_F, compound_size, comp_id, err)

      compound_size = compound_size/6
      offset = 0
      CALL H5Tinsert_f(comp_id, "lo_i", offset, H5T_NATIVE_INTEGER, err)
      offset = compound_size
      CALL H5Tinsert_f(comp_id, "lo_j", offset, H5T_NATIVE_INTEGER, err)
      offset = compound_size*2
      CALL H5Tinsert_f(comp_id, "lo_k", offset, H5T_NATIVE_INTEGER, err)
      offset = compound_size*3
      CALL H5Tinsert_f(comp_id, "hi_i", offset, H5T_NATIVE_INTEGER, err)
      offset = compound_size*4
      CALL H5Tinsert_f(comp_id, "hi_j", offset, H5T_NATIVE_INTEGER, err)
      offset = compound_size*5
      CALL H5Tinsert_f(comp_id, "hi_k", offset, H5T_NATIVE_INTEGER, err)

      CALL H5Sclose_f(space_id, err)
      ! NB: plist_id & comp_id left open
   END SUBROUTINE WriteChomboHeader

   SUBROUTINE WriteChombo(this_lev)

      IMPLICIT NONE
      INTEGER, INTENT(IN) :: this_lev
      REAL(kind=8)        :: dx
      INTEGER, PARAMETER  :: ref_ratio = 2
      INTEGER, ALLOCATABLE :: boxes(:, :)
      INTEGER             :: i, this_size, count, rank, count_id
      INTEGER(HSIZE_T)    :: this_attr(1)

      WRITE (dummy_string, '(i1.1)') this_lev
      groupname = "/level_"//TRIM(dummy_string)

      CALL H5Gcreate_f(file_id, groupname, group_id, err)

      print *, "writing Chombo ", TRIM(groupname)

      CALL H5Screate_simple_f(1, attr_1d, space_id, err)

      CALL H5Acreate_f(group_id, "ref_ratio", H5T_NATIVE_INTEGER, space_id, attr_id, err)
      CALL H5Awrite_f(attr_id, H5T_NATIVE_INTEGER, ref_ratio, attr_1d, err)
      CALL H5Aclose_f(attr_id, err)

      dx = 1.d0/REAL(prob_domain(1), KIND=8)/REAL(ref_ratio**this_lev, KIND=8)

      CALL H5Acreate_f(group_id, "dx", H5T_NATIVE_DOUBLE, space_id, attr_id, err)
      CALL H5Awrite_f(attr_id, H5T_NATIVE_DOUBLE, dx, attr_1d, err)
      CALL H5Aclose_f(attr_id, err)

      IF (ALLOCATED(boxes)) DEALLOCATE (boxes)

      ngrids = tail(this_lev + lmin) - head(this_lev + lmin) + 1
      ALLOCATE (boxes(ngrids, 6))
      DO i = 1, ngrids
         ! these run on the wrong index for fortran and are inefficient, however this
         ! avoid to make a subsequent copy of the array for the Chombo compound type
         boxes(i, 1:3) = grid(i + tail(this_lev + lmin - 1))%LE - 1
         boxes(i, 4:6) = grid(i + tail(this_lev + lmin - 1))%RE - 1

      END DO

      CALL h5tcopy_f(H5T_NATIVE_INTEGER, dt2_id, err)
      CALL h5tget_size_f(dt2_id, type_size, err)
      CALL h5tcreate_f(H5T_COMPOUND_F, type_size, dt1_id, err)

      this_attr = ngrids
      CALL H5Screate_simple_f(1, this_attr, boxspace_id, err)

      offset = 0
      CALL H5Dcreate_f(group_id, "boxes", comp_id, boxspace_id, dset_id, err)

      dims = ngrids
      CALL h5tclose_f(dt1_id, err)
      CALL h5tcreate_f(H5T_COMPOUND_F, type_size, dt1_id, err)
      CALL h5Tinsert_f(dt1_id, "lo_i", offset, dt2_id, err)
      CALL h5Dwrite_f(dset_id, dt1_id, boxes(:, 1), dims, err, xfer_prp=plist_id)

      CALL h5tclose_f(dt1_id, err)
      CALL h5tcreate_f(H5T_COMPOUND_F, type_size, dt1_id, err)
      CALL h5Tinsert_f(dt1_id, "lo_j", offset, dt2_id, err)
      CALL h5Dwrite_f(dset_id, dt1_id, boxes(:, 2), dims, err, xfer_prp=plist_id)

      CALL h5tclose_f(dt1_id, err)
      CALL h5tcreate_f(H5T_COMPOUND_F, type_size, dt1_id, err)
      CALL h5Tinsert_f(dt1_id, "lo_k", offset, dt2_id, err)
      CALL h5Dwrite_f(dset_id, dt1_id, boxes(:, 3), dims, err, xfer_prp=plist_id)

      CALL h5tclose_f(dt1_id, err)
      CALL h5tcreate_f(H5T_COMPOUND_F, type_size, dt1_id, err)
      CALL h5Tinsert_f(dt1_id, "hi_i", offset, dt2_id, err)
      CALL h5Dwrite_f(dset_id, dt1_id, boxes(:, 4), dims, err, xfer_prp=plist_id)

      CALL h5tclose_f(dt1_id, err)
      CALL h5tcreate_f(H5T_COMPOUND_F, type_size, dt1_id, err)
      CALL h5Tinsert_f(dt1_id, "hi_j", offset, dt2_id, err)
      CALL h5Dwrite_f(dset_id, dt1_id, boxes(:, 5), dims, err, xfer_prp=plist_id)

      CALL h5tclose_f(dt1_id, err)
      CALL h5tcreate_f(H5T_COMPOUND_F, type_size, dt1_id, err)
      CALL h5Tinsert_f(dt1_id, "hi_k", offset, dt2_id, err)
      CALL h5Dwrite_f(dset_id, dt1_id, boxes(:, 6), dims, err, xfer_prp=plist_id)

      CALL H5Dclose_f(dset_id, err)

      CALL h5tclose_f(dt1_id, err)
      DEALLOCATE (boxes)
!..write down the data
      totsize = 0
      DO i = head(this_lev + lmin), tail(this_lev + lmin)
         totsize = totsize + PRODUCT(grid(i)%RE - grid(i)%LE + 1)
      END DO
      IF (ALLOCATED(DataArray)) DEALLOCATE (DataArray, GridId, Hier_Up, Hier_Next, Hier_Down)
      ALLOCATE (DataArray(totsize*nvarh))
      ALLOCATE (GridId(ngrids), Hier_Up(ngrids), Hier_Next(ngrids), &
                Hier_Down(ngrids))
!..fill auxiliary arrays and DataArray
      count = 0
      count_id = 0
      DO i = head(this_lev + lmin), tail(this_lev + lmin)

         !..Id
         count_id = count_id + 1
         GridId(count_id) = i

         !..Hierarchy
         IF (ASSOCIATED(Grid(i)%Down)) THEN
            Curr => Grid(i)%Down
            Hier_Down(count_id) = Curr%Id
         ELSE
            Hier_Down(count_id) = -1
         ENDIF
         IF (ASSOCIATED(Grid(i)%Next)) THEN
            Curr => Grid(i)%Next
            Hier_Next(count_id) = Curr%Id
         ELSE
            Hier_Next(count_id) = -1
         ENDIF
         IF (ASSOCIATED(Grid(i)%Up)) THEN
            Curr => Grid(i)%Up
            Hier_Up(count_id) = Curr%Id
         ELSE
            Hier_Up(count_id) = -1
         ENDIF

         this_size = PRODUCT(grid(i)%RE - grid(i)%LE + 1)
         DO ii = 1, nvarh
            DataArray(count + 1:count + this_size) = RESHAPE(grid(i)%Data(ii, :, :, :), (/this_size/))
            count = count + this_size
         END DO

      END DO

      dims(1) = SIZE(DataArray)
      CALL H5Screate_simple_f(1, dims(1), dspace_id, err)
#ifdef SP
      CALL H5Dcreate_f(group_id, "data:datatype=0", H5T_NATIVE_REAL, dspace_id, dset_id, err)
      CALL H5Dwrite_f(dset_id, H5T_NATIVE_REAL, DataArray, dims, err)
#else
      CALL H5Dcreate_f(group_id, "data:datatype=0", H5T_NATIVE_DOUBLE, dspace_id, dset_id, err)
      CALL H5Dwrite_f(dset_id, H5T_NATIVE_DOUBLE, DataArray, dims, err)
#endif
      CALL H5Dclose_f(dset_id, err)

!..write down auxiliary arrays
      dims(1) = ngrids
      rank = 1
      CALL H5LTmake_dataset_int_f(group_id, "GridId", rank, dims, GridId, err)
      CALL H5LTmake_dataset_int_f(group_id, "Hier_Up", rank, dims, Hier_Up, err)
      CALL H5LTmake_dataset_int_f(group_id, "Hier_Down", rank, dims, Hier_Down, err)
      CALL H5LTmake_dataset_int_f(group_id, "Hier_Next", rank, dims, Hier_Next, err)

      CALL H5Gclose_f(group_id, err)

      CALL H5Sclose_f(space_id, err)
      CALL H5Sclose_f(dspace_id, err)
      CALL H5Sclose_f(boxspace_id, err)

   END SUBROUTINE WriteChombo

END PROGRAM P2C
