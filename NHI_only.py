# import
import h5py
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import colors

size_data = 512
sol_mass = 1.9891E33
proton_mass = 1.6726E-24
speed_of_light = 3E10
h=0.6777
Mpc_to_cm = 3.086 * (1E24)

def load_file(filename, cellsize):
    f = h5py.File(filename, "r")
    #print("Itemes: ", list(f.items()))
    print("--------------------------")
    base=f.get('level_0')
    data = base.get('data:datatype=0')

    density = np.array(data[:size_data**3]).reshape((size_data, size_data, size_data),order='F')
    hydro_density = np.array(data[-size_data**3:]).reshape((size_data, size_data, size_data),order='F')
    return hydro_density, density

def plot_colormap(folder,matrix_to_plot, color_label,x_label, y_label, title_string, min_max_1,min_max_2,is_log_10=True, v_min=0, v_max=0, axis=0):
    dir_name = folder+str(axis)+"/"
    plt.clf()
    matrix_to_plot = np.where(np.log10(matrix_to_plot)<=16.2,1E-31,matrix_to_plot)

    if is_log_10:
        plt.imshow(np.log10(matrix_to_plot).T, vmin=v_min, vmax=v_max,cmap='viridis', origin='lower', extent=[min_max_1[0],min_max_1[1],min_max_2[0],min_max_2[1]])
    else:
        plt.imshow(matrix_to_plot.T, clim=(v_min,v_max), cmap='viridis', origin='lower', extent=[min_max_1[0],min_max_1[1],min_max_2[0],min_max_2[1]])

    cbar = plt.colorbar(label=color_label)
    plt.title(title_string)
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.xlim(min_max_1[0],min_max_1[1])
    plt.ylim(min_max_2[0],min_max_2[1])
    plt.savefig(dir_name+"2D_"+title_string)
    cbar.remove()

def extract_2d_array(A, j, index):
    return A[index, :, :] if j == 0 else A[:, index, :] if j == 1 else A[:, :, index]


def return_density(Mass_array,cell_length,dimensions):
    Unit_mass_to_g = sol_mass
    
    print("Total Mass", "{:e}".format(np.sum(Mass_array)))

    # compute Number of particles
    Particles = Mass_array * Unit_mass_to_g / proton_mass
    # compute density
    Gas_density = Particles / (cell_length ** dimensions)                                     
    density_array = np.where(Gas_density <= 1E-30, 1E-30, Gas_density)

    return density_array

def Compute_column_density(Mass, cell_size,axis_par,n_slices):
    # compute mass along line of sight
    Mass_los = np.cumsum(Mass, axis=axis_par)
    Mass_los_2 = np.sum(Mass, axis=axis_par)

    cumulative_density = return_density(Mass_los,cell_size,2)
    density_slices = []
    min_max_1=[0,4]
    min_max_2=[0,4]
    print("N_slices: ",n_slices) 
    print(cumulative_density.shape)
    for i in range(n_slices):
        density_slices.append(extract_2d_array(cumulative_density,axis_par,int((i+1)*size_data/(n_slices)-1))-extract_2d_array(cumulative_density,axis_par,int((i)*size_data/(n_slices))))
        plot_colormap("Comparison_z_4_CDM_WDM_zoom_1p5kev/axis_",density_slices[i], "color_label","x_label","y_label", "title_string"+str(i), min_max_1,min_max_2, v_min=17, v_max=22, axis=axis_par)
    
    return density_slices 

def plot_two_histograms(directory,array_1,array_2,label_1,label_2,title,x_label,bin_number,minimum_value=-22,maximum_value=-12,plot_xlim=1,is_log=True):
    plt.clf()
    if is_log:  
        plt.hist(np.log10(array_1), bins=bin_number, range=(minimum_value,maximum_value), histtype='step', density=False,label=label_1)
        plt.hist(np.log10(array_2), bins=bin_number, range=(minimum_value,maximum_value), histtype='step', density=False,label=label_2)
    else:
        plt.hist(array_1, bins=bin_number, histtype='step', density=False,label=label_1)
        plt.hist(array_2, bins=bin_number, histtype='step', density=False,label=label_2)

    plt.title(title)
    #plt.yscale("log")
    plt.ylim(0,3.8E6)
    if plot_xlim:
        plt.xlim(minimum_value,maximum_value)
        #plt.ylim(1E-1,5E4)
    plt.xlabel(x_label)
    plt.legend()
    plt.title(title)
    plt.savefig(directory+"Distributions " + title)
    plt.clf()

# Load files
#z, kev, folder
settings = ["4", "1", "1"]
z = [int(settings[0]),int(settings[0])]

folder_name = "Comparison_z_"+settings[0]+"_CDM_WDM_zoom_"+settings[1]+"p5kev/axis_"
folder_location=["/data/fbara/simulations/CDM/","/data/fbara/simulations/WDM/"]
filenames =["CDM_z_"+settings[0]+"_zoom_1p5kev","WDM_z_"+settings[0]+"_zoom_"+settings[1]+"p5kev"]

if int(settings[0]) == 3:
    min_max_list=[[66.2415993685/h,68.95239936853/h],[64.0552471324/h,66.7660471324/h],[68.35748598955/h,71.06828598955/h]]
if int(settings[0]) == 4:
    min_max_list=[[66.22052349/h,68.92852349/h],[64.74644574/h,67.45444574/h],[68.13438534/h,70.84238534/h]]

size_in_Mpc=min_max_list[0][1]-min_max_list[0][0]
cell_size_cm = (size_in_Mpc / 512) * Mpc_to_cm

NHI_Column_densities_map= [[[],[],[]],[[],[],[]]]
Gas_Column_densities_map= [[[],[],[]],[[],[],[]]]

data = [load_file(folder_location[0]+filenames[0],cell_size_cm),load_file(folder_location[1]+filenames[1],cell_size_cm)]
Neutral_mass = [data[0][0], data[1][0]]
Gas_mass = [data[0][1], data[1][1]]

Gas_densities_map=[return_density(Neutral_mass[0], cell_size_cm, 3), return_density(Neutral_mass[1], cell_size_cm, 3)]

NHI_3D_label =r'Log(New Neutral Hydrogen Densities[$cm^{-3}$]'
plot_two_histograms("Comparison_z_"+settings[0]+"_CDM_WDM_zoom_"+settings[1]+"p5kev/",np.ravel(Gas_densities_map[0]),np.ravel(Gas_densities_map[1]),'CDM','WDM '+str(settings[1])+'keV',"NHI densities z="+str(settings[0]),NHI_3D_label,200,minimum_value=-15,maximum_value=-5,plot_xlim=1,is_log=True)


plot_two_histograms("Comparison_z_"+settings[0]+"_CDM_WDM_zoom_"+settings[1]+"p5kev/",np.ravel(Gas_densities_map[0]),np.ravel(Gas_densities_map[1]),'CDM','WDM '+str(settings[1])+'keV',"NHI densities z="+str(settings[0]),NHI_3D_label,200,minimum_value=-15,maximum_value=-5,plot_xlim=1,is_log=True)

N_of_slices = 64 
for j in range(2):
    for i in range(3):
        Neutral_Column_density = Compute_column_density(Neutral_mass[j], cell_size_cm, i, N_of_slices)
        
        NHI_Column_densities_map[j][i] = Neutral_Column_density

dir_name_1 = "Comparison_z_"+settings[0]+"_CDM_WDM_zoom_"+settings[1]+"p5kev/"

#----------------------------------------------------------------------------------------------------------------------
for i in range(N_of_slices):
    hist_axis_0_cdm,_=np.histogram(np.log10(np.where(np.ravel(NHI_Column_densities_map[0][0][i])<=1,1,np.ravel(NHI_Column_densities_map[0][0][i]))), bins=100, range=(8,16), density=False)
    hist_axis_1_cdm,_=np.histogram(np.log10(np.where(np.ravel(NHI_Column_densities_map[0][1][i])<=1,1,np.ravel(NHI_Column_densities_map[0][1][i]))), bins=100, range=(8,16), density=False)
    hist_axis_2_cdm,_=np.histogram(np.log10(np.where(np.ravel(NHI_Column_densities_map[0][2][i])<=1,1,np.ravel(NHI_Column_densities_map[0][2][i]))), bins=100, range=(8,16), density=False)

    hist1cdm_nhi= (hist_axis_0_cdm + hist_axis_1_cdm + hist_axis_2_cdm) / 3
    hist1cdm_dev_nhi = np.std(np.stack((hist_axis_0_cdm, hist_axis_1_cdm, hist_axis_2_cdm)),axis=0) / np.sqrt(3)

    hist_axis_0_wdm,_=np.histogram(np.log10(np.where(np.ravel(NHI_Column_densities_map[1][0][i])<=1,1,np.ravel(NHI_Column_densities_map[1][0][i]))), bins=100, range=(8,16), density=False)
    hist_axis_1_wdm,_=np.histogram(np.log10(np.where(np.ravel(NHI_Column_densities_map[1][1][i])<=1,1,np.ravel(NHI_Column_densities_map[1][1][i]))), bins=100, range=(8,16), density=False)
    hist_axis_2_wdm,_=np.histogram(np.log10(np.where(np.ravel(NHI_Column_densities_map[1][2][i])<=1,1,np.ravel(NHI_Column_densities_map[1][2][i]))), bins=100, range=(8,16), density=False)

    hist2wdm_nhi= (hist_axis_0_wdm + hist_axis_1_wdm + hist_axis_2_wdm) / 3
    hist2wdm_dev_nhi = np.std(np.stack((hist_axis_0_wdm, hist_axis_1_wdm, hist_axis_2_wdm)),axis=0) / np.sqrt(3)

    plt.clf()
    bincentres = [(_[i]+_[i+1])/2. for i in range(len(_)-1)]
    #plt.step(bincentres,hist1cdm,where='mid',label='CDM')
    plt.fill_between(bincentres, hist1cdm_nhi-hist1cdm_dev_nhi, hist1cdm_nhi+hist1cdm_dev_nhi,color='blue', alpha=0.5, label="CDM")
    #plt.step(bincentres,hist2wdm,where='mid',label='WDM')
    plt.fill_between(bincentres, hist2wdm_nhi-hist2wdm_dev_nhi, hist2wdm_nhi+hist2wdm_dev_nhi,color='orange', alpha=0.5, label="WDM "+str(settings[1])+".5 keV")

    plt.title("NHI Distribution z= "+str(settings[0])+", N_of_slics="+str(N_of_slices))
    plt.xlabel(r"log(Neutral Hydrogen Column Density [$cm^{-2}$])")
    plt.ylim(0,18000)
    plt.legend()
    plt.savefig(dir_name_1+"NHI Distribution column density "+str(i))
    plt.clf()
