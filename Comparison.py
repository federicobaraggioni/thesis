# import
import h5py
import numpy as np
import matplotlib.pyplot as plt
from astropy.io import fits
from matplotlib import colors
from sklearn.mixture import GaussianMixture
from sklearn.neighbors import KernelDensity
from scipy import stats
from scipy.optimize import curve_fit
from sklearn.gaussian_process import GaussianProcessRegressor
from sklearn.gaussian_process.kernels import ConstantKernel, RBF


size_data = 512
sol_mass = 1.9891E33
proton_mass = 1.6726E-24
speed_of_light = 3E10
h=0.6777
Mpc_to_cm = 3.086 * (1E24)

def sigma_function(x,A,K,Q,b,n):
    return A+(K-A)/((1+Q*np.e**(-b*(x-1)))**(1/n))

def index_list(input_index):
    if input_index ==0:
        return [1,2]
    if input_index ==1:
        return [0,2]
    if input_index ==2:
        return [0,1]
def convert_dataset(points):
    X = points[::2]
    Odd = points[1::2]
    Y = 10**np.array(Odd)
    return X,Y

def save_lists(list1, list2, filename):
  with open(filename, 'w') as f:
    # Combine lists with newline character as separator
    f.write(",".join(str(item) for item in list1) + "\n")
    f.write(",".join(str(item) for item in list2) + "\n")

def load_lists(filename):   
  with open(filename, 'r') as f:
    loaded_data = f.readlines()
    loaded_lists = [line.strip().split(",") for line in loaded_data]
  return np.array(loaded_lists[0],dtype="float"),np.array(loaded_lists[1],dtype="float")

X_Gallego_z_3,Y_Gallego_z_3=load_lists("Gallego_3.txt")
X_Gallego_z_6,Y_Gallego_z_6=load_lists("Gallego_6.txt")
X_Rahmati,Y_Rahmati=load_lists("Rahmati.txt")
X_Observations,Y_Observations=load_lists("Observations.txt")

def load_file(filename,minx,miny,minz,max_x,max_y,max_z,cellsize,halo_location):
    f = h5py.File(filename, "r")
    #print("Itemes: ", list(f.items()))
    print("--------------------------")
    base=f.get('level_0')
    data = base.get('data:datatype=0')

    density = np.array(data[:size_data**3]).reshape((size_data, size_data, size_data),order='F')
    hydro_density = np.array(data[-size_data**3:]).reshape((size_data, size_data, size_data),order='F')
    #hydro_density = remove_CGM(hydro_density,minx,miny,minz,max_x,max_y,max_z,cellsize,halo_location)
    #temperature = np.array(data[4*size_data**3:5*size_data**3]).reshape((size_data, size_data, size_data),order='F')
    temperature = 1
    return hydro_density, density,temperature
    
def remove_CGM(hydrogen_matrix,min_x,min_y,min_z,max_x,max_y,max_z,cellsize,location):
    x,y,z,R = load_galaxies_positions_and_radius(location,min_x,min_y,min_z,max_x,max_y,max_z)
    px,py,pz = compute_grid_pos_3d(x,y,z,cellsize,min_x,min_y,min_z)
    hydrogen_matrix = set_gal_pos_to_zero(hydrogen_matrix,px,py,pz,R,cellsize)
    return hydrogen_matrix

def load_galaxies_positions_and_radius(location,minx,miny,minz,max_x,max_y,max_z):
    columns = (5, 6, 7, 64, 11)
    # Read the data using genfromtxt and select specific columns
    data = np.genfromtxt(location, usecols=columns)
    galaxies_mass = data[:,3]
    # selct galaxies with stellar mass and convert to physical Mpc from kpc/h
    min_mass=3E8
    xC = data[:,0][galaxies_mass>min_mass]/(1000*h)
    yC = data[:,1][galaxies_mass>min_mass]/(1000*h)
    zC = data[:,2][galaxies_mass>min_mass]/(1000*h)
    Rh = data[:,4][galaxies_mass>min_mass]/(1000*h)
    Gal_masses = data[:,3][galaxies_mass>min_mass]/h
    #select data with ZC in the zoom in region
    xC = xC[(zC>minz) & (zC<max_z)]
    yC = yC[(zC>minz) & (zC<max_z)] 
    Gal_masses = Gal_masses[(zC>minz) & (zC<max_z)]
    Rh = Rh[(zC>minz) & (zC<max_z)]
    zC = zC[(zC>minz) & (zC<max_z)] 

    yC = yC[(xC>minx) & (xC<max_x)] 
    Gal_masses = Gal_masses[(xC>minx) & (xC<max_x)]
    zC = zC[(xC>minx) & (xC<max_x)] 
    Rh = Rh[(xC>minx) & (xC<max_x)]
    xC = xC[(xC>minx) & (xC<max_x)]

    xC = xC[(yC>miny) & (yC<max_y)]
    Gal_masses = Gal_masses[(yC>miny) & (yC<max_y)]
    zC = zC[(yC>miny) & (yC<max_y)]   
    Rh = Rh[(yC>miny) & (yC<max_y)]
    yC = yC[(yC>miny) & (yC<max_y)] 
    print("Galaxy Mass")
    print("Max\tMin")
    print(max(Gal_masses),min(Gal_masses))
    print(len(xC))

    return xC,yC,zC,Rh

def compute_grid_pos_3d(x,y,z,cell_size,le_cut_x,le_cut_y,le_cut_z):
    cell_size_Mpc=cell_size/(Mpc_to_cm)
    #print("cells_size: "+str(cell_size_kpc)+"x: "+str(x)+"y: "+str(y))
    px = np.floor((x-le_cut_x)/(cell_size_Mpc))
    py = np.floor((y-le_cut_y)/(cell_size_Mpc))
    pz = np.floor((z-le_cut_z)/(cell_size_Mpc))
    return px,py,pz

def set_gal_pos_to_zero(matrix,px,py,pz,R,cellsize):
    cell_size_Mpc=cellsize/(Mpc_to_cm)
    for halo in range(len(px)):
        ext = R[halo]/(cell_size_Mpc)
        x_min = max(int(px[halo]-ext),0)
        x_max = min(int(px[halo]+ext),511)
        y_min = max(int(py[halo]-ext),0)
        y_max = min(int(py[halo]+ext),511)
        z_min = max(int(pz[halo]-ext),0)
        z_max = min(int(pz[halo]+ext),511)
        for i in range(x_min,x_max):
            for j in range(y_min,y_max):
                for k in range(z_min,z_max):
                    if (i-px[halo])**2+(j-py[halo])**2+(k-pz[halo])**2<ext**2:
                        matrix[i,j,k] = 0
    return matrix

def Gauss_process(z,mu_sample,dmu,Const_var,Rbf_par):
    kernel1 =ConstantKernel(Const_var, (1e-3, 1e3)) * RBF(Rbf_par, (0.01, 100)) 
    gp = GaussianProcessRegressor(kernel=kernel1,alpha=dmu**2)
    gp.fit(z,mu_sample)
    return gp

def plot_colormap(folder,matrix_to_plot, color_label,x_label, y_label, title_string, min_max_1,min_max_2,is_log_10=True, v_min=0, v_max=0, axis=0):
    dir_name = folder+str(axis)+"/"
    plt.clf()
    matrix_to_plot = np.where(np.log10(matrix_to_plot)<=16.2,1E-31,matrix_to_plot)

    if is_log_10:
        plt.imshow(np.log10(matrix_to_plot).T, vmin=v_min, vmax=v_max,cmap='viridis', origin='lower', extent=[min_max_1[0],min_max_1[1],min_max_2[0],min_max_2[1]])
    else:
        plt.imshow(matrix_to_plot.T, clim=(v_min,v_max), cmap='viridis', origin='lower', extent=[min_max_1[0],min_max_1[1],min_max_2[0],min_max_2[1]])

    cbar = plt.colorbar(label=color_label)
    plt.title(title_string)
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.xlim(min_max_1[0],min_max_1[1])
    plt.ylim(min_max_2[0],min_max_2[1])
    plt.savefig(dir_name+"2D "+title_string)
    cbar.remove()

def extract_2d_array(A, j, index):
    return A[index, :, :] if j == 0 else A[:, index, :] if j == 1 else A[:, :, index]


def return_density(Mass_array,cell_length,dimensions):
    Unit_mass_to_g = sol_mass
    
    print("Total Mass", "{:e}".format(np.sum(Mass_array)))

    # compute Number of particles
    Particles = Mass_array * Unit_mass_to_g / proton_mass
    # compute density
    Gas_density = Particles / (cell_length ** dimensions)                                     
    density_array = np.where(Gas_density <= 1E-30, 1E-21, Gas_density)

    return density_array

def Compute_column_density(Mass, cell_size,axis_par):
    # compute mass along line of sight
    Mass_los = np.cumsum(Mass, axis=axis_par)
    Mass_los_2 = np.sum(Mass, axis=axis_par)

    cumulative_density = return_density(Mass_los,cell_size,2)
    density_slices = []
    n_slices = 8 
    min_max_1=[0,4]
    min_max_2=[0,4]
    """
    for i in range(n_slices):
        density_slices.append(extract_2d_array(cumulative_density,axis_par,int((i+1)*size_data/n_slices-1))-extract_2d_array(cumulative_density,axis_par,int((i)*size_data/n_slices-1)))
        print("Massimo,minimo",axis_par)
        print(np.max(density_slices[i]))
        print(density_slices[i].shape)
        plot_colormap("Comparison_z_4_CDM_WDM_zoom/axis_",density_slices[i], "color_label","x_label","y_label", "title_string"+str(i), min_max_1,min_max_2, v_min=17, v_max=22, axis=axis_par)
    """
    density_slices.append(extract_2d_array(cumulative_density,axis_par,-1)) 
    return density_slices

def Compute_density(Mass, cell_size):
    return return_density(Mass,cell_size,3)

def plot_two_histograms(directory,array_1,array_2,label_1,label_2,title,x_label,bin_number,minimum_value=-22,maximum_value=-12,plot_xlim=1,is_log=True):
    plt.clf()
    if is_log:  
        plt.hist(np.log10(array_1), bins=bin_number, range=(minimum_value,maximum_value), histtype='step', density=False,label=label_1)
        plt.hist(np.log10(array_2), bins=bin_number, range=(minimum_value,maximum_value), histtype='step', density=False,label=label_2)
    else:
        plt.hist(array_1, bins=bin_number, histtype='step', density=False,label=label_1)
        plt.hist(array_2, bins=bin_number, histtype='step', density=False,label=label_2)

    plt.title(title)
    #plt.yscale("log")
    plt.ylim(0,3.8E6)
    if plot_xlim:
        plt.xlim(minimum_value,maximum_value)
        #plt.ylim(1E-1,5E4)
    plt.xlabel(x_label)
    plt.legend()
    plt.title(title)
    plt.savefig(directory+"Distributions " + title)
    plt.clf()

def plot_two_kdes(directory, x_coord, kde_1, kde_2, label_1, label_2, title, x_label, minimum_value, maximum_value):
    plt.plot(x_coord, np.exp(kde_1),label=label_1)   
    plt.plot(x_coord, np.exp(kde_2),label=label_2)
    plt.ylim(1E-4,1E0)
    plt.xlim(minimum_value,maximum_value)
    plt.yscale("log")
    plt.xlabel(x_label)
    plt.legend()
    plt.title(title)
    plt.savefig(directory+"Distributions KDE "+ title)
    plt.clf()

def plot_residuals(directory, x_coord, residuals_dist, x_label, title):
    X_plot_2 = np.linspace(-0.85,0.85,1000)[:,np.newaxis]
    kde_3=KernelDensity(kernel="gaussian", bandwidth=0.06).fit(residuals_dist[:, np.newaxis])
    log_dens_3 = kde_3.score_samples(X_plot_2)

    plt.axvline(x=0.0, color="red")
    plt.plot(X_plot_2[:,0],np.exp(log_dens_3))
    plt.hist(residuals_dist, bins=200,histtype='step', density=True)
    plt.xlim(-0.85,0.85)
    plt.title("Relative Residuals")
    plt.savefig(directory+"Residuals_distributions " + title)
    plt.clf()

    plt.scatter(x=X_plot[:,0],y=residuals_dist,s=1,alpha=0.5)
    plt.axhline(y=0.0,color="red",alpha=0.5)
    plt.ylim(-1.5,1.5)
    plt.xlabel(x_label)
    plt.title("Relative Residuals")
    plt.savefig(directory+"Residuals_function "+title)
    plt.clf()

def bidimensional_histograms(folder, map_1, map_2, x_label, y_label, title_string, v_min, v_max):
    dir_name = folder+"0/"
    plt.clf()

    x = np.log10(np.ravel(map_1))
    y = np.log10(np.ravel(map_2))
    """
    H, xedges, yedges = np.histogram2d(x, y, bins=200)
    H = H.T
    plt.imshow(np.log10(H), interpolation='nearest', origin='lower',cmap='viridis', extent=[xedges[0], xedges[-1], yedges[0], yedges[-1]])
    """
    plt.hist2d(x,y,norm = colors.LogNorm(vmin=1E0),cmap='viridis',range=[[-8,2],[0,8]],bins=200)
    cbr=plt.colorbar()
    plt.title(title_string)
    #plt.yscale("log")
    #plt.xlim(v_min,v_max)
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.legend()
    plt.title(title_string)
    plt.savefig(dir_name+"histogarms" + title_string)
    plt.clf()

def plot_distribution_2(folder,map_1,map_2,color_label,title_string,n_bins,v_min,v_max,dist_1_label,dist_2_label,redshift,axis=0,is_log=False):
    dir_name = folder+str(axis)+"/"
    flat_matrix_1 = np.where(np.ravel(map_1)<=1,1,np.ravel(map_1))
    flat_matrix_2 = np.where(np.ravel(map_2)<=1,1,np.ravel(map_2))
    min_value = max(min(np.log10(flat_matrix_1)),min(np.log10(flat_matrix_2)),v_min)
    max_value = max(max(np.log10(flat_matrix_1)),max(np.log10(flat_matrix_2)),v_max)
    if np.isnan(min_value) or np.isnan(max_value):
        print("NAN")
        input(str(axis))
        return 0
    
    plot_two_histograms(dir_name, flat_matrix_1, flat_matrix_2, dist_1_label, dist_2_label, title_string, color_label, n_bins, min_value,max_value)
        
    X_plot = np.linspace(min_value,max_value, 1000)[:, np.newaxis]
    hist_1,bin_edges_1 = np.histogram(np.log10(flat_matrix_1),bins=n_bins,range=(17.2,max_value),density=False)
    hist_2,bin_edges_2 = np.histogram(np.log10(flat_matrix_2),bins=n_bins,range=(17.2,max_value),density=False)
    print("-------------------")
    #print("Covering Fraction")
    #print(folder, axis)
    #print("CDM: ",np.sum(hist_1)/(512**2))
    #print("WDM: ",np.sum(hist_2)/(512**2))
    #print("relative difference")
    #print((np.sum(hist_1)/(512**2)-(np.sum(hist_2)/(512**2)))/(np.sum(hist_2)/(512**2)))
    #print("-------------------")
    bin_width=bin_edges_1[1]-bin_edges_1[0]
    try:
        kde_1 = KernelDensity(kernel="gaussian", bandwidth=0.03).fit(np.log10(flat_matrix_1)[:, np.newaxis])
        kde_2 = KernelDensity(kernel="gaussian", bandwidth=0.03).fit(np.log10(flat_matrix_2)[:, np.newaxis])
    except:
        print("Error")
        input(str(axis))
        return 0

    log_dens_1 = kde_1.score_samples(X_plot)
    log_dens_2 = kde_2.score_samples(X_plot)

    plot_two_kdes(dir_name, X_plot[:,0], log_dens_1, log_dens_2, dist_1_label, dist_2_label, title_string, color_label, min_value, max_value)
    
    hist_1,bin_edges_1 = np.histogram(np.log10(flat_matrix_1),bins=n_bins,range=(min_value,max_value),density=False)
    hist_2,bin_edges_2 = np.histogram(np.log10(flat_matrix_2),bins=n_bins,range=(min_value,max_value),density=False)
    bin_width=bin_edges_1[1]-bin_edges_1[0]
    
    H_0 = h*100*1E5/Mpc_to_cm
    cLbox = (1+redshift)*cell_size_cm*512

    dX1 = H_0*cLbox*(1+redshift)**2/speed_of_light
    dX = 512**2*dX1
    n=200
    x = np.linspace(14, 21.5, n)

    fHI_1 = np.where(hist_1<=1E-30,1E-30,hist_1/(bin_width*10**(bin_edges_1[:-1]+bin_width/2)*dX))
    fHI_2 = np.where(hist_2<=1E-30,1E-30,hist_2/(bin_width*10**(bin_edges_2[:-1]+bin_width/2)*dX))
    NHI_1=bin_edges_1[:-1][:,np.newaxis]

    gp=Gauss_process(NHI_1,np.log10(fHI_1),np.log10(fHI_1)/30,1,10)
    mu_1, err_1 = gp.predict(x[:,None], return_std=True)
    #print(gp.kernel_.get_params())

    gp=Gauss_process(NHI_1,np.log10(fHI_2),np.log10(fHI_2)/30,1,10)
    mu_2, err_2 = gp.predict(x[:,None], return_std=True)
    #print(gp.kernel_.get_params())

    plt.plot(x,10**mu_1,color='orange',label="My data")

    plt.plot(X_Rahmati,Y_Rahmati,label="Rahmati",color='red')
    plt.plot(X_Gallego_z_3,Y_Gallego_z_3,label="Gallego z=3",color="purple")
    plt.plot(X_Gallego_z_6,Y_Gallego_z_6,label="Gallego z=6",color="black")
    #plt.plot(X_Observations,Y_Observations,label="Observations",color="green")
    #plt.plot(bin_edges_1[:-1]+bin_width/2,fHI,color='orange')
    plt.yscale("log")   
    plt.xlabel(r"NHI [$cm^{-2}$]")
    plt.ylabel(r"fHI [$cm^{-2}$]")
    plt.legend()
    plt.ylim(1E-30,1E-13)
    plt.xlim(13.5,22)
    plt.title("HI Column Density Distribution Function")
    plt.savefig(dir_name+"fHI_WDM_CDM"+title_string)
    plt.clf()


    plt.plot(x,10**mu_1,color='blue',label="CDM")
    plt.plot(x,10**mu_2,color='orange',label="WDM")
    plt.yscale("log")   
    plt.xlabel(r"NHI [$cm^{-2}$]")
    plt.ylabel(r"fHI [$cm^{-2}$]")
    plt.ylim(1E-30,1E-13)
    plt.xlim(13.5,22)
    plt.title("HI Column Density Distribution Function")
    plt.legend()
    plt.savefig(dir_name+"fHI"+title_string)
    plt.clf()
    
    return x,10**mu_1,10**mu_2 

    #diff_distribution = (hist_1-hist_2)/hist_1

    #plot_residuals(dir_name, X_plot[:,0], diff_distribution, color_label, title_string)
    
    #print("Kolmogorov_Smirnov_test_hist: ",stats.ks_2samp(hist_1,hist_2))

def load_galaxies_positions(location,min_max_z,z_axis,minx,miny,max_x,max_y):
    columns = (5, 6, 7, 64, 11)
    # Read the data using genfromtxt and select specific columns
    data = np.genfromtxt(location, usecols=columns)
    galaxies_mass = data[:,3]
    minz=min_max_z[0]
    max_z=min_max_z[1]
    index_x=index_list(z_axis)[0]
    index_y=index_list(z_axis)[1]
    # selct galaxies with stellar mass and convert to physical Mpc from kpc/h
    min_mass=3E8
    xC = data[:,index_x][galaxies_mass>min_mass]/(1000*h)
    yC = data[:,index_y][galaxies_mass>min_mass]/(1000*h)
    zC = data[:,z_axis][galaxies_mass>min_mass]/(1000*h)
    Rh = data[:,4][galaxies_mass>min_mass]/(1000*h)
    Gal_masses = data[:,3][galaxies_mass>min_mass]/h
    #select data with ZC in the zoom in region
    xC = xC[(zC>minz) & (zC<max_z)]
    yC = yC[(zC>minz) & (zC<max_z)] 
    Gal_masses = Gal_masses[(zC>minz) & (zC<max_z)]
    zC = zC[(zC>minz) & (zC<max_z)] 

    yC = yC[(xC>minx) & (xC<max_x)] 
    Gal_masses = Gal_masses[(xC>minx) & (xC<max_x)]
    zC = zC[(xC>minx) & (xC<max_x)] 
    xC = xC[(xC>minx) & (xC<max_x)]

    xC = xC[(yC>miny) & (yC<max_y)]
    Gal_masses = Gal_masses[(yC>miny) & (yC<max_y)]
    zC = zC[(yC>miny) & (yC<max_y)]   
    yC = yC[(yC>miny) & (yC<max_y)] 
    #print("Galaxy Mass")
    #print("Max\tMin")
    #print(max(Gal_masses),min(Gal_masses))
    #print(len(xC))

    return xC,yC,zC,Gal_masses

#returns index position
def compute_grid_pos(x,y,cell_size,le_cut_x,le_cut_y):
    cell_size_Mpc=cell_size/(Mpc_to_cm)
    #print("cells_size: "+str(cell_size_kpc)+"x: "+str(x)+"y: "+str(y))
    px = np.floor((x-le_cut_x)/(cell_size_Mpc))
    py = np.floor((y-le_cut_y)/(cell_size_Mpc))
    return px,py

#returns coordinates
def compute_center(px,py,cell_size,le_cut_x,le_cut_y):
    cell_size_Mpc = cell_size/Mpc_to_cm
    #print("cells_size: "+str(cell_size_kpc)+"x: "+str(x)+"y: "+str(y))
    cx=px*cell_size_Mpc/512+le_cut_x+cell_size_Mpc/(2*512)
    cy=py*cell_size_Mpc/512+le_cut_y+cell_size_Mpc/(2*512)
    return cx,cy

def compute_distance(center_x,center_y,i,j):
    distance = np.floor(np.sqrt((center_x-i)**2+(center_y-j)**2))
    return distance

def compute_covering_array(dir_name,map_1,halo_loc,min_x_Mpc,max_x_Mpc,min_y_Mpc,max_y_Mpc,min_max_z,axis,cell_size,number_of_pixels):
    map_1_flip=np.flipud(map_1)
    x,y,z,Gal_masses = load_galaxies_positions(halo_loc,min_max_z,axis,min_x_Mpc,min_y_Mpc,max_x_Mpc,max_y_Mpc)
    #print("minimum x axis:",axis," : ",min_x_Mpc,min(x))
    #print("minimum y axis:",axis," : ",min_y_Mpc,min(x))

    number_of_radii = 17
    average_nhi_d_tot_1=np.zeros(int(np.ceil(number_of_radii*np.sqrt(2))))
    square_average_nhi_d_tot_1=np.zeros(int(np.ceil(number_of_radii*np.sqrt(2))))
    cont_2 = 0
    for cont,mass in enumerate(Gal_masses):
        px,py=compute_grid_pos(x[cont],y[cont],cell_size,min_x_Mpc,min_y_Mpc)
        NHI_r_1=np.zeros(len(average_nhi_d_tot_1))
        cont_radius=np.zeros(len(NHI_r_1))
        if px>number_of_radii and px<number_of_pixels-number_of_radii and py>number_of_radii and py<number_of_pixels-number_of_radii:
            cont_2 +=1
            for i in range(number_of_radii*2+1):
                for j in range(number_of_radii*2+1):
                    new_x=int(px+i-number_of_radii)
                    new_y=int(py+j-number_of_radii)
                    d=compute_distance(px,py,new_x,new_y)
                    if np.log10(map_1[int(new_x)][int(new_y)]) > 17.2:
                        NHI_r_1[int(d)]+=1

                    cont_radius[int(d)]+=1

            for i,el in enumerate(NHI_r_1):
                average_nhi_d_tot_1[i]+=el/cont_radius[i]
                square_average_nhi_d_tot_1[i]+=((el/cont_radius[i])**2)

            #print("NHI:", NHI_r_1)
            #print(cont_radius)
            number_of_pixels = 512
            radius=np.arange(len(NHI_r_1))
            radius=radius*4000/number_of_pixels #4000 kpc
            plt.plot(radius,NHI_r_1/(cont_radius),label="CDM")
            plt.xlabel("Distance from galaxy (kpc)")
            plt.ylabel("LLS covering fraction")
            plt.title("LLS covering fraction")
            plt.legend()
            #plt.ylim(0.00,0.095)
            plt.savefig(dir_name+"covering_radius"+"individual"+str(cont))
            plt.clf()
    average_nhi_d_tot_1 = average_nhi_d_tot_1/cont_2
    square_average_nhi_d_tot_1 = square_average_nhi_d_tot_1/cont_2

    sigma = np.sqrt(square_average_nhi_d_tot_1-average_nhi_d_tot_1**2)/np.sqrt(cont_2)

    #print(average_nhi_d_tot_1)
    #print(square_average_nhi_d_tot_1)
    #print(len(Gal_masses))

    return average_nhi_d_tot_1,sigma,x,y
 

def covering_fraction(map_1,map_2,folder,cell_size,axis,title_string,min_max_1,min_max_2,min_max_z,v_min,v_max,halo_location,halo_location_2):
    dir_name = folder+str(axis)+"/"
    color_label=r'Log(NHI[$cm^{-2}$]'
    x_label='x (Mpc)'
    y_label='y (Mpc)'
    number_of_pixels = 512
    average_nhi_d_tot_1,sigma_1,x,y = compute_covering_array(dir_name,map_1,halo_location,min_max_1[0],min_max_1[1],min_max_2[0],min_max_2[1],min_max_z,axis,cell_size,number_of_pixels)
    average_nhi_d_tot_2,sigma_2,x2,y2 = compute_covering_array(dir_name,map_2,halo_location_2,min_max_1[0],min_max_1[1],min_max_2[0],min_max_2[1],min_max_z,axis,cell_size,number_of_pixels)
    radius=np.arange(len(average_nhi_d_tot_1))
    radius=radius*4000/number_of_pixels #4000 kpc

    #plt.errorbar(radius,average_nhi_d_tot_1,yerr=sigma_1,label="CDM",capsize=10)
    #plt.errorbar(radius,average_nhi_d_tot_2,yerr=sigma_2,label="WDM",capsize=10)
    plt.plot(radius,average_nhi_d_tot_1,label="CDM")
    plt.plot(radius,average_nhi_d_tot_2,label="WDM")

    plt.xlabel("Distance from galaxy (kpc)")
    plt.ylabel("LLS covering fraction")
    plt.title("LLS covering fraction")
    plt.legend()
    plt.ylim(0.00,1.1)
    plt.savefig(dir_name+"covering_radius"+title_string)
    plt.clf()

    plt.imshow((np.log10(map_1)).T, vmin=v_min, vmax=v_max,cmap='viridis', origin='lower', extent=[min_max_1[0],min_max_1[1],min_max_2[0],min_max_2[1]])
    cbar = plt.colorbar(label=color_label)
    plt.scatter(x=x, y=y,c="white",s=9)
    plt.xlim(min_max_1[0],min_max_1[1])
    plt.ylim(min_max_2[0],min_max_2[1])
    plt.title(title_string+"CDM")
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.savefig(dir_name+"CirclesCDM"+title_string)
    plt.clf()

    plt.imshow((np.log10(map_2)).T, vmin=v_min, vmax=v_max,cmap='viridis', origin='lower', extent=[min_max_1[0],min_max_1[1],min_max_2[0],min_max_2[1]])
    cbar = plt.colorbar(label=color_label)

    plt.scatter(x=x2, y=y2,c="white",s=9)
    plt.xlim(min_max_1[0],min_max_1[1])
    plt.ylim(min_max_2[0],min_max_2[1])
    plt.title(title_string+"WDM")
    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.savefig(dir_name+"CirclesWDM"+title_string)
    plt.clf()
    
    return average_nhi_d_tot_1,average_nhi_d_tot_2,radius

def order_and_move(arr1, arr2):
    # Create a numpy array of indices
    indices = np.argsort(arr1)
    
    # Use the indices to reorder both arrays
    sorted_arr1 = arr1[indices]
    reordered_arr2 = [arr2[i] for i in indices]
    
    return sorted_arr1, reordered_arr2
def variance_SB(dir_name, min_max_x,min_max_y,matrix_1,matrix_2,axis):
    variance_matrix_1 = np.zeros_like(matrix_1)
    variance_matrix_2 = np.zeros_like(matrix_2)

    # Iterate over each cell in the input matrix
    for i in range(matrix_1.shape[0]):
        for j in range(matrix_1.shape[1]):
            if i > 3 and j >3 and i<507 and j<507:
                # Extract the 7x7 submatrix centered on the current cell
                submatrix = matrix_1[i-3:i+4, j-3:j+4]

                # Compute the variance of the submatrix
                variance = np.sqrt(np.sum((submatrix-np.ones((7,7))*matrix_1[i,j])**2)/49)

                # Store the variance in the output matrix
                variance_matrix_1[i, j] = variance

    for i in range(matrix_2.shape[0]):
        for j in range(matrix_2.shape[1]):
            if i > 3 and j >3 and i<507 and j<507:
                # Extract the 7x7 submatrix centered on the current cell
                submatrix = matrix_2[i-3:i+4, j-3:j+4]

                # Compute the variance of the submatrix
                variance = np.sqrt(np.sum((submatrix-np.ones((7,7))*matrix_2[i,j])**2)/49)

                # Store the variance in the output matrix
                variance_matrix_2[i, j] = variance


    color_label="log(SB [erg s^-1 cm^-2 arcsec^-2])"
    plt.imshow(np.log10(variance_matrix_1), cmap='jet', origin='lower',clim=[-22,-13], extent=[min_max_x[0],min_max_x[1],min_max_y[0],min_max_y[1]])
    cbar = plt.colorbar(label="log(SB standard deviation"+color_label[6:])
    plt.title("CDM")
    plt.xlabel("X")
    plt.ylabel("Y")
    plt.xlim(min_max_x[0],min_max_x[1])
    plt.ylim(min_max_y[0],min_max_y[1])
    plt.savefig(folder_name+str(axis)+"/SB_variance_CDM")
    cbar.remove()
    plt.clf()

    plt.imshow(np.log10(variance_matrix_2), cmap='jet', origin='lower', clim=[-22,-13], extent=[min_max_x[0],min_max_x[1],min_max_y[0],min_max_y[1]])
    cbar = plt.colorbar(label="log(SB standard deviation"+color_label[6:])
    plt.title("WDM")
    plt.xlabel("X")
    plt.ylabel("Y")
    plt.xlim(min_max_x[0],min_max_x[1])
    plt.ylim(min_max_y[0],min_max_y[1])
    plt.savefig(folder_name+str(axis)+"/SB_variance_WDM")
    cbar.remove()
    plt.clf()

    flat_matrix_1 = np.where(np.ravel(variance_matrix_1)<=1E-30,1E-35,np.ravel(variance_matrix_1))
    flat_matrix_2 = np.where(np.ravel(variance_matrix_2)<=1E-30,1E-35,np.ravel(variance_matrix_2))
    min_value = min(np.log10(flat_matrix_1))
    max_value = max(np.log10(flat_matrix_1))
    if np.isnan(min_value) or np.isnan(max_value):
        print("NAN")
        input(str(axis))
        return 0
    
    dist_1_label="CDM"
    dist_2_label="WDM"
    title_string="SB Variance histogram"
    n_bins=100
    plot_two_histograms(dir_name, flat_matrix_1, flat_matrix_2, dist_1_label, dist_2_label,"Special SB standard deviation Histogram", "log(SB standard deviation"+color_label[6:], n_bins,plot_xlim=False) 
    
    hist1 = flat_matrix_1
    hist2 = flat_matrix_2

    print('Start')
    n_bins = 100 
    bins = np.linspace(np.log10(np.ravel(matrix_1)).min(), np.log10(np.ravel(matrix_1)).max(), n_bins + 1)

    sorted_sb_central_cdm, sorted_sb_var_cdm = order_and_move(np.ravel(matrix_1),flat_matrix_1)
    sorted_sb_central_wdm, sorted_sb_var_wdm = order_and_move(np.ravel(matrix_2),flat_matrix_2)

    central_SB_cdm=np.array(sorted_sb_central_cdm)
    central_SB_wdm=np.array(sorted_sb_central_wdm)
    
    index_1 =np.log10(sorted_sb_var_cdm) > -25
    index_2 =np.log10(sorted_sb_var_wdm) > -25
    
    central_SB_cdm = central_SB_cdm[index_1]
    central_SB_wdm = central_SB_wdm[index_2]

    sorted_sb_var_cdm = np.array(sorted_sb_var_cdm)[index_1]
    sorted_sb_var_wdm = np.array(sorted_sb_var_wdm)[index_2]


    bins = np.linspace(np.log10(central_SB_cdm).min(), np.log10(central_SB_cdm).max(), n_bins + 1)

    medians_cdm = []
    medians_wdm = []
    cont = -30
    for bin_ in bins:
        indices = (np.log10(central_SB_cdm) < bin_) & (np.log10(central_SB_cdm) > cont)
        median = np.median(sorted_sb_var_cdm[indices])
        medians_cdm.append(median)
        cont = bin_

    for bin_ in bins:
        indices = (np.log10(central_SB_wdm) < bin_) & (np.log10(central_SB_wdm) > cont)
        median = np.median(sorted_sb_var_wdm[indices])
        medians_wdm.append(median)
        cont = bin_

    plt.plot(bins, np.log10(medians_cdm),label="CDM")
    plt.plot(bins, np.log10(medians_wdm),label="CDM")
    plt.ylim(-24,-14.5)
     

    #plt.scatter(x=np.log10(central_SB_cdm),y=np.log10(sorted_sb_var_cdm),s=0.1,alpha=0.01)

    """ 
    n=200
    x = np.linspace(-20, -14, n)

    par_1 = np.polyfit(np.log10(central_SB_cdm),np.log10(sorted_sb_var_cdm),18)
    par_2 = np.polyfit(np.log10(central_SB_wdm),np.log10(sorted_sb_var_wdm),18)

    mu_1 = np.poly1d(par_1)
    mu_2 = np.poly1d(par_2)

    plt.plot(x,mu_1(x),color='blue',label="CDM")
    plt.plot(x,mu_2(x),color='orange',label="WDM")

    plt.scatter(x=np.log10(central_SB_cdm),y=np.log10(sorted_sb_var_cdm),s=0.01,alpha=0.1)
    plt.scatter(x=np.log10(central_SB_wdm),y=np.log10(sorted_sb_var_wdm),s=0.01,alpha=0.1)
    """
    plt.title("Special SB standard deviation VS central SB value")
    plt.xlabel(color_label)
    plt.ylabel("log(SB standard deviation"+color_label[6:])
    plt.legend()
    plt.savefig(folder_name+str(axis)+"/SB_special_variance_vs_SB")
    plt.clf()
    print("Done")

    n_bins=40
    flat_matrix_1 = np.where(np.ravel(np.log10(matrix_1))<=-30,-35,np.ravel(np.log10(matrix_1)))
    flat_matrix_2 = np.where(np.ravel(np.log10(matrix_2))<=-30,-35,np.ravel(np.log10(matrix_2)))
    hist_1, bin_edges_1 = np.histogram(flat_matrix_1, bins=n_bins)
    hist_2, bin_edges_2 = np.histogram(flat_matrix_2, bins=n_bins)

    # Calculate the variance for each bin
    variances_1 = []
    for i in range(len(bin_edges_1) - 1):
        bin_data = flat_matrix_1[(flat_matrix_1 >= bin_edges_1[i]) & (flat_matrix_1 < bin_edges_1[i+1])]
        variances_1.append(np.var(bin_data))

    variances_2 = []
    for i in range(len(bin_edges_2) - 1):
        bin_data = flat_matrix_2[(flat_matrix_2 >= bin_edges_2[i]) & (flat_matrix_2 < bin_edges_2[i+1])]
        variances_2.append(np.var(bin_data))
    
    q_1 = np.median(variances_1)
    q_2 = np.median(variances_2)
    plt.axhline(y=q_1,color = "cyan",label="CDM: "+str(round(q_1,5)),linestyle="dashed")
    plt.axhline(y=q_2,color = "red",label="WDM: "+str(round(q_2,5)),linestyle="dashed")
    plt.plot(bin_edges_1[:-1], variances_1,color="blue")
    plt.plot(bin_edges_2[:-1], variances_2,color="orange")
    plt.xlabel(r"log(SB [erg $s^{-1}$ $cm^{-2}$ $arcsec^{-2}$])")
    plt.ylabel("Log(SB) Variance")
    plt.title("Variance in Surface Brightness bins")
    plt.ylim(0,0.0045)
    plt.xlim(-22,-12)
    plt.tight_layout()
    plt.legend()
    plt.savefig(dir_name+"SB_Variances")
    return variances_1,variances_2,q_1,q_2,bin_edges_1[:-1],hist1,hist2

def surface_brightness(location1,location2,axis,min_max_x,min_max_y,folder):
    hdul1 = fits.open(location1)
    hdul2 = fits.open(location2)

    data_SB1 = np.sum(hdul1[0].data,axis=0)*1e-20*(1+z[0])**4
    data_SB2 = np.sum(hdul2[0].data,axis=0)*1e-20*(1+z[0])**4

    data_SB1 = np.where(data_SB1<=-1E-27,1E-27,data_SB1)
    data_SB2 = np.where(data_SB2<=-1E-27,1E-27,data_SB2)

    plt.imshow(np.log10(data_SB1), vmin=-19, vmax=-13,cmap='jet', origin='lower', extent=[min_max_x[0],min_max_x[1],min_max_y[0],min_max_y[1]])
    cbar = plt.colorbar(label="SB")
    plt.title("SB")
    plt.xlabel("X")
    plt.ylabel("Y")
    plt.xlim(min_max_x[0],min_max_x[1])
    plt.ylim(min_max_y[0],min_max_y[1])
    plt.savefig(folder_name+str(axis)+"/SB_CDM")
    cbar.remove()

    plt.imshow(np.log10(data_SB2), vmin=-19, vmax=-13,cmap='jet', origin='lower', extent=[min_max_x[0],min_max_x[1],min_max_y[0],min_max_y[1]])
    cbar = plt.colorbar(label="SB")
    plt.title("SB")
    plt.xlabel("X")
    plt.ylabel("Y")
    plt.xlim(min_max_x[0],min_max_x[1])
    plt.ylim(min_max_y[0],min_max_y[1])
    plt.savefig(folder_name+str(axis)+"/SB_WDM")
    cbar.remove()

    dir_name = folder+str(axis)+"/"
    flat_matrix_1 = np.where(np.ravel(data_SB1)<=1E-30,1E-35,np.ravel(data_SB1))
    flat_matrix_2 = np.where(np.ravel(data_SB2)<=1E-30,1E-35,np.ravel(data_SB2))
    min_value = min(np.log10(flat_matrix_1))
    max_value = max(np.log10(flat_matrix_1))
    if np.isnan(min_value) or np.isnan(max_value):
        print("NAN")
        input(str(axis))
        return 0
    
    dist_1_label="CDM"
    dist_2_label="WDM"
    title_string="SB histogram"
    color_label=r"log(SB [erg $s^{-1} cm^{-2} arcsec^{-2}$])"
    n_bins=100

    # Create the plots
    plot_two_histograms(dir_name, flat_matrix_1, flat_matrix_2, dist_1_label, dist_2_label, title_string, color_label, n_bins)
    SB_v_CDM,SB_v_WDM,median_CDM,median_WDM,edges,hist1,hist2 = variance_SB(dir_name, min_max_x,min_max_y,data_SB1,data_SB2,axis)
    return np.array(SB_v_CDM),np.array(SB_v_WDM),median_CDM,median_WDM,edges,hist1,hist2,flat_matrix_1,flat_matrix_2

# Load files
#z, kev, folder
settings = ["4", "3", "1"]
folder_name = "Comparison_z_"+settings[0]+"_CDM_WDM_zoom_"+settings[1]+"p5kev/axis_"
folder_location=["/data/fbara/simulations/CDM/","/data/fbara/simulations/WDM/"]
#folder_location=["/data/fbara/simulations/CDM/HELLO_","/data/fbara/simulations/WDM/HELLO_"]
filenames =["CDM_z_"+settings[0]+"_zoom_1p5kev","WDM_z_"+settings[0]+"_zoom_"+settings[1]+"p5kev"]
#filenames = ["Output_sftresh_CDM_z_4_zoom","Output_sftresh_WDM_z_4_zoom"]
z = [int(settings[0]),int(settings[0])]
#min_max_list=[[68.3284/h,71.0392/h],[60.9496/h,63.6604/h],[67.3383/h,70.00491/h]]
if int(settings[0]) == 3:
    min_max_list=[[66.2415993685/h,68.95239936853/h],[64.0552471324/h,66.7660471324/h],[68.35748598955/h,71.06828598955/h]]
if int(settings[0]) == 4:
    min_max_list=[[66.22052349/h,68.92852349/h],[64.74644574/h,67.45444574/h],[68.13438534/h,70.84238534/h]]
#min_max_list=[[49.5455/h,52.2285/h],[49.2569/h,51.9409/h],[48.1217/h,50.0857/h]]
size_in_Mpc=min_max_list[0][1]-min_max_list[0][0]
cell_size_cm = (size_in_Mpc / 512) * Mpc_to_cm
SB_location_CDM=[]
SB_location_WDM=[]
axis_name=["X","Y","Z"] 

for i in range(3):
    SB_location_CDM.append("/data/titouan/Eagle_cosmo_gizmo/200_512/CDM/zoom1p5_hydro_fire2_lvl7/Halo10/sftresh_cool_low_fb_boost/snapdir_00"+settings[2]+"/4cMpc/mockCube"+axis_name[i]+"_NoNoise.fits")
    #SB_location_CDM.append("/data/titouan/HELLO/CDM/g9.20e12/4cMpc/mockCube"+axis_name[i]+"_NoNoise.fits")

for i in range(3):
    SB_location_WDM.append("/data/titouan/Eagle_cosmo_gizmo/200_512/WDM_"+settings[1]+"p5keV/zoom1p5_hydro_fire2_lvl7/Halo10/sftresh_cool_low_fb_boost/snapdir_00"+settings[2]+"/4cMpc/mockCube"+axis_name[i]+"_NoNoise.fits")
    #SB_location_WDM.append("/data/titouan/HELLO/WDM/g9.20e12/hydro/4cMpc/mockCube"+axis_name[i]+"_NoNoise.fits")

minx = min_max_list[0][0]
miny = min_max_list[1][0]
minz = min_max_list[2][0]
max_x = min_max_list[0][1]
max_y = min_max_list[1][1]
max_z =  min_max_list[2][1]

NHI_Column_densities_map= [[[],[],[]],[[],[],[]]]
Gas_Column_densities_map= [[[],[],[]],[[],[],[]]]

halo_location = "/data/titouan/Eagle_cosmo_gizmo/200_512/CDM/zoom1p5_hydro_fire2_lvl7/Halo10/sftresh_cool_low_fb_boost/AHF_00"+settings[2]+"_allparts/200_snap_z"+settings[0]+".000_halos_sorted_main_hires"
halo_location_2 = "/data/titouan/Eagle_cosmo_gizmo/200_512/WDM_"+settings[1]+"p5keV/zoom1p5_hydro_fire2_lvl7/Halo10/sftresh_cool_low_fb_boost/AHF_00"+settings[2]+"_allparts/200_snap_z"+settings[0]+".000_halos_sorted_main_hires"

#halo_location = "/data/titouan/HELLO/CDM/g9.20e12/g9.20e12.00112.z4.022.AHF_halos"
#halo_location_2 = "/data/titouan/HELLO/WDM/g9.20e12/hydro/g9.20e12.00112.z4.022.AHF_halos"

data = [load_file(folder_location[0]+filenames[0],minx,miny,minz,max_x,max_y,max_z,cell_size_cm,halo_location),load_file(folder_location[1]+filenames[1],minx,miny,minz,max_x,max_y,max_z,cell_size_cm,halo_location_2)]
Neutral_mass = [data[0][0], data[1][0]]
Gas_mass = [data[0][1], data[1][1]]  
Gas_densities_map=[Compute_density(Neutral_mass[0], cell_size_cm), Compute_density(Neutral_mass[1], cell_size_cm)]
print(Gas_densities_map[0])
print('max,min')
print(Gas_densities_map[0].min(),Gas_densities_map[0].max())
NHI_3D_label =r'Log(Neutral Hydrogen Densities[$cm^{-3}$]'
plot_two_histograms("Comparison_z_"+settings[0]+"_CDM_WDM_zoom_"+settings[1]+"p5kev/",np.ravel(Gas_densities_map[0]),np.ravel(Gas_densities_map[1]),'CDM','WDM '+str(settings[1])+'keV',"NHI densities z="+str(settings[0]),NHI_3D_label,200,minimum_value=-15,maximum_value=-5,plot_xlim=1,is_log=True)
print('Hist done')
Temperature = [data[0][2], data[1][2]] 

#bidimensional_histograms(folder_name,Gas_densities_map[0],Temperature[0],r"log(density)$cm^{-3}$",r"log(T[K])","Density_vs_T",v_min=1E-3,v_max=1E1)
for j in range(2):
    for i in range(3):
        Neutral_Column_density = Compute_column_density(Neutral_mass[j], cell_size_cm,i)#/((1+z[j])**2)
        #Gas_Column_density = Compute_column_density(Gas_mass[j], cell_size_cm,i)#/((1+z[j])**2)
        
        NHI_Column_densities_map[j][i] = Neutral_Column_density
        #Gas_Column_densities_map[j][i] = Gas_Column_density

avg_median_CDM = 0
avg_median_WDM = 0
medians_cdm = []
medians_wdm = []
hist1cdm_list = []
hist2wdm_list = []
NHI_label =r'Log(NHI[$cm^{-2}$]'
Col_den = 'NHI Column Density Distribution'

dir_name_1 = "Comparison_z_"+settings[0]+"_CDM_WDM_zoom_"+settings[1]+"p5kev/"

#----------------------------------------------------------------------------------------------------------------------

hist_axis_0_cdm,_=np.histogram(np.log10(np.where(np.ravel(NHI_Column_densities_map[0][0][0])<=1,1,np.ravel(NHI_Column_densities_map[0][0][0]))), bins=100, range=(12.5,21), density=False)
hist_axis_1_cdm,_=np.histogram(np.log10(np.where(np.ravel(NHI_Column_densities_map[0][1][0])<=1,1,np.ravel(NHI_Column_densities_map[0][1][0]))), bins=100, range=(12.5,21), density=False)
hist_axis_2_cdm,_=np.histogram(np.log10(np.where(np.ravel(NHI_Column_densities_map[0][2][0])<=1,1,np.ravel(NHI_Column_densities_map[0][2][0]))), bins=100, range=(12.5,21), density=False)

hist1cdm_nhi= (hist_axis_0_cdm + hist_axis_1_cdm + hist_axis_2_cdm) / 3
hist1cdm_dev_nhi = np.std(np.stack((hist_axis_0_cdm, hist_axis_1_cdm, hist_axis_2_cdm)),axis=0) / np.sqrt(3)

hist_axis_0_wdm,_=np.histogram(np.log10(np.where(np.ravel(NHI_Column_densities_map[1][0][0])<=1,1,np.ravel(NHI_Column_densities_map[1][0][0]))), bins=100, range=(12.5,21), density=False)
hist_axis_1_wdm,_=np.histogram(np.log10(np.where(np.ravel(NHI_Column_densities_map[1][1][0])<=1,1,np.ravel(NHI_Column_densities_map[1][1][0]))), bins=100, range=(12.5,21), density=False)
hist_axis_2_wdm,_=np.histogram(np.log10(np.where(np.ravel(NHI_Column_densities_map[1][2][0])<=1,1,np.ravel(NHI_Column_densities_map[1][2][0]))), bins=100, range=(12.5,21), density=False)

hist2wdm_nhi= (hist_axis_0_wdm + hist_axis_1_wdm + hist_axis_2_wdm) / 3
hist2wdm_dev_nhi = np.std(np.stack((hist_axis_0_wdm, hist_axis_1_wdm, hist_axis_2_wdm)),axis=0) / np.sqrt(3)

plt.clf()
bincentres = [(_[i]+_[i+1])/2. for i in range(len(_)-1)]
#plt.step(bincentres,hist1cdm,where='mid',label='CDM')
plt.fill_between(bincentres, hist1cdm_nhi-hist1cdm_dev_nhi, hist1cdm_nhi+hist1cdm_dev_nhi,color='blue', alpha=0.5, label='CDM')
#plt.step(bincentres,hist2wdm,where='mid',label='WDM')
plt.fill_between(bincentres, hist2wdm_nhi-hist2wdm_dev_nhi, hist2wdm_nhi+hist2wdm_dev_nhi,color='orange', alpha=0.5, label='WDM '+str(settings[1])+'.5 keV')

plt.title("NHI Distribution | z = "+str(settings[0]))
plt.xlabel(r"log(Neutral Hydrogen Column Density [$cm^{-2}$])")
plt.ylim(0,14500)
plt.legend()
plt.savefig(dir_name_1+"NHI Distribution column density")
plt.clf()


print('Hist done 2')
fNHI_cdm_list = []
fNHI_wdm_list = []
SB_dist_cdm_list = []
SB_dist_wdm_list = []
c1_list=[]
c2_list=[]
for i in range(3):
    index_x = index_list(i)[0]
    index_y = index_list(i)[1]
    min_max_x=min_max_list[index_x]
    min_max_y=min_max_list[index_y]

    SB_v_cdm,SB_v_wdm,var_cdm,var_wdm,SB_bins,hist1cdm,hist2wdm,SB_dist_cdm,SB_dist_wdm = surface_brightness(SB_location_CDM[i],SB_location_WDM[i],i,min_max_x,min_max_y,folder_name)
    avg_median_CDM += var_cdm/3
    avg_median_WDM += var_wdm/3
    medians_cdm.append(var_cdm) 
    medians_wdm.append(var_wdm) 
    hist1cdm_list.append(hist1cdm)   
    hist2wdm_list.append(hist2wdm)   
    #for k in range(9):
    #    if k==0:
    c_1,c_2,radii=covering_fraction(NHI_Column_densities_map[0][i][0],NHI_Column_densities_map[1][i][0],folder_name,cell_size_cm,i, 'CDM_z='+settings[0]+str(0),min_max_x,min_max_y,min_max_list[i],13,22,halo_location,halo_location_2)
    map_residual=(np.array(NHI_Column_densities_map[0][i][0])-np.array(NHI_Column_densities_map[1][i][0]))/np.array(NHI_Column_densities_map[0][i][0])
    plot_colormap(folder_name,map_residual, NHI_label,'x (Mpc)', 'y (Mpc)','Neutral Hydrogen Column Density'+str(0), min_max_x,min_max_y, is_log_10=False, v_min=-1, v_max=1,axis=i)
    #plot_distribution(map_residual, "NHI Column Density Distribution", 1000, -1,1,axis=i,is_log=False)
    plot_colormap(folder_name,NHI_Column_densities_map[0][i][0], NHI_label,'x (Mpc)', 'y (Mpc)','CDM Neutral Hydrogen Column Density z='+settings[0]+str(0), min_max_x,min_max_y, is_log_10=True, v_min=12,v_max=22,axis=i)
    plot_colormap(folder_name,NHI_Column_densities_map[1][i][0], NHI_label,'x (Mpc)', 'y (Mpc)','WDM Neutral Hydrogen Column Density z='+settings[0]+str(0), min_max_x,min_max_y, is_log_10=True, v_min=12,v_max=22,axis=i)
    x_NHI,fNHI_cdm,fNHI_wdm = plot_distribution_2(folder_name,NHI_Column_densities_map[0][i][0],NHI_Column_densities_map[1][i][0],NHI_label,Col_den +str(0), 1000, 13,22,"CDM_z="+settings[0],"WDM_z="+settings[0],z[0],axis=i)
    print(SB_v_cdm)
    print(var_cdm)
    c1_list.append(c_1)
    c2_list.append(c_2)

    if i == 0:
        covering_average_1 = c_1/3
        covering_average_2 = c_2/3
        SB_var_avg_cdm = SB_v_cdm/3
        SB_var_avg_wdm = SB_v_wdm/3
    else:
        covering_average_1 += c_1/3
        covering_average_2 += c_2/3
        SB_var_avg_cdm += SB_v_cdm/3
        SB_var_avg_wdm += SB_v_wdm/3

    fNHI_cdm_list.append(fNHI_cdm)    
    fNHI_wdm_list.append(fNHI_wdm)    
    SB_dist_cdm_list.append(SB_dist_cdm)
    SB_dist_wdm_list.append(SB_dist_wdm)


#----------------------------------------------------------------------------------------------------------------------
dir_name = "Comparison_z_"+settings[0]+"_CDM_WDM_zoom_"+settings[1]+"p5kev/"
#----------------------------------------------------------------------------------------------------------------------
hist1cdm = (fNHI_cdm_list[0]+fNHI_cdm_list[1]+fNHI_cdm_list[2]) / 3
hist1cdm_dev = np.std(np.stack((fNHI_cdm_list[0],fNHI_cdm_list[1],fNHI_cdm_list[2])),axis=0) / np.sqrt(3)

print(hist1cdm)

hist2wdm = (fNHI_wdm_list[0]+fNHI_wdm_list[1]+fNHI_wdm_list[2]) / 3
hist2wdm_dev = np.std(np.stack((fNHI_wdm_list[0],fNHI_wdm_list[1],fNHI_wdm_list[2])),axis=0) / np.sqrt(3)


plt.clf()
#plt.step(bincentres,hist1cdm,where='mid',label='CDM')
plt.fill_between(x_NHI, hist1cdm-hist1cdm_dev, hist1cdm+hist1cdm_dev,color='blue', alpha=0.5,label='CDM')
#plt.step(bincentres,hist2wdm,where='mid',label='WDM')
plt.fill_between(x_NHI, hist2wdm-hist2wdm_dev, hist2wdm+hist2wdm_dev,color='orange', alpha=0.5,label='WDM '+str(settings[1])+'.5 keV')

plt.plot(X_Rahmati,Y_Rahmati,label="Rahmati",color='red')
plt.plot(X_Gallego_z_3,Y_Gallego_z_3,label="Gallego z=3",color="purple")
plt.plot(X_Gallego_z_6,Y_Gallego_z_6,label="Gallego z=6",color="black")

plt.yscale("log")   
plt.xlabel(r"NHI [$cm^{-2}$]")
plt.ylabel(r"fHI [$cm^{-2}$]")
plt.legend()
plt.ylim(1E-30,1E-13)
plt.xlim(13.5,22)
plt.title("HI Column Density Distribution Function | z = "+str(settings[0]))

plt.legend()
plt.savefig(dir_name+"fNHI_average")
plt.clf()


#----------------------------------------------------------------------------------------------------------------------
hist_axis_0_cdm,_=np.histogram(np.log10(SB_dist_cdm_list[0]), bins=100, range=(-22,-11), density=False)
hist_axis_1_cdm,_=np.histogram(np.log10(SB_dist_cdm_list[1]), bins=100, range=(-22,-11), density=False)
hist_axis_2_cdm,_=np.histogram(np.log10(SB_dist_cdm_list[2]), bins=100, range=(-22,-11), density=False)

print(hist_axis_0_cdm)

hist1cdm = (hist_axis_0_cdm + hist_axis_1_cdm + hist_axis_2_cdm) / 3
hist1cdm_dev = np.std(np.stack((hist_axis_0_cdm, hist_axis_1_cdm, hist_axis_2_cdm)),axis=0) / np.sqrt(3)

print(hist1cdm)
hist_axis_0_wdm,_=np.histogram(np.log10(SB_dist_wdm_list[0]), bins=100, range=(-22,-11), density=False)
hist_axis_1_wdm,_=np.histogram(np.log10(SB_dist_wdm_list[1]), bins=100, range=(-22,-11), density=False)
hist_axis_2_wdm,_=np.histogram(np.log10(SB_dist_wdm_list[2]), bins=100, range=(-22,-11), density=False)

hist2wdm = (hist_axis_0_wdm + hist_axis_1_wdm + hist_axis_2_wdm) / 3
hist2wdm_dev = np.std(np.stack((hist_axis_0_wdm, hist_axis_1_wdm, hist_axis_2_wdm)),axis=0) / np.sqrt(3)

plt.clf()
bincentres = [(_[i]+_[i+1])/2. for i in range(len(_)-1)]
#plt.step(bincentres,hist1cdm,where='mid',label='CDM')
plt.fill_between(bincentres, hist1cdm-hist1cdm_dev, hist1cdm+hist1cdm_dev,color='blue', alpha=0.5,label='CDM')
#plt.step(bincentres,hist2wdm,where='mid',label='WDM')
plt.fill_between(bincentres, hist2wdm-hist2wdm_dev, hist2wdm+hist2wdm_dev,color='orange', alpha=0.5,label='WDM '+str(settings[1])+'.5 keV')

plt.title("Surface Brightness Distribution | z = "+str(settings[0]))
plt.xlabel(r"log(SB [erg $s^{-1} cm^{-2} arcsec^{-2}$])")
plt.ylim(0,18000)
plt.legend()
plt.savefig(dir_name+"SB_distribution_average")
plt.clf()

#----------------------------------------------------------------------------------------------------------------------
print(np.log10(hist1cdm_list[0]))

hist_axis_0_cdm,_=np.histogram(np.log10(hist1cdm_list[0]), bins=100, range=(-24,-13), density=False)
hist_axis_1_cdm,_=np.histogram(np.log10(hist1cdm_list[1]), bins=100, range=(-24,-13), density=False)
hist_axis_2_cdm,_=np.histogram(np.log10(hist1cdm_list[2]), bins=100, range=(-24,-13), density=False)

print(hist_axis_0_cdm)

hist1cdm = (hist_axis_0_cdm + hist_axis_1_cdm + hist_axis_2_cdm) / 3
hist1cdm_dev = np.std(np.stack((hist_axis_0_cdm, hist_axis_1_cdm, hist_axis_2_cdm)),axis=0) / np.sqrt(3)

print(hist1cdm)

hist_axis_0_wdm,_=np.histogram(np.log10(hist2wdm_list[0]), bins=100, range=(-24,-13), density=False)
hist_axis_1_wdm,_=np.histogram(np.log10(hist2wdm_list[1]), bins=100, range=(-24,-13), density=False)
hist_axis_2_wdm,_=np.histogram(np.log10(hist2wdm_list[2]), bins=100, range=(-24,-13), density=False)

hist2wdm = (hist_axis_0_wdm + hist_axis_1_wdm + hist_axis_2_wdm) / 3
hist2wdm_dev = np.std(np.stack((hist_axis_0_wdm, hist_axis_1_wdm, hist_axis_2_wdm)),axis=0) / np.sqrt(3)

plt.clf()
bincentres = [(_[i]+_[i+1])/2. for i in range(len(_)-1)]
#plt.step(bincentres,hist1cdm,where='mid',label='CDM')
plt.fill_between(bincentres, hist1cdm-hist1cdm_dev, hist1cdm+hist1cdm_dev,color='blue', alpha=0.5,label='CDM')
#plt.step(bincentres,hist2wdm,where='mid',label='WDM')
plt.fill_between(bincentres, hist2wdm-hist2wdm_dev, hist2wdm+hist2wdm_dev,color='orange', alpha=0.5,label='WDM '+str(settings[1])+'.5 keV')

plt.title("log(SB standard deviation) | z = "+str(settings[0]))
plt.xlabel(r"log(SB standard deviation [erg $s^{-1} cm^{-2} arcsec^{-2}$])")
plt.ylim(0,9000)
plt.legend()
plt.savefig(dir_name+"7x7 Special Standard Deviation Distribution")
plt.clf()

print(SB_var_avg_cdm)
print(avg_median_CDM)


std_dev_array_1 = np.std(np.stack((c1_list[0], c1_list[1], c1_list[2])), axis=0)
std_dev_array_2 = np.std(np.stack((c2_list[0], c2_list[1], c2_list[2])), axis=0)
std_dev_array_cdm = np.where(std_dev_array_1 != 0, std_dev_array_1, 0.1)
std_dev_array_wdm = np.where(std_dev_array_2 != 0, std_dev_array_2, 0.1)
popt_1,pcov_1 = curve_fit(sigma_function,radii,covering_average_1,sigma=std_dev_array_cdm,bounds=(0.0,[1.05,0.45,0.01,0.1,0.1]))
popt_2,pcov_2 = curve_fit(sigma_function,radii,covering_average_2,sigma=std_dev_array_wdm,bounds=(0.0,[1.05,0.45,0.01,0.1,0.1]))

print("fit")
print(popt_1)
print(popt_2)

plt.scatter(radii,covering_average_1)
plt.scatter(radii,covering_average_2)

plt.plot(radii,sigma_function(radii,*popt_1))
textstr = '\n'.join((
    "CDM",
    r'$\mathrm{K}=(%.1e) \pm (%.0e)$' % (popt_1[1], np.sqrt(np.diag(pcov_1)[1])),
    r'$\mathrm{Q}=(%.1e) \pm (%.0e)$' % (popt_1[2],np.sqrt(np.diag(pcov_1)[2])),
    r'$\beta=(%.1e) \pm (%.0e)$' % (popt_1[3],np.sqrt(np.diag(pcov_1)[3])),
    r'$\nu=(%.1e) \pm (%.0e)$' % (popt_1[4],np.sqrt(np.diag(pcov_1)[4]))))
plt.text(0, 0.05,textstr, fontsize = 10, bbox = dict(facecolor = 'white', alpha = 0.5))
textstr = '\n'.join((
    "WDM",
    r'$\mathrm{K}=(%.1e) \pm (%.0e)$' % (popt_2[1], np.sqrt(np.diag(pcov_1)[1])),
    r'$\mathrm{Q}=(%.1e) \pm (%.0e)$' % (popt_2[2], np.sqrt(np.diag(pcov_1)[2])),
    r'$\beta=(%.1e) \pm (%.0e)$' % (popt_2[3], np.sqrt(np.diag(pcov_1)[3])),
    r'$\nu=(%.1e) \pm (%.0e)$' % (popt_2[4], np.sqrt(np.diag(pcov_1)[4]))))
plt.text(82, 0.05,textstr, fontsize = 10, bbox = dict(facecolor = 'white', alpha = 0.5))
plt.text(90, 1.0,r'$1+\frac{K-1}{(1+Qe^{-\beta x})^{\frac{1}{\nu}}}$', fontsize = 17, bbox = dict(facecolor = 'white', alpha = 0.5))
plt.plot(radii,sigma_function(radii,*popt_2))
plt.xlabel("Distance from galaxy (kpc)")
plt.ylabel("LLS covering fraction")
plt.title("LLS covering fraction | z = "+str(settings[0]))
plt.ylim(0.00,1.1)
plt.savefig(dir_name+"AVG_covering_radius_fit")
plt.clf()

dir_name =folder_name = "Comparison_z_"+settings[0]+"_CDM_WDM_zoom_"+settings[1]+"p5kev/"
plt.plot(SB_bins,SB_var_avg_cdm)
plt.plot(SB_bins,SB_var_avg_wdm)
plt.axhline(y=np.median(SB_var_avg_cdm),color = "cyan",label="CDM: "+str(round(np.median(SB_var_avg_cdm),4))+"±"+str(round(np.std(medians_cdm)/np.sqrt(3),4)),linestyle="dashed")
plt.axhline(y=np.median(SB_var_avg_wdm),color = "red",label="WDM: "+str(round(np.median(SB_var_avg_wdm),4))+"±"+str(round(np.std(medians_wdm)/np.sqrt(3),4)),linestyle="dashed")
plt.legend()
plt.xlabel(r"log(SB [erg $s^{-1}$ $cm^{-2}$ $arcsec^{-2}$])")
plt.ylabel("Log(SB) Variance")
plt.title("Variance in Surface Brightness bins | z = "+str(settings[0]))
 
plt.ylim(0.002,0.004)
plt.savefig(dir_name+"AVG_SB")
plt.clf()

print("Average Variance along the three lines of sight:")
print("CDM:",avg_median_CDM)
print("WDM:",avg_median_WDM)
